﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace ProjetoEmpresaDasAguas
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
            FormInicio formInicio = new FormInicio();
            formInicio.MdiParent = this;
            formInicio.Show();
        }

        
        private void Form1_Load(object sender, EventArgs e)
        {
            AlterarCorForm();
        }

        /// <summary>
        /// Merodo para alterar a cor do form principal
        /// </summary>
        private void AlterarCorForm()
        {
            // Classe que contem os multiplos formulários
            MdiClient mdiClient;

            // Procura os controlos do tipo MdiClient
            foreach (Control var in this.Controls)
            {
                try
                {
                    // cast para ser MdiClient
                    mdiClient = (MdiClient)var;

                    // atribui cor ao MdiClient
                    mdiClient.BackColor = this.BackColor;
                }
                catch (Exception)
                {
                    // Se cast falhar ignora o erro
                }
            }
        }

        /// <summary>
        /// abre o formulario de inicio
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void inicioToolStripMenuItem_Click(object sender, EventArgs e)
        {
            FormInicio formInicio = new FormInicio();
            formInicio.MdiParent = this;
            formInicio.Show();
            inicioToolStripMenuItem.Enabled = false;
            clienteToolStripMenuItem.Enabled = true;
            faturaToolStripMenuItem.Enabled = true;
            consumoToolStripMenuItem.Enabled = true;
            escalaoToolStripMenuItem.Enabled = true;
            sobreToolStripMenuItem.Enabled = true;
        }

        /// <summary>
        /// abre o formulario de cliente
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void clienteToolStripMenuItem_Click(object sender, EventArgs e)
        {
            FormCliente formCliente = new FormCliente();
            formCliente.MdiParent = this;
            formCliente.Show();
            inicioToolStripMenuItem.Enabled = true;
            clienteToolStripMenuItem.Enabled = false;
            faturaToolStripMenuItem.Enabled = true;
            consumoToolStripMenuItem.Enabled = true;
            escalaoToolStripMenuItem.Enabled = true;
            sobreToolStripMenuItem.Enabled = true;
        }

        /// <summary>
        /// abre o formulario das faturas
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void faturaToolStripMenuItem_Click(object sender, EventArgs e)
        {
            FormFatura formFatura = new FormFatura();
            formFatura.MdiParent = this;
            formFatura.Show();
            inicioToolStripMenuItem.Enabled = true;
            clienteToolStripMenuItem.Enabled = true;
            faturaToolStripMenuItem.Enabled = false;
            consumoToolStripMenuItem.Enabled = true;
            escalaoToolStripMenuItem.Enabled = true;
            sobreToolStripMenuItem.Enabled = true;
        }

        /// <summary>
        /// abre o formulario dos consumos
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void consumoToolStripMenuItem_Click(object sender, EventArgs e)
        {
            FormConsumo formConsumo = new FormConsumo();
            formConsumo.MdiParent = this;
            formConsumo.Show();
            inicioToolStripMenuItem.Enabled = true;
            clienteToolStripMenuItem.Enabled = true;
            faturaToolStripMenuItem.Enabled = true;
            consumoToolStripMenuItem.Enabled = false;
            escalaoToolStripMenuItem.Enabled = true;
            sobreToolStripMenuItem.Enabled = true;
        }

        /// <summary>
        /// abre o formulario do escalão
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void escalaoToolStripMenuItem_Click(object sender, EventArgs e)
        {
            FormEscalao formEscalao = new FormEscalao();
            formEscalao.MdiParent = this;
            formEscalao.Show();
            inicioToolStripMenuItem.Enabled = true;
            clienteToolStripMenuItem.Enabled = true;
            faturaToolStripMenuItem.Enabled = true;
            consumoToolStripMenuItem.Enabled = true;
            escalaoToolStripMenuItem.Enabled = false;
            sobreToolStripMenuItem.Enabled = true;
        }

        /// <summary>
        /// abre o formulario dos creditos
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void sobreToolStripMenuItem_Click(object sender, EventArgs e)
        {
            FormSobre formSobre = new FormSobre();
            formSobre.MdiParent = this;
            formSobre.Show();
            inicioToolStripMenuItem.Enabled = true;
            clienteToolStripMenuItem.Enabled = true;
            faturaToolStripMenuItem.Enabled = true;
            consumoToolStripMenuItem.Enabled = true;
            escalaoToolStripMenuItem.Enabled = true;
            sobreToolStripMenuItem.Enabled = false;
        }
    }
}

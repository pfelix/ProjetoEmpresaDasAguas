﻿namespace ProjetoEmpresaDasAguas
{
    partial class FormCliente
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.toolStrip1 = new System.Windows.Forms.ToolStrip();
            this.ToolStripButtonNovo = new System.Windows.Forms.ToolStripButton();
            this.ToolStripButtonGuardar = new System.Windows.Forms.ToolStripButton();
            this.ToolStripButtonAtualizar = new System.Windows.Forms.ToolStripButton();
            this.ToolStripButtonApagar = new System.Windows.Forms.ToolStripButton();
            this.ToolStripButtonProcurar = new System.Windows.Forms.ToolStripButton();
            this.toolStripSeparator1 = new System.Windows.Forms.ToolStripSeparator();
            this.ToolStripButtonTras = new System.Windows.Forms.ToolStripButton();
            this.ToolStripButtonFrente = new System.Windows.Forms.ToolStripButton();
            this.ToolStripButtonCancelar = new System.Windows.Forms.ToolStripButton();
            this.ListBoxFiltro = new System.Windows.Forms.ListBox();
            this.PanelCliente = new System.Windows.Forms.Panel();
            this.TextBoxId = new System.Windows.Forms.TextBox();
            this.ButtonProcuraCodPostal = new System.Windows.Forms.Button();
            this.ButtonProcuraNif = new System.Windows.Forms.Button();
            this.ButtonProcuraMorada = new System.Windows.Forms.Button();
            this.ButtonProcuraNome = new System.Windows.Forms.Button();
            this.ButtonProcuraId = new System.Windows.Forms.Button();
            this.label7 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.TextBoxPassword = new System.Windows.Forms.TextBox();
            this.TextBoxUser = new System.Windows.Forms.TextBox();
            this.TextBoxNif = new System.Windows.Forms.TextBox();
            this.TextBoxCodPostal = new System.Windows.Forms.TextBox();
            this.TextBoxMorada = new System.Windows.Forms.TextBox();
            this.TextBoxNome = new System.Windows.Forms.TextBox();
            this.toolStrip1.SuspendLayout();
            this.PanelCliente.SuspendLayout();
            this.SuspendLayout();
            // 
            // toolStrip1
            // 
            this.toolStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.ToolStripButtonNovo,
            this.ToolStripButtonGuardar,
            this.ToolStripButtonAtualizar,
            this.ToolStripButtonApagar,
            this.ToolStripButtonProcurar,
            this.toolStripSeparator1,
            this.ToolStripButtonTras,
            this.ToolStripButtonFrente,
            this.ToolStripButtonCancelar});
            this.toolStrip1.Location = new System.Drawing.Point(0, 0);
            this.toolStrip1.Name = "toolStrip1";
            this.toolStrip1.Size = new System.Drawing.Size(534, 25);
            this.toolStrip1.Stretch = true;
            this.toolStrip1.TabIndex = 15;
            this.toolStrip1.Text = "toolStrip1";
            // 
            // ToolStripButtonNovo
            // 
            this.ToolStripButtonNovo.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.ToolStripButtonNovo.Image = global::ProjetoEmpresaDasAguas.Properties.Resources.if_document_new_118910;
            this.ToolStripButtonNovo.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.ToolStripButtonNovo.Name = "ToolStripButtonNovo";
            this.ToolStripButtonNovo.Size = new System.Drawing.Size(23, 22);
            this.ToolStripButtonNovo.Text = "Novo";
            this.ToolStripButtonNovo.Click += new System.EventHandler(this.ToolStripButtonNovo_Click);
            // 
            // ToolStripButtonGuardar
            // 
            this.ToolStripButtonGuardar.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.ToolStripButtonGuardar.Enabled = false;
            this.ToolStripButtonGuardar.Image = global::ProjetoEmpresaDasAguas.Properties.Resources.if_save_173091;
            this.ToolStripButtonGuardar.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.ToolStripButtonGuardar.Name = "ToolStripButtonGuardar";
            this.ToolStripButtonGuardar.Size = new System.Drawing.Size(23, 22);
            this.ToolStripButtonGuardar.Text = "Guardar";
            this.ToolStripButtonGuardar.Click += new System.EventHandler(this.ToolStripButtonGuardar_Click);
            // 
            // ToolStripButtonAtualizar
            // 
            this.ToolStripButtonAtualizar.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.ToolStripButtonAtualizar.Enabled = false;
            this.ToolStripButtonAtualizar.Image = global::ProjetoEmpresaDasAguas.Properties.Resources.if_gtk_refresh_48111;
            this.ToolStripButtonAtualizar.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.ToolStripButtonAtualizar.Name = "ToolStripButtonAtualizar";
            this.ToolStripButtonAtualizar.Size = new System.Drawing.Size(23, 22);
            this.ToolStripButtonAtualizar.Text = "Atualizar";
            this.ToolStripButtonAtualizar.Click += new System.EventHandler(this.ToolStripButtonAtualizar_Click);
            // 
            // ToolStripButtonApagar
            // 
            this.ToolStripButtonApagar.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.ToolStripButtonApagar.Enabled = false;
            this.ToolStripButtonApagar.Image = global::ProjetoEmpresaDasAguas.Properties.Resources.if_trash_115789;
            this.ToolStripButtonApagar.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.ToolStripButtonApagar.Name = "ToolStripButtonApagar";
            this.ToolStripButtonApagar.Size = new System.Drawing.Size(23, 22);
            this.ToolStripButtonApagar.Text = "Apagar";
            this.ToolStripButtonApagar.Click += new System.EventHandler(this.ToolStripButtonApagar_Click);
            // 
            // ToolStripButtonProcurar
            // 
            this.ToolStripButtonProcurar.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.ToolStripButtonProcurar.Image = global::ProjetoEmpresaDasAguas.Properties.Resources.if_system_search_118797__1_;
            this.ToolStripButtonProcurar.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.ToolStripButtonProcurar.Name = "ToolStripButtonProcurar";
            this.ToolStripButtonProcurar.Size = new System.Drawing.Size(23, 22);
            this.ToolStripButtonProcurar.Text = "Procurar";
            this.ToolStripButtonProcurar.Click += new System.EventHandler(this.ToolStripButtonProcurar_Click);
            // 
            // toolStripSeparator1
            // 
            this.toolStripSeparator1.Name = "toolStripSeparator1";
            this.toolStripSeparator1.Size = new System.Drawing.Size(6, 25);
            // 
            // ToolStripButtonTras
            // 
            this.ToolStripButtonTras.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.ToolStripButtonTras.Image = global::ProjetoEmpresaDasAguas.Properties.Resources.if_gtk_go_back_ltr_48100;
            this.ToolStripButtonTras.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.ToolStripButtonTras.Name = "ToolStripButtonTras";
            this.ToolStripButtonTras.Size = new System.Drawing.Size(23, 22);
            this.ToolStripButtonTras.Text = "Trás";
            this.ToolStripButtonTras.Click += new System.EventHandler(this.ToolStripButtonTras_Click);
            // 
            // ToolStripButtonFrente
            // 
            this.ToolStripButtonFrente.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.ToolStripButtonFrente.Image = global::ProjetoEmpresaDasAguas.Properties.Resources.if_gtk_go_back_rtl_48101;
            this.ToolStripButtonFrente.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.ToolStripButtonFrente.Name = "ToolStripButtonFrente";
            this.ToolStripButtonFrente.Size = new System.Drawing.Size(23, 22);
            this.ToolStripButtonFrente.Text = "Frente";
            this.ToolStripButtonFrente.Click += new System.EventHandler(this.ToolStripButtonFrente_Click);
            // 
            // ToolStripButtonCancelar
            // 
            this.ToolStripButtonCancelar.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.ToolStripButtonCancelar.Enabled = false;
            this.ToolStripButtonCancelar.Image = global::ProjetoEmpresaDasAguas.Properties.Resources.if_f_cross_256_282471;
            this.ToolStripButtonCancelar.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.ToolStripButtonCancelar.Name = "ToolStripButtonCancelar";
            this.ToolStripButtonCancelar.Size = new System.Drawing.Size(23, 22);
            this.ToolStripButtonCancelar.Text = "Cancelar";
            this.ToolStripButtonCancelar.Click += new System.EventHandler(this.ToolStripButtonCancelar_Click);
            // 
            // ListBoxFiltro
            // 
            this.ListBoxFiltro.FormattingEnabled = true;
            this.ListBoxFiltro.Location = new System.Drawing.Point(385, 57);
            this.ListBoxFiltro.Name = "ListBoxFiltro";
            this.ListBoxFiltro.ScrollAlwaysVisible = true;
            this.ListBoxFiltro.Size = new System.Drawing.Size(140, 173);
            this.ListBoxFiltro.TabIndex = 16;
            this.ListBoxFiltro.SelectedIndexChanged += new System.EventHandler(this.ListBoxFiltro_SelectedIndexChanged);
            // 
            // PanelCliente
            // 
            this.PanelCliente.Controls.Add(this.TextBoxId);
            this.PanelCliente.Controls.Add(this.ButtonProcuraCodPostal);
            this.PanelCliente.Controls.Add(this.ButtonProcuraNif);
            this.PanelCliente.Controls.Add(this.ButtonProcuraMorada);
            this.PanelCliente.Controls.Add(this.ButtonProcuraNome);
            this.PanelCliente.Controls.Add(this.ButtonProcuraId);
            this.PanelCliente.Controls.Add(this.label7);
            this.PanelCliente.Controls.Add(this.label6);
            this.PanelCliente.Controls.Add(this.label5);
            this.PanelCliente.Controls.Add(this.label4);
            this.PanelCliente.Controls.Add(this.label3);
            this.PanelCliente.Controls.Add(this.label2);
            this.PanelCliente.Controls.Add(this.label1);
            this.PanelCliente.Controls.Add(this.TextBoxPassword);
            this.PanelCliente.Controls.Add(this.TextBoxUser);
            this.PanelCliente.Controls.Add(this.TextBoxNif);
            this.PanelCliente.Controls.Add(this.TextBoxCodPostal);
            this.PanelCliente.Controls.Add(this.TextBoxMorada);
            this.PanelCliente.Controls.Add(this.TextBoxNome);
            this.PanelCliente.Enabled = false;
            this.PanelCliente.Location = new System.Drawing.Point(9, 33);
            this.PanelCliente.Name = "PanelCliente";
            this.PanelCliente.Size = new System.Drawing.Size(367, 272);
            this.PanelCliente.TabIndex = 17;
            // 
            // TextBoxId
            // 
            this.TextBoxId.Location = new System.Drawing.Point(9, 21);
            this.TextBoxId.Name = "TextBoxId";
            this.TextBoxId.Size = new System.Drawing.Size(100, 20);
            this.TextBoxId.TabIndex = 41;
            // 
            // ButtonProcuraCodPostal
            // 
            this.ButtonProcuraCodPostal.Image = global::ProjetoEmpresaDasAguas.Properties.Resources.if_system_search_118797__1_;
            this.ButtonProcuraCodPostal.Location = new System.Drawing.Point(169, 128);
            this.ButtonProcuraCodPostal.Name = "ButtonProcuraCodPostal";
            this.ButtonProcuraCodPostal.Size = new System.Drawing.Size(25, 25);
            this.ButtonProcuraCodPostal.TabIndex = 40;
            this.ButtonProcuraCodPostal.UseVisualStyleBackColor = true;
            this.ButtonProcuraCodPostal.Click += new System.EventHandler(this.ButtonProcuraCodPostal_Click);
            // 
            // ButtonProcuraNif
            // 
            this.ButtonProcuraNif.Image = global::ProjetoEmpresaDasAguas.Properties.Resources.if_system_search_118797__1_;
            this.ButtonProcuraNif.Location = new System.Drawing.Point(333, 128);
            this.ButtonProcuraNif.Name = "ButtonProcuraNif";
            this.ButtonProcuraNif.Size = new System.Drawing.Size(25, 25);
            this.ButtonProcuraNif.TabIndex = 39;
            this.ButtonProcuraNif.UseVisualStyleBackColor = true;
            this.ButtonProcuraNif.Click += new System.EventHandler(this.ButtonProcuraNif_Click);
            // 
            // ButtonProcuraMorada
            // 
            this.ButtonProcuraMorada.Image = global::ProjetoEmpresaDasAguas.Properties.Resources.if_system_search_118797__1_;
            this.ButtonProcuraMorada.Location = new System.Drawing.Point(333, 91);
            this.ButtonProcuraMorada.Name = "ButtonProcuraMorada";
            this.ButtonProcuraMorada.Size = new System.Drawing.Size(25, 25);
            this.ButtonProcuraMorada.TabIndex = 38;
            this.ButtonProcuraMorada.UseVisualStyleBackColor = true;
            this.ButtonProcuraMorada.Click += new System.EventHandler(this.ButtonProcuraMorada_Click);
            // 
            // ButtonProcuraNome
            // 
            this.ButtonProcuraNome.Image = global::ProjetoEmpresaDasAguas.Properties.Resources.if_system_search_118797__1_;
            this.ButtonProcuraNome.Location = new System.Drawing.Point(333, 54);
            this.ButtonProcuraNome.Name = "ButtonProcuraNome";
            this.ButtonProcuraNome.Size = new System.Drawing.Size(25, 25);
            this.ButtonProcuraNome.TabIndex = 37;
            this.ButtonProcuraNome.UseVisualStyleBackColor = true;
            this.ButtonProcuraNome.Click += new System.EventHandler(this.ButtonProcuraNome_Click);
            // 
            // ButtonProcuraId
            // 
            this.ButtonProcuraId.Image = global::ProjetoEmpresaDasAguas.Properties.Resources.if_system_search_118797__1_;
            this.ButtonProcuraId.Location = new System.Drawing.Point(112, 17);
            this.ButtonProcuraId.Name = "ButtonProcuraId";
            this.ButtonProcuraId.Size = new System.Drawing.Size(25, 25);
            this.ButtonProcuraId.TabIndex = 36;
            this.ButtonProcuraId.UseVisualStyleBackColor = true;
            this.ButtonProcuraId.Click += new System.EventHandler(this.ButtonProcuraId_Click);
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label7.ForeColor = System.Drawing.SystemColors.HotTrack;
            this.label7.Location = new System.Drawing.Point(201, 155);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(61, 13);
            this.label7.TabIndex = 35;
            this.label7.Text = "Password";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label6.ForeColor = System.Drawing.SystemColors.HotTrack;
            this.label6.Location = new System.Drawing.Point(9, 155);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(33, 13);
            this.label6.TabIndex = 34;
            this.label6.Text = "User";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label5.ForeColor = System.Drawing.SystemColors.HotTrack;
            this.label5.Location = new System.Drawing.Point(201, 118);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(27, 13);
            this.label5.TabIndex = 33;
            this.label5.Text = "NIF";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.ForeColor = System.Drawing.SystemColors.HotTrack;
            this.label4.Location = new System.Drawing.Point(9, 118);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(85, 13);
            this.label4.TabIndex = 32;
            this.label4.Text = "Código Postal";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.ForeColor = System.Drawing.SystemColors.HotTrack;
            this.label3.Location = new System.Drawing.Point(9, 81);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(49, 13);
            this.label3.TabIndex = 31;
            this.label3.Text = "Morada";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.ForeColor = System.Drawing.SystemColors.HotTrack;
            this.label2.Location = new System.Drawing.Point(9, 44);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(39, 13);
            this.label2.TabIndex = 30;
            this.label2.Text = "Nome";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.ForeColor = System.Drawing.SystemColors.HotTrack;
            this.label1.Location = new System.Drawing.Point(9, 6);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(61, 13);
            this.label1.TabIndex = 29;
            this.label1.Text = "Id Cliente";
            // 
            // TextBoxPassword
            // 
            this.TextBoxPassword.Location = new System.Drawing.Point(201, 170);
            this.TextBoxPassword.Name = "TextBoxPassword";
            this.TextBoxPassword.Size = new System.Drawing.Size(128, 20);
            this.TextBoxPassword.TabIndex = 28;
            // 
            // TextBoxUser
            // 
            this.TextBoxUser.Location = new System.Drawing.Point(9, 170);
            this.TextBoxUser.Name = "TextBoxUser";
            this.TextBoxUser.Size = new System.Drawing.Size(156, 20);
            this.TextBoxUser.TabIndex = 27;
            // 
            // TextBoxNif
            // 
            this.TextBoxNif.Location = new System.Drawing.Point(201, 133);
            this.TextBoxNif.Name = "TextBoxNif";
            this.TextBoxNif.Size = new System.Drawing.Size(128, 20);
            this.TextBoxNif.TabIndex = 26;
            // 
            // TextBoxCodPostal
            // 
            this.TextBoxCodPostal.Location = new System.Drawing.Point(9, 133);
            this.TextBoxCodPostal.Name = "TextBoxCodPostal";
            this.TextBoxCodPostal.Size = new System.Drawing.Size(156, 20);
            this.TextBoxCodPostal.TabIndex = 25;
            // 
            // TextBoxMorada
            // 
            this.TextBoxMorada.Location = new System.Drawing.Point(9, 96);
            this.TextBoxMorada.Name = "TextBoxMorada";
            this.TextBoxMorada.Size = new System.Drawing.Size(320, 20);
            this.TextBoxMorada.TabIndex = 24;
            // 
            // TextBoxNome
            // 
            this.TextBoxNome.Location = new System.Drawing.Point(9, 59);
            this.TextBoxNome.Name = "TextBoxNome";
            this.TextBoxNome.Size = new System.Drawing.Size(320, 20);
            this.TextBoxNome.TabIndex = 23;
            // 
            // FormCliente
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(534, 311);
            this.Controls.Add(this.PanelCliente);
            this.Controls.Add(this.ListBoxFiltro);
            this.Controls.Add(this.toolStrip1);
            this.Name = "FormCliente";
            this.Text = "Cliente";
            this.WindowState = System.Windows.Forms.FormWindowState.Maximized;
            this.toolStrip1.ResumeLayout(false);
            this.toolStrip1.PerformLayout();
            this.PanelCliente.ResumeLayout(false);
            this.PanelCliente.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        private System.Windows.Forms.ToolStrip toolStrip1;
        private System.Windows.Forms.ToolStripButton ToolStripButtonNovo;
        private System.Windows.Forms.ToolStripButton ToolStripButtonGuardar;
        private System.Windows.Forms.ToolStripButton ToolStripButtonApagar;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator1;
        private System.Windows.Forms.ToolStripButton ToolStripButtonTras;
        private System.Windows.Forms.ToolStripButton ToolStripButtonFrente;
        private System.Windows.Forms.ListBox ListBoxFiltro;
        private System.Windows.Forms.ToolStripButton ToolStripButtonAtualizar;
        private System.Windows.Forms.ToolStripButton ToolStripButtonCancelar;
        private System.Windows.Forms.ToolStripButton ToolStripButtonProcurar;
        private System.Windows.Forms.Panel PanelCliente;
        private System.Windows.Forms.TextBox TextBoxId;
        private System.Windows.Forms.Button ButtonProcuraCodPostal;
        private System.Windows.Forms.Button ButtonProcuraNif;
        private System.Windows.Forms.Button ButtonProcuraMorada;
        private System.Windows.Forms.Button ButtonProcuraNome;
        private System.Windows.Forms.Button ButtonProcuraId;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox TextBoxPassword;
        private System.Windows.Forms.TextBox TextBoxUser;
        private System.Windows.Forms.TextBox TextBoxNif;
        private System.Windows.Forms.TextBox TextBoxCodPostal;
        private System.Windows.Forms.TextBox TextBoxMorada;
        private System.Windows.Forms.TextBox TextBoxNome;
    }
}
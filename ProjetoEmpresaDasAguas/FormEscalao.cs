﻿namespace ProjetoEmpresaDasAguas
{
    using LivrariaDeClasses;
    using System;
    using System.Collections.Generic;
    using System.Data;
    using System.Linq;
    using System.Windows.Forms;

    public partial class FormEscalao : Form
    {
        CRUDEscalao _CRUDEscalao;
        List<Escalao> lista;

        bool novo = false;

        public FormEscalao()
        {
            InitializeComponent();
            ControlBox = false;
            _CRUDEscalao = new CRUDEscalao();

            lista = _CRUDEscalao.ReadEscalao();

            //selecionar a linha completa
            ListViewFiltro.View = View.Details;
            ListViewFiltro.FullRowSelect = true;

            CarregarListView();

        }
        

        #region metodos

        /// <summary>
        /// carregar dados na listview
        /// </summary>
        private void CarregarListView()
        {
            ListViewFiltro.Clear();

            //Criar as colunas
            ListViewFiltro.Columns.Add("Id Escalao");
            ListViewFiltro.Columns.Add("Minimo");
            ListViewFiltro.Columns.Add("Valor Unitário");

            var listas = from escaloes in _CRUDEscalao.ReadEscalao() orderby escaloes.minimo ascending select escaloes;
            foreach (var escalao in listas)
            {
                ListViewItem item;

                item = ListViewFiltro.Items.Add(escalao.idEscalao.ToString());
                item.SubItems.Add(escalao.minimo.ToString());
                item.SubItems.Add($"" + escalao.valorUnitario.ToString() + " Euros");
            }


            for (int idx = 0; idx <= 2; idx++)
            {
                ListViewFiltro.Columns[idx].AutoResize(ColumnHeaderAutoResizeStyle.HeaderSize);
            }

            //procura o primeiro e mete nas textbox
            Escalao temp = (from escaloes in _CRUDEscalao.ReadEscalao() orderby escaloes.idEscalao ascending select escaloes).First();

            TextBoxId.Text = temp.idEscalao.ToString();
            TextBoxMinimo.Text = temp.minimo.ToString();
            TextBoxValor.Text = temp.valorUnitario.ToString();

            lista = null;
            lista = _CRUDEscalao.ReadEscalao();
        }


        /// <summary>
        /// carregar a listview
        /// </summary>
        /// <param name="idEscalao"></param>
        private void CarregarListViewEscalao(int idEscalao)
        {

            ListViewFiltro.Clear();

            //Criar as colunas
            ListViewFiltro.Columns.Add("Id Escalao");
            ListViewFiltro.Columns.Add("Minimo");
            ListViewFiltro.Columns.Add("Valor Unitário");

            var listas = from escaloes in _CRUDEscalao.ReadEscalao() where escaloes.idEscalao == idEscalao select escaloes;

            foreach (var escalao in listas)
            {
                ListViewItem item;

                item = ListViewFiltro.Items.Add(escalao.idEscalao.ToString());
                item.SubItems.Add(escalao.minimo.ToString());
                item.SubItems.Add($"" + escalao.valorUnitario.ToString() + " Euros");

            }


            for (int idx = 0; idx <= 2; idx++)
            {
                ListViewFiltro.Columns[idx].AutoResize(ColumnHeaderAutoResizeStyle.HeaderSize);
            }


        }

        

        /// <summary>
        /// carregar as textbox
        /// </summary>
        private void ClearTextBox()
        {
            TextBoxId.Clear();
            TextBoxMinimo.Clear();
            TextBoxValor.Clear(); ;
        }

        #endregion

        #region eventos

        /// <summary>
        /// evento de click na listview
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void ListViewFiltro_MouseClick(object sender, MouseEventArgs e)
        {
            int idEscalao = Convert.ToInt32(ListViewFiltro.SelectedItems[0].Text);

            ToolStripButtonUpdate.Enabled = true;
            ToolStripButtonDelete.Enabled = true;

            Escalao temp = _CRUDEscalao.ReadEscalaoPorId(idEscalao);

            TextBoxId.Text = temp.idEscalao.ToString();
            TextBoxMinimo.Text = temp.minimo.ToString();
            TextBoxValor.Text = temp.valorUnitario.ToString();

        }

        /// <summary>
        /// pesquisar pelo escalão
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void ButtonPesquisaIdEscalao_Click(object sender, EventArgs e)
        {
            int id = 0;

            if (Validacoes.ValidacaoInt(TextBoxId.Text))
            {
                MessageBox.Show("Não existe nenhum dado para procurar.");
                return;
            }

            id = Convert.ToInt32(TextBoxId.Text);

            Escalao escalao = _CRUDEscalao.ReadEscalaoPorId(id);


            if (escalao == null)
            {
                MessageBox.Show($"Não existe nenhum escalao com o id {id}");
                ClearTextBox();
                return;
            }


            TextBoxId.Text = escalao.idEscalao.ToString();
            TextBoxMinimo.Text = escalao.minimo.ToString();
            TextBoxValor.Text = escalao.valorUnitario.ToString();
            ToolStripButtonCancel.Enabled = false;

            panel1.Enabled = false;
        }

        #endregion
        
        #region toolstrip


        /// <summary>
        /// butão para criar novo escalao
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void ToolStripButtonNew_Click(object sender, EventArgs e)
        {
            panel1.Enabled = true;

            ToolStripButtonSave.Enabled = true;
            TextBoxId.Enabled = false;
            ToolStripButtonUpdate.Enabled = false;
            ToolStripButtonDelete.Enabled = false;
            ToolStripButtonCancel.Enabled = true;
            ToolStripButtonNew.Enabled = false;

            ClearTextBox(); //limpa as TextBox

            novo = true;
        }

        /// <summary>
        /// butão para gravar a ciração de novo escalão e atualização de escalão
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void ToolStripButtonSave_Click(object sender, EventArgs e)
        {
            bool erro = false;
            string mensagem = null;


            if (Validacoes.ValidacaoInt(TextBoxMinimo.Text))
            {
                mensagem = "Insira um Valor minimo do escalão!\n";
                erro = true;
            }


            if (Validacoes.ValidacaoDouble(TextBoxValor.Text))
            {
                mensagem += "insira um valor para esse escalão!\n";
                erro = true;
            }

            if (erro)
            {
                MessageBox.Show(mensagem);
                return;
            }

            Escalao escalao = new Escalao
            {
                valorUnitario = Convert.ToDouble(TextBoxValor.Text),
                minimo = Convert.ToInt32(TextBoxMinimo.Text)
            };

            //inserir um novo
            if (novo)
            {
                if (!_CRUDEscalao.CreateEscalao(escalao))
                {
                    MessageBox.Show(_CRUDEscalao.Mensagem);
                    return;
                }

                MessageBox.Show($"Escalão inserido com sucesso!");

            }
            //atualizar um existente
            else
            {
                Escalao UpdateEscalao = new Escalao
                {
                    idEscalao = Convert.ToInt32(TextBoxId.Text),
                    valorUnitario = Convert.ToDouble(TextBoxValor.Text),
                    minimo = Convert.ToInt32(TextBoxMinimo.Text)
                };

                if (!_CRUDEscalao.UpdateEscalao(UpdateEscalao))
                {
                    MessageBox.Show(_CRUDEscalao.Mensagem);
                    return;
                }

                MessageBox.Show($"escalão actualizado com sucesso!");

            }

            ClearTextBox();

            panel1.Enabled = false;
            ToolStripButtonSave.Enabled = false;
            ToolStripButtonCancel.Enabled = false;
            ToolStripButtonUpdate.Enabled = false;
            ToolStripButtonDelete.Enabled = false;
            ToolStripButtonNew.Enabled = true;
            CarregarListView();

        }
        

        /// <summary>
        /// butão para ativar a actualização
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void ToolStripButtonUpdate_Click(object sender, EventArgs e)
        {
            panel1.Enabled = true;
            ToolStripButtonCancel.Enabled = true;
            ToolStripButtonDelete.Enabled = false;
            ToolStripButtonSave.Enabled = true;
            ToolStripButtonNew.Enabled = false;
            ToolStripButtonSearch.Enabled = false;
            novo = false;
        }


        /// <summary>
        /// butão para apagar um escalão
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void ToolStripButtonDelete_Click(object sender, EventArgs e)
        {
            if (MessageBox.Show("Eliminar?", "Eliminar", MessageBoxButtons.OKCancel) == DialogResult.OK)
            {
                if (string.IsNullOrEmpty(TextBoxId.Text))
                {
                    MessageBox.Show("Escolha um escalão para apagar");
                    return;
                }

                if (!_CRUDEscalao.DeleteEscalao(Convert.ToInt32(TextBoxId.Text)))
                {
                    MessageBox.Show(_CRUDEscalao.Mensagem);
                    return;
                }
                MessageBox.Show($"Escalão eliminado com sucesso!");

                ClearTextBox();

                panel1.Enabled = false;

                ListViewFiltro.Clear();
                CarregarListView();
                ToolStripButtonCancel.Enabled = false;
            }
            else
            {
                return;
            }
        }


        /// <summary>
        /// butão de procura
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void ToolStripButtonSearch_Click(object sender, EventArgs e)
        {
            panel1.Enabled = true;
            ToolStripButtonCancel.Enabled = true;
        }

        //variavel de contagem para botoes proximo e anterior
        int i = 0;


        /// <summary>
        /// mostra escalão anterior
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void ToolStripButtonReturn_Click(object sender, EventArgs e)
        {
            lista = (from lista in lista select lista).ToList();

            if (i == -1)
            {
                i = lista.Count - 1;
            }

            TextBoxId.Text = lista[i].idEscalao.ToString();
            TextBoxMinimo.Text = lista[i].minimo.ToString();
            TextBoxValor.Text = lista[i].valorUnitario.ToString();

            i--;
        }

        /// <summary>
        /// mortra proximo escalão
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void ToolStripButtonNext_Click(object sender, EventArgs e)
        {
            lista = (from lista in lista select lista).ToList();

            if (i == lista.Count)
            {
                i = 0;
            }

            TextBoxId.Text = lista[i].idEscalao.ToString();
            TextBoxMinimo.Text = lista[i].minimo.ToString();
            TextBoxValor.Text = lista[i].valorUnitario.ToString();

            i++;
        }

        /// <summary>
        /// butão para cancelar a acção
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void ToolStripButtonCancel_Click(object sender, EventArgs e)
        {
            if (novo)
            {
                if (MessageBox.Show("Cancelar?", "Cancelar", MessageBoxButtons.OKCancel) == DialogResult.OK)
                {

                    ClearTextBox();

                    ToolStripButtonCancel.Enabled = false;
                    ToolStripButtonDelete.Enabled = false;
                    ToolStripButtonNew.Enabled = true;
                    ToolStripButtonSave.Enabled = false;
                    panel1.Enabled = false;
                    //ListViewFiltro.Items.Clear();
                    CarregarListView();
                    ToolStripButtonCancel.Enabled = false;

                }
                else
                {
                    return;
                }
            }
            else
            {
                // Verifica se alguma das textbox tem algum texto
                if (!string.IsNullOrEmpty(TextBoxMinimo.Text) || !string.IsNullOrEmpty(TextBoxValor.Text))


                    if (MessageBox.Show("Cancelar?", "Cancelar", MessageBoxButtons.OKCancel) == DialogResult.OK)
                    {

                        ClearTextBox();

                        ToolStripButtonCancel.Enabled = false;
                        ToolStripButtonDelete.Enabled = false;
                        ToolStripButtonNew.Enabled = true;
                        ToolStripButtonSave.Enabled = false;
                        panel1.Enabled = false;
                        //ListViewFiltro.Items.Clear();
                        CarregarListView();
                        ToolStripButtonCancel.Enabled = false;

                    }
                    else
                    {
                        return;
                    }
            }
        }

        #endregion
    }
}

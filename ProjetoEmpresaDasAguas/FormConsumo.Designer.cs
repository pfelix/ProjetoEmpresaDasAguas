﻿namespace ProjetoEmpresaDasAguas
{
    partial class FormConsumo
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FormConsumo));
            this.toolStrip1 = new System.Windows.Forms.ToolStrip();
            this.ToolStripButtonNew = new System.Windows.Forms.ToolStripButton();
            this.ToolStripButtonSave = new System.Windows.Forms.ToolStripButton();
            this.ToolStripButtonUpdate = new System.Windows.Forms.ToolStripButton();
            this.ToolStripButtonDelete = new System.Windows.Forms.ToolStripButton();
            this.ToolStripButtonSearch = new System.Windows.Forms.ToolStripButton();
            this.toolStripSeparator1 = new System.Windows.Forms.ToolStripSeparator();
            this.ToolStripButtonReturn = new System.Windows.Forms.ToolStripButton();
            this.ToolStripButtonNext = new System.Windows.Forms.ToolStripButton();
            this.ToolStripButtonCancel = new System.Windows.Forms.ToolStripButton();
            this.panel1 = new System.Windows.Forms.Panel();
            this.ComboBoxCliente = new System.Windows.Forms.ComboBox();
            this.DateTimePickerData = new System.Windows.Forms.DateTimePicker();
            this.label4 = new System.Windows.Forms.Label();
            this.TextBoxContagem = new System.Windows.Forms.TextBox();
            this.TextBoxId = new System.Windows.Forms.TextBox();
            this.ButtonPesquisaData = new System.Windows.Forms.Button();
            this.ButtonPesquisaIdCliente = new System.Windows.Forms.Button();
            this.ButtonPesquisaIdConsumo = new System.Windows.Forms.Button();
            this.label3 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.TreeViewFiltro = new System.Windows.Forms.TreeView();
            this.imageList1 = new System.Windows.Forms.ImageList(this.components);
            this.ListViewFiltro = new System.Windows.Forms.ListView();
            this.label5 = new System.Windows.Forms.Label();
            this.toolStrip1.SuspendLayout();
            this.panel1.SuspendLayout();
            this.SuspendLayout();
            // 
            // toolStrip1
            // 
            this.toolStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.ToolStripButtonNew,
            this.ToolStripButtonSave,
            this.ToolStripButtonUpdate,
            this.ToolStripButtonDelete,
            this.ToolStripButtonSearch,
            this.toolStripSeparator1,
            this.ToolStripButtonReturn,
            this.ToolStripButtonNext,
            this.ToolStripButtonCancel});
            this.toolStrip1.Location = new System.Drawing.Point(0, 0);
            this.toolStrip1.Name = "toolStrip1";
            this.toolStrip1.Size = new System.Drawing.Size(534, 25);
            this.toolStrip1.Stretch = true;
            this.toolStrip1.TabIndex = 16;
            this.toolStrip1.Text = "toolStrip1";
            // 
            // ToolStripButtonNew
            // 
            this.ToolStripButtonNew.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.ToolStripButtonNew.Image = global::ProjetoEmpresaDasAguas.Properties.Resources.if_document_new_118910;
            this.ToolStripButtonNew.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.ToolStripButtonNew.Name = "ToolStripButtonNew";
            this.ToolStripButtonNew.Size = new System.Drawing.Size(23, 22);
            this.ToolStripButtonNew.Text = "Novo";
            this.ToolStripButtonNew.Click += new System.EventHandler(this.ToolStripButtonNew_Click);
            // 
            // ToolStripButtonSave
            // 
            this.ToolStripButtonSave.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.ToolStripButtonSave.Enabled = false;
            this.ToolStripButtonSave.Image = global::ProjetoEmpresaDasAguas.Properties.Resources.if_save_173091;
            this.ToolStripButtonSave.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.ToolStripButtonSave.Name = "ToolStripButtonSave";
            this.ToolStripButtonSave.Size = new System.Drawing.Size(23, 22);
            this.ToolStripButtonSave.Text = "Gravar";
            this.ToolStripButtonSave.Click += new System.EventHandler(this.ToolStripButtonSave_Click);
            // 
            // ToolStripButtonUpdate
            // 
            this.ToolStripButtonUpdate.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.ToolStripButtonUpdate.Enabled = false;
            this.ToolStripButtonUpdate.Image = global::ProjetoEmpresaDasAguas.Properties.Resources.if_gtk_refresh_48111;
            this.ToolStripButtonUpdate.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.ToolStripButtonUpdate.Name = "ToolStripButtonUpdate";
            this.ToolStripButtonUpdate.Size = new System.Drawing.Size(23, 22);
            this.ToolStripButtonUpdate.Text = "Atualizar";
            this.ToolStripButtonUpdate.Click += new System.EventHandler(this.ToolStripButtonUpdate_Click);
            // 
            // ToolStripButtonDelete
            // 
            this.ToolStripButtonDelete.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.ToolStripButtonDelete.DoubleClickEnabled = true;
            this.ToolStripButtonDelete.Enabled = false;
            this.ToolStripButtonDelete.Image = global::ProjetoEmpresaDasAguas.Properties.Resources.if_trash_115789;
            this.ToolStripButtonDelete.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.ToolStripButtonDelete.Name = "ToolStripButtonDelete";
            this.ToolStripButtonDelete.Size = new System.Drawing.Size(23, 22);
            this.ToolStripButtonDelete.Text = "Eliminar";
            this.ToolStripButtonDelete.Click += new System.EventHandler(this.ToolStripButtonDelete_Click);
            // 
            // ToolStripButtonSearch
            // 
            this.ToolStripButtonSearch.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.ToolStripButtonSearch.Image = global::ProjetoEmpresaDasAguas.Properties.Resources.if_system_search_118797__1_;
            this.ToolStripButtonSearch.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.ToolStripButtonSearch.Name = "ToolStripButtonSearch";
            this.ToolStripButtonSearch.Size = new System.Drawing.Size(23, 22);
            this.ToolStripButtonSearch.Text = "Pesquisar";
            this.ToolStripButtonSearch.Click += new System.EventHandler(this.ToolStripButtonSearch_Click);
            // 
            // toolStripSeparator1
            // 
            this.toolStripSeparator1.Name = "toolStripSeparator1";
            this.toolStripSeparator1.Size = new System.Drawing.Size(6, 25);
            // 
            // ToolStripButtonReturn
            // 
            this.ToolStripButtonReturn.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.ToolStripButtonReturn.Image = global::ProjetoEmpresaDasAguas.Properties.Resources.if_gtk_go_back_ltr_48100;
            this.ToolStripButtonReturn.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.ToolStripButtonReturn.Name = "ToolStripButtonReturn";
            this.ToolStripButtonReturn.Size = new System.Drawing.Size(23, 22);
            this.ToolStripButtonReturn.Text = "Anterior";
            this.ToolStripButtonReturn.Click += new System.EventHandler(this.ToolStripButtonReturn_Click);
            // 
            // ToolStripButtonNext
            // 
            this.ToolStripButtonNext.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.ToolStripButtonNext.Image = global::ProjetoEmpresaDasAguas.Properties.Resources.if_gtk_go_back_rtl_48101;
            this.ToolStripButtonNext.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.ToolStripButtonNext.Name = "ToolStripButtonNext";
            this.ToolStripButtonNext.Size = new System.Drawing.Size(23, 22);
            this.ToolStripButtonNext.Text = "Próximo";
            this.ToolStripButtonNext.Click += new System.EventHandler(this.ToolStripButtonNext_Click);
            // 
            // ToolStripButtonCancel
            // 
            this.ToolStripButtonCancel.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.ToolStripButtonCancel.Enabled = false;
            this.ToolStripButtonCancel.Image = global::ProjetoEmpresaDasAguas.Properties.Resources.if_f_cross_256_282471;
            this.ToolStripButtonCancel.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.ToolStripButtonCancel.Name = "ToolStripButtonCancel";
            this.ToolStripButtonCancel.Size = new System.Drawing.Size(23, 22);
            this.ToolStripButtonCancel.Text = "Cancelar";
            this.ToolStripButtonCancel.Click += new System.EventHandler(this.ToolStripButtonCancel_Click);
            // 
            // panel1
            // 
            this.panel1.Controls.Add(this.ComboBoxCliente);
            this.panel1.Controls.Add(this.DateTimePickerData);
            this.panel1.Controls.Add(this.label4);
            this.panel1.Controls.Add(this.TextBoxContagem);
            this.panel1.Controls.Add(this.TextBoxId);
            this.panel1.Controls.Add(this.ButtonPesquisaData);
            this.panel1.Controls.Add(this.ButtonPesquisaIdCliente);
            this.panel1.Controls.Add(this.ButtonPesquisaIdConsumo);
            this.panel1.Controls.Add(this.label3);
            this.panel1.Controls.Add(this.label2);
            this.panel1.Controls.Add(this.label1);
            this.panel1.Enabled = false;
            this.panel1.Location = new System.Drawing.Point(1, 28);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(366, 163);
            this.panel1.TabIndex = 17;
            // 
            // ComboBoxCliente
            // 
            this.ComboBoxCliente.FormattingEnabled = true;
            this.ComboBoxCliente.Location = new System.Drawing.Point(5, 57);
            this.ComboBoxCliente.Name = "ComboBoxCliente";
            this.ComboBoxCliente.Size = new System.Drawing.Size(318, 21);
            this.ComboBoxCliente.TabIndex = 60;
            // 
            // DateTimePickerData
            // 
            this.DateTimePickerData.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.DateTimePickerData.Location = new System.Drawing.Point(5, 97);
            this.DateTimePickerData.MinDate = new System.DateTime(1900, 1, 1, 0, 0, 0, 0);
            this.DateTimePickerData.Name = "DateTimePickerData";
            this.DateTimePickerData.Size = new System.Drawing.Size(318, 20);
            this.DateTimePickerData.TabIndex = 58;
            this.DateTimePickerData.Value = new System.DateTime(2018, 1, 28, 0, 0, 0, 0);
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.ForeColor = System.Drawing.SystemColors.HotTrack;
            this.label4.Location = new System.Drawing.Point(8, 122);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(63, 13);
            this.label4.TabIndex = 57;
            this.label4.Text = "Contagem";
            // 
            // TextBoxContagem
            // 
            this.TextBoxContagem.Location = new System.Drawing.Point(5, 138);
            this.TextBoxContagem.Name = "TextBoxContagem";
            this.TextBoxContagem.Size = new System.Drawing.Size(320, 20);
            this.TextBoxContagem.TabIndex = 56;
            // 
            // TextBoxId
            // 
            this.TextBoxId.Location = new System.Drawing.Point(5, 21);
            this.TextBoxId.Name = "TextBoxId";
            this.TextBoxId.Size = new System.Drawing.Size(100, 20);
            this.TextBoxId.TabIndex = 55;
            // 
            // ButtonPesquisaData
            // 
            this.ButtonPesquisaData.Image = global::ProjetoEmpresaDasAguas.Properties.Resources.if_system_search_118797__1_;
            this.ButtonPesquisaData.Location = new System.Drawing.Point(329, 93);
            this.ButtonPesquisaData.Name = "ButtonPesquisaData";
            this.ButtonPesquisaData.Size = new System.Drawing.Size(25, 25);
            this.ButtonPesquisaData.TabIndex = 54;
            this.ButtonPesquisaData.UseVisualStyleBackColor = true;
            this.ButtonPesquisaData.Click += new System.EventHandler(this.ButtonPesquisaData_Click);
            // 
            // ButtonPesquisaIdCliente
            // 
            this.ButtonPesquisaIdCliente.Image = global::ProjetoEmpresaDasAguas.Properties.Resources.if_system_search_118797__1_;
            this.ButtonPesquisaIdCliente.Location = new System.Drawing.Point(329, 54);
            this.ButtonPesquisaIdCliente.Name = "ButtonPesquisaIdCliente";
            this.ButtonPesquisaIdCliente.Size = new System.Drawing.Size(25, 25);
            this.ButtonPesquisaIdCliente.TabIndex = 53;
            this.ButtonPesquisaIdCliente.UseVisualStyleBackColor = true;
            this.ButtonPesquisaIdCliente.Click += new System.EventHandler(this.ButtonPesquisaIdCliente_Click);
            // 
            // ButtonPesquisaIdConsumo
            // 
            this.ButtonPesquisaIdConsumo.Image = global::ProjetoEmpresaDasAguas.Properties.Resources.if_system_search_118797__1_;
            this.ButtonPesquisaIdConsumo.Location = new System.Drawing.Point(108, 17);
            this.ButtonPesquisaIdConsumo.Name = "ButtonPesquisaIdConsumo";
            this.ButtonPesquisaIdConsumo.Size = new System.Drawing.Size(25, 25);
            this.ButtonPesquisaIdConsumo.TabIndex = 52;
            this.ButtonPesquisaIdConsumo.UseVisualStyleBackColor = true;
            this.ButtonPesquisaIdConsumo.Click += new System.EventHandler(this.ButtonPesquisaIdConsumo_Click);
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.ForeColor = System.Drawing.SystemColors.HotTrack;
            this.label3.Location = new System.Drawing.Point(5, 81);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(34, 13);
            this.label3.TabIndex = 50;
            this.label3.Text = "Data";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.ForeColor = System.Drawing.SystemColors.HotTrack;
            this.label2.Location = new System.Drawing.Point(5, 44);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(63, 13);
            this.label2.TabIndex = 49;
            this.label2.Text = "ID Cliente";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.ForeColor = System.Drawing.SystemColors.HotTrack;
            this.label1.Location = new System.Drawing.Point(5, 6);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(73, 13);
            this.label1.TabIndex = 48;
            this.label1.Text = "Id Consumo";
            // 
            // TreeViewFiltro
            // 
            this.TreeViewFiltro.BackColor = System.Drawing.SystemColors.MenuBar;
            this.TreeViewFiltro.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.TreeViewFiltro.Location = new System.Drawing.Point(381, 45);
            this.TreeViewFiltro.Name = "TreeViewFiltro";
            this.TreeViewFiltro.Size = new System.Drawing.Size(141, 255);
            this.TreeViewFiltro.TabIndex = 59;
            this.TreeViewFiltro.AfterSelect += new System.Windows.Forms.TreeViewEventHandler(this.TreeViewFiltro_AfterSelect_1);
            // 
            // imageList1
            // 
            this.imageList1.ImageStream = ((System.Windows.Forms.ImageListStreamer)(resources.GetObject("imageList1.ImageStream")));
            this.imageList1.TransparentColor = System.Drawing.Color.Transparent;
            this.imageList1.Images.SetKeyName(0, "if_system-users_118828.png");
            this.imageList1.Images.SetKeyName(1, "if_system-users_118828_pb.png");
            // 
            // ListViewFiltro
            // 
            this.ListViewFiltro.BackColor = System.Drawing.SystemColors.MenuBar;
            this.ListViewFiltro.GridLines = true;
            this.ListViewFiltro.Location = new System.Drawing.Point(6, 198);
            this.ListViewFiltro.Name = "ListViewFiltro";
            this.ListViewFiltro.Size = new System.Drawing.Size(320, 108);
            this.ListViewFiltro.TabIndex = 61;
            this.ListViewFiltro.UseCompatibleStateImageBehavior = false;
            this.ListViewFiltro.View = System.Windows.Forms.View.Details;
            this.ListViewFiltro.MouseClick += new System.Windows.Forms.MouseEventHandler(this.ListViewFiltro_MouseClick);
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label5.ForeColor = System.Drawing.SystemColors.HotTrack;
            this.label5.Location = new System.Drawing.Point(384, 28);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(139, 13);
            this.label5.TabIndex = 61;
            this.label5.Text = "Clientes com consumos";
            // 
            // FormConsumo
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(534, 311);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.ListViewFiltro);
            this.Controls.Add(this.TreeViewFiltro);
            this.Controls.Add(this.panel1);
            this.Controls.Add(this.toolStrip1);
            this.Name = "FormConsumo";
            this.Text = "Consumo";
            this.WindowState = System.Windows.Forms.FormWindowState.Maximized;
            this.toolStrip1.ResumeLayout(false);
            this.toolStrip1.PerformLayout();
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.ToolStrip toolStrip1;
        private System.Windows.Forms.ToolStripButton ToolStripButtonNew;
        private System.Windows.Forms.ToolStripButton ToolStripButtonSave;
        private System.Windows.Forms.ToolStripButton ToolStripButtonUpdate;
        private System.Windows.Forms.ToolStripButton ToolStripButtonDelete;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator1;
        private System.Windows.Forms.ToolStripButton ToolStripButtonReturn;
        private System.Windows.Forms.ToolStripButton ToolStripButtonNext;
        private System.Windows.Forms.ToolStripButton ToolStripButtonCancel;
        private System.Windows.Forms.ToolStripButton ToolStripButtonSearch;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.TextBox TextBoxContagem;
        private System.Windows.Forms.TextBox TextBoxId;
        private System.Windows.Forms.Button ButtonPesquisaData;
        private System.Windows.Forms.Button ButtonPesquisaIdCliente;
        private System.Windows.Forms.Button ButtonPesquisaIdConsumo;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TreeView TreeViewFiltro;
        private System.Windows.Forms.DateTimePicker DateTimePickerData;
        private System.Windows.Forms.ImageList imageList1;
        private System.Windows.Forms.ComboBox ComboBoxCliente;
        private System.Windows.Forms.ListView ListViewFiltro;
        private System.Windows.Forms.Label label5;
    }
}
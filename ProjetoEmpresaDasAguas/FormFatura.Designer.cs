﻿namespace ProjetoEmpresaDasAguas
{
    partial class FormFatura
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.toolStrip1 = new System.Windows.Forms.ToolStrip();
            this.ToolStripButtonNovo = new System.Windows.Forms.ToolStripButton();
            this.ToolStripButtonGuardar = new System.Windows.Forms.ToolStripButton();
            this.ToolStripButtonAtualizar = new System.Windows.Forms.ToolStripButton();
            this.ToolStripButtonApagar = new System.Windows.Forms.ToolStripButton();
            this.ToolStripButtonProcurar = new System.Windows.Forms.ToolStripButton();
            this.toolStripSeparator1 = new System.Windows.Forms.ToolStripSeparator();
            this.ToolStripButtonCancelar = new System.Windows.Forms.ToolStripButton();
            this.PanelFatura = new System.Windows.Forms.Panel();
            this.ButtonProcurarIdCliente = new System.Windows.Forms.Button();
            this.ButtonProcurarData = new System.Windows.Forms.Button();
            this.ButtonProcurarIdFatura = new System.Windows.Forms.Button();
            this.TextBoxCodPostal = new System.Windows.Forms.TextBox();
            this.label10 = new System.Windows.Forms.Label();
            this.TextBoxMorada = new System.Windows.Forms.TextBox();
            this.label9 = new System.Windows.Forms.Label();
            this.TextBoxNome = new System.Windows.Forms.TextBox();
            this.label8 = new System.Windows.Forms.Label();
            this.TextBoxTotal = new System.Windows.Forms.TextBox();
            this.NumericUpDownIva = new System.Windows.Forms.NumericUpDown();
            this.DateTimePickerFatura = new System.Windows.Forms.DateTimePicker();
            this.TextBoxContagem = new System.Windows.Forms.TextBox();
            this.ComboBoxIdConsumo = new System.Windows.Forms.ComboBox();
            this.ComboBoxIdCliente = new System.Windows.Forms.ComboBox();
            this.TextBoxIdFatura = new System.Windows.Forms.TextBox();
            this.label7 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.ListViewFaturas = new System.Windows.Forms.ListView();
            this.toolStrip1.SuspendLayout();
            this.PanelFatura.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.NumericUpDownIva)).BeginInit();
            this.SuspendLayout();
            // 
            // toolStrip1
            // 
            this.toolStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.ToolStripButtonNovo,
            this.ToolStripButtonGuardar,
            this.ToolStripButtonAtualizar,
            this.ToolStripButtonApagar,
            this.ToolStripButtonProcurar,
            this.toolStripSeparator1,
            this.ToolStripButtonCancelar});
            this.toolStrip1.Location = new System.Drawing.Point(0, 0);
            this.toolStrip1.Name = "toolStrip1";
            this.toolStrip1.Size = new System.Drawing.Size(534, 25);
            this.toolStrip1.TabIndex = 0;
            this.toolStrip1.Text = "toolStrip1";
            // 
            // ToolStripButtonNovo
            // 
            this.ToolStripButtonNovo.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.ToolStripButtonNovo.Image = global::ProjetoEmpresaDasAguas.Properties.Resources.if_document_new_118910;
            this.ToolStripButtonNovo.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.ToolStripButtonNovo.Name = "ToolStripButtonNovo";
            this.ToolStripButtonNovo.Size = new System.Drawing.Size(23, 22);
            this.ToolStripButtonNovo.Text = "Novo";
            this.ToolStripButtonNovo.Click += new System.EventHandler(this.ToolStripButtonNovo_Click);
            // 
            // ToolStripButtonGuardar
            // 
            this.ToolStripButtonGuardar.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.ToolStripButtonGuardar.Enabled = false;
            this.ToolStripButtonGuardar.Image = global::ProjetoEmpresaDasAguas.Properties.Resources.if_save_173091;
            this.ToolStripButtonGuardar.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.ToolStripButtonGuardar.Name = "ToolStripButtonGuardar";
            this.ToolStripButtonGuardar.Size = new System.Drawing.Size(23, 22);
            this.ToolStripButtonGuardar.Text = "Guardar";
            this.ToolStripButtonGuardar.Click += new System.EventHandler(this.ToolStripButtonGuardar_Click);
            // 
            // ToolStripButtonAtualizar
            // 
            this.ToolStripButtonAtualizar.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.ToolStripButtonAtualizar.Enabled = false;
            this.ToolStripButtonAtualizar.Image = global::ProjetoEmpresaDasAguas.Properties.Resources.if_gtk_refresh_48111;
            this.ToolStripButtonAtualizar.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.ToolStripButtonAtualizar.Name = "ToolStripButtonAtualizar";
            this.ToolStripButtonAtualizar.Size = new System.Drawing.Size(23, 22);
            this.ToolStripButtonAtualizar.Text = "Atualizar";
            this.ToolStripButtonAtualizar.Click += new System.EventHandler(this.ToolStripButtonAtualizar_Click);
            // 
            // ToolStripButtonApagar
            // 
            this.ToolStripButtonApagar.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.ToolStripButtonApagar.Enabled = false;
            this.ToolStripButtonApagar.Image = global::ProjetoEmpresaDasAguas.Properties.Resources.if_trash_115789;
            this.ToolStripButtonApagar.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.ToolStripButtonApagar.Name = "ToolStripButtonApagar";
            this.ToolStripButtonApagar.Size = new System.Drawing.Size(23, 22);
            this.ToolStripButtonApagar.Text = "Apagar";
            this.ToolStripButtonApagar.Click += new System.EventHandler(this.ToolStripButtonApagar_Click);
            // 
            // ToolStripButtonProcurar
            // 
            this.ToolStripButtonProcurar.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.ToolStripButtonProcurar.Image = global::ProjetoEmpresaDasAguas.Properties.Resources.if_system_search_118797__1_;
            this.ToolStripButtonProcurar.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.ToolStripButtonProcurar.Name = "ToolStripButtonProcurar";
            this.ToolStripButtonProcurar.Size = new System.Drawing.Size(23, 22);
            this.ToolStripButtonProcurar.Text = "Procurar";
            this.ToolStripButtonProcurar.Click += new System.EventHandler(this.ToolStripButtonProcurar_Click);
            // 
            // toolStripSeparator1
            // 
            this.toolStripSeparator1.Name = "toolStripSeparator1";
            this.toolStripSeparator1.Size = new System.Drawing.Size(6, 25);
            // 
            // ToolStripButtonCancelar
            // 
            this.ToolStripButtonCancelar.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.ToolStripButtonCancelar.Enabled = false;
            this.ToolStripButtonCancelar.Image = global::ProjetoEmpresaDasAguas.Properties.Resources.if_f_cross_256_282471;
            this.ToolStripButtonCancelar.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.ToolStripButtonCancelar.Name = "ToolStripButtonCancelar";
            this.ToolStripButtonCancelar.Size = new System.Drawing.Size(23, 22);
            this.ToolStripButtonCancelar.Text = "Cancelar";
            this.ToolStripButtonCancelar.Click += new System.EventHandler(this.ToolStripButtonCancelar_Click);
            // 
            // PanelFatura
            // 
            this.PanelFatura.Controls.Add(this.ButtonProcurarIdCliente);
            this.PanelFatura.Controls.Add(this.ButtonProcurarData);
            this.PanelFatura.Controls.Add(this.ButtonProcurarIdFatura);
            this.PanelFatura.Controls.Add(this.TextBoxCodPostal);
            this.PanelFatura.Controls.Add(this.label10);
            this.PanelFatura.Controls.Add(this.TextBoxMorada);
            this.PanelFatura.Controls.Add(this.label9);
            this.PanelFatura.Controls.Add(this.TextBoxNome);
            this.PanelFatura.Controls.Add(this.label8);
            this.PanelFatura.Controls.Add(this.TextBoxTotal);
            this.PanelFatura.Controls.Add(this.NumericUpDownIva);
            this.PanelFatura.Controls.Add(this.DateTimePickerFatura);
            this.PanelFatura.Controls.Add(this.TextBoxContagem);
            this.PanelFatura.Controls.Add(this.ComboBoxIdConsumo);
            this.PanelFatura.Controls.Add(this.ComboBoxIdCliente);
            this.PanelFatura.Controls.Add(this.TextBoxIdFatura);
            this.PanelFatura.Controls.Add(this.label7);
            this.PanelFatura.Controls.Add(this.label6);
            this.PanelFatura.Controls.Add(this.label5);
            this.PanelFatura.Controls.Add(this.label4);
            this.PanelFatura.Controls.Add(this.label3);
            this.PanelFatura.Controls.Add(this.label2);
            this.PanelFatura.Controls.Add(this.label1);
            this.PanelFatura.Enabled = false;
            this.PanelFatura.Location = new System.Drawing.Point(13, 29);
            this.PanelFatura.Name = "PanelFatura";
            this.PanelFatura.Size = new System.Drawing.Size(509, 185);
            this.PanelFatura.TabIndex = 1;
            // 
            // ButtonProcurarIdCliente
            // 
            this.ButtonProcurarIdCliente.Image = global::ProjetoEmpresaDasAguas.Properties.Resources.if_system_search_118797__1_;
            this.ButtonProcurarIdCliente.Location = new System.Drawing.Point(165, 36);
            this.ButtonProcurarIdCliente.Name = "ButtonProcurarIdCliente";
            this.ButtonProcurarIdCliente.Size = new System.Drawing.Size(25, 25);
            this.ButtonProcurarIdCliente.TabIndex = 23;
            this.ButtonProcurarIdCliente.UseVisualStyleBackColor = true;
            this.ButtonProcurarIdCliente.Click += new System.EventHandler(this.ButtonProcurarIdCliente_Click);
            // 
            // ButtonProcurarData
            // 
            this.ButtonProcurarData.Image = global::ProjetoEmpresaDasAguas.Properties.Resources.if_system_search_118797__1_;
            this.ButtonProcurarData.Location = new System.Drawing.Point(471, 6);
            this.ButtonProcurarData.Name = "ButtonProcurarData";
            this.ButtonProcurarData.Size = new System.Drawing.Size(25, 25);
            this.ButtonProcurarData.TabIndex = 22;
            this.ButtonProcurarData.UseVisualStyleBackColor = true;
            this.ButtonProcurarData.Click += new System.EventHandler(this.ButtonProcurarData_Click);
            // 
            // ButtonProcurarIdFatura
            // 
            this.ButtonProcurarIdFatura.Image = global::ProjetoEmpresaDasAguas.Properties.Resources.if_system_search_118797__1_;
            this.ButtonProcurarIdFatura.Location = new System.Drawing.Point(219, 6);
            this.ButtonProcurarIdFatura.Name = "ButtonProcurarIdFatura";
            this.ButtonProcurarIdFatura.Size = new System.Drawing.Size(25, 25);
            this.ButtonProcurarIdFatura.TabIndex = 21;
            this.ButtonProcurarIdFatura.UseVisualStyleBackColor = true;
            this.ButtonProcurarIdFatura.Click += new System.EventHandler(this.ButtonProcurarIdFatura_Click);
            // 
            // TextBoxCodPostal
            // 
            this.TextBoxCodPostal.Location = new System.Drawing.Point(91, 97);
            this.TextBoxCodPostal.Name = "TextBoxCodPostal";
            this.TextBoxCodPostal.ReadOnly = true;
            this.TextBoxCodPostal.Size = new System.Drawing.Size(121, 20);
            this.TextBoxCodPostal.TabIndex = 20;
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label10.ForeColor = System.Drawing.SystemColors.HotTrack;
            this.label10.Location = new System.Drawing.Point(20, 101);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(72, 13);
            this.label10.TabIndex = 19;
            this.label10.Text = "Cod. Postal";
            // 
            // TextBoxMorada
            // 
            this.TextBoxMorada.Location = new System.Drawing.Point(91, 68);
            this.TextBoxMorada.Name = "TextBoxMorada";
            this.TextBoxMorada.ReadOnly = true;
            this.TextBoxMorada.Size = new System.Drawing.Size(405, 20);
            this.TextBoxMorada.TabIndex = 18;
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label9.ForeColor = System.Drawing.SystemColors.HotTrack;
            this.label9.Location = new System.Drawing.Point(20, 72);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(49, 13);
            this.label9.TabIndex = 17;
            this.label9.Text = "Morada";
            // 
            // TextBoxNome
            // 
            this.TextBoxNome.Location = new System.Drawing.Point(244, 38);
            this.TextBoxNome.Name = "TextBoxNome";
            this.TextBoxNome.ReadOnly = true;
            this.TextBoxNome.Size = new System.Drawing.Size(252, 20);
            this.TextBoxNome.TabIndex = 16;
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label8.ForeColor = System.Drawing.SystemColors.HotTrack;
            this.label8.Location = new System.Drawing.Point(204, 42);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(39, 13);
            this.label8.TabIndex = 15;
            this.label8.Text = "Nome";
            // 
            // TextBoxTotal
            // 
            this.TextBoxTotal.Location = new System.Drawing.Point(377, 156);
            this.TextBoxTotal.Name = "TextBoxTotal";
            this.TextBoxTotal.ReadOnly = true;
            this.TextBoxTotal.Size = new System.Drawing.Size(115, 20);
            this.TextBoxTotal.TabIndex = 14;
            // 
            // NumericUpDownIva
            // 
            this.NumericUpDownIva.DecimalPlaces = 2;
            this.NumericUpDownIva.Location = new System.Drawing.Point(91, 156);
            this.NumericUpDownIva.Name = "NumericUpDownIva";
            this.NumericUpDownIva.Size = new System.Drawing.Size(121, 20);
            this.NumericUpDownIva.TabIndex = 13;
            // 
            // DateTimePickerFatura
            // 
            this.DateTimePickerFatura.Format = System.Windows.Forms.DateTimePickerFormat.Short;
            this.DateTimePickerFatura.Location = new System.Drawing.Point(309, 9);
            this.DateTimePickerFatura.Name = "DateTimePickerFatura";
            this.DateTimePickerFatura.Size = new System.Drawing.Size(156, 20);
            this.DateTimePickerFatura.TabIndex = 12;
            this.DateTimePickerFatura.Value = new System.DateTime(2018, 1, 31, 0, 0, 0, 0);
            // 
            // TextBoxContagem
            // 
            this.TextBoxContagem.Location = new System.Drawing.Point(377, 126);
            this.TextBoxContagem.Name = "TextBoxContagem";
            this.TextBoxContagem.ReadOnly = true;
            this.TextBoxContagem.Size = new System.Drawing.Size(115, 20);
            this.TextBoxContagem.TabIndex = 11;
            // 
            // ComboBoxIdConsumo
            // 
            this.ComboBoxIdConsumo.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.Suggest;
            this.ComboBoxIdConsumo.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this.ComboBoxIdConsumo.FormattingEnabled = true;
            this.ComboBoxIdConsumo.Location = new System.Drawing.Point(91, 126);
            this.ComboBoxIdConsumo.Name = "ComboBoxIdConsumo";
            this.ComboBoxIdConsumo.Size = new System.Drawing.Size(121, 21);
            this.ComboBoxIdConsumo.TabIndex = 10;
            this.ComboBoxIdConsumo.SelectedIndexChanged += new System.EventHandler(this.ComboBoxIdConsumo_SelectedIndexChanged);
            // 
            // ComboBoxIdCliente
            // 
            this.ComboBoxIdCliente.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.Suggest;
            this.ComboBoxIdCliente.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this.ComboBoxIdCliente.FormattingEnabled = true;
            this.ComboBoxIdCliente.Location = new System.Drawing.Point(91, 38);
            this.ComboBoxIdCliente.Name = "ComboBoxIdCliente";
            this.ComboBoxIdCliente.Size = new System.Drawing.Size(67, 21);
            this.ComboBoxIdCliente.TabIndex = 9;
            this.ComboBoxIdCliente.SelectedIndexChanged += new System.EventHandler(this.ComboBoxIdCliente_SelectedIndexChanged);
            // 
            // TextBoxIdFatura
            // 
            this.TextBoxIdFatura.Location = new System.Drawing.Point(91, 9);
            this.TextBoxIdFatura.Name = "TextBoxIdFatura";
            this.TextBoxIdFatura.Size = new System.Drawing.Size(121, 20);
            this.TextBoxIdFatura.TabIndex = 8;
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label7.ForeColor = System.Drawing.SystemColors.HotTrack;
            this.label7.Location = new System.Drawing.Point(303, 160);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(36, 13);
            this.label7.TabIndex = 7;
            this.label7.Text = "Total";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label6.ForeColor = System.Drawing.SystemColors.HotTrack;
            this.label6.Location = new System.Drawing.Point(17, 160);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(27, 13);
            this.label6.TabIndex = 6;
            this.label6.Text = "IVA";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label5.ForeColor = System.Drawing.SystemColors.HotTrack;
            this.label5.Location = new System.Drawing.Point(266, 13);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(34, 13);
            this.label5.TabIndex = 5;
            this.label5.Text = "Data";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.ForeColor = System.Drawing.SystemColors.HotTrack;
            this.label4.Location = new System.Drawing.Point(303, 130);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(63, 13);
            this.label4.TabIndex = 4;
            this.label4.Text = "Contagem";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.ForeColor = System.Drawing.SystemColors.HotTrack;
            this.label3.Location = new System.Drawing.Point(17, 130);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(75, 13);
            this.label3.TabIndex = 3;
            this.label3.Text = "N. Consumo";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.ForeColor = System.Drawing.SystemColors.HotTrack;
            this.label2.Location = new System.Drawing.Point(17, 42);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(63, 13);
            this.label2.TabIndex = 2;
            this.label2.Text = "N. Cliente";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.ForeColor = System.Drawing.SystemColors.HotTrack;
            this.label1.Location = new System.Drawing.Point(17, 13);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(60, 13);
            this.label1.TabIndex = 1;
            this.label1.Text = "N. Fatura";
            // 
            // ListViewFaturas
            // 
            this.ListViewFaturas.GridLines = true;
            this.ListViewFaturas.Location = new System.Drawing.Point(15, 220);
            this.ListViewFaturas.Name = "ListViewFaturas";
            this.ListViewFaturas.Size = new System.Drawing.Size(503, 85);
            this.ListViewFaturas.TabIndex = 25;
            this.ListViewFaturas.UseCompatibleStateImageBehavior = false;
            this.ListViewFaturas.View = System.Windows.Forms.View.Details;
            this.ListViewFaturas.MouseClick += new System.Windows.Forms.MouseEventHandler(this.ListViewFaturas_MouseClick);
            // 
            // FormFatura
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(534, 311);
            this.Controls.Add(this.ListViewFaturas);
            this.Controls.Add(this.PanelFatura);
            this.Controls.Add(this.toolStrip1);
            this.Name = "FormFatura";
            this.Text = "Fatura";
            this.WindowState = System.Windows.Forms.FormWindowState.Maximized;
            this.toolStrip1.ResumeLayout(false);
            this.toolStrip1.PerformLayout();
            this.PanelFatura.ResumeLayout(false);
            this.PanelFatura.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.NumericUpDownIva)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.ToolStrip toolStrip1;
        private System.Windows.Forms.ToolStripButton ToolStripButtonNovo;
        private System.Windows.Forms.ToolStripButton ToolStripButtonGuardar;
        private System.Windows.Forms.ToolStripButton ToolStripButtonAtualizar;
        private System.Windows.Forms.ToolStripButton ToolStripButtonApagar;
        private System.Windows.Forms.ToolStripButton ToolStripButtonProcurar;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator1;
        private System.Windows.Forms.ToolStripButton ToolStripButtonCancelar;
        private System.Windows.Forms.Panel PanelFatura;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox TextBoxTotal;
        private System.Windows.Forms.NumericUpDown NumericUpDownIva;
        private System.Windows.Forms.DateTimePicker DateTimePickerFatura;
        private System.Windows.Forms.TextBox TextBoxContagem;
        private System.Windows.Forms.ComboBox ComboBoxIdConsumo;
        private System.Windows.Forms.ComboBox ComboBoxIdCliente;
        private System.Windows.Forms.TextBox TextBoxIdFatura;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.TextBox TextBoxNome;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.TextBox TextBoxMorada;
        private System.Windows.Forms.TextBox TextBoxCodPostal;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.Button ButtonProcurarIdCliente;
        private System.Windows.Forms.Button ButtonProcurarData;
        private System.Windows.Forms.Button ButtonProcurarIdFatura;
        private System.Windows.Forms.ListView ListViewFaturas;
    }
}
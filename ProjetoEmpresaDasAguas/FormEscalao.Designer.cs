﻿namespace ProjetoEmpresaDasAguas
{
    partial class FormEscalao
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.toolStrip1 = new System.Windows.Forms.ToolStrip();
            this.ToolStripButtonNew = new System.Windows.Forms.ToolStripButton();
            this.ToolStripButtonSave = new System.Windows.Forms.ToolStripButton();
            this.ToolStripButtonUpdate = new System.Windows.Forms.ToolStripButton();
            this.ToolStripButtonDelete = new System.Windows.Forms.ToolStripButton();
            this.ToolStripButtonSearch = new System.Windows.Forms.ToolStripButton();
            this.toolStripSeparator1 = new System.Windows.Forms.ToolStripSeparator();
            this.ToolStripButtonReturn = new System.Windows.Forms.ToolStripButton();
            this.ToolStripButtonNext = new System.Windows.Forms.ToolStripButton();
            this.ToolStripButtonCancel = new System.Windows.Forms.ToolStripButton();
            this.panel1 = new System.Windows.Forms.Panel();
            this.TextBoxMinimo = new System.Windows.Forms.TextBox();
            this.label4 = new System.Windows.Forms.Label();
            this.TextBoxValor = new System.Windows.Forms.TextBox();
            this.TextBoxId = new System.Windows.Forms.TextBox();
            this.ButtonPesquisaIdEscalao = new System.Windows.Forms.Button();
            this.label2 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.ListViewFiltro = new System.Windows.Forms.ListView();
            this.toolStrip1.SuspendLayout();
            this.panel1.SuspendLayout();
            this.SuspendLayout();
            // 
            // toolStrip1
            // 
            this.toolStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.ToolStripButtonNew,
            this.ToolStripButtonSave,
            this.ToolStripButtonUpdate,
            this.ToolStripButtonDelete,
            this.ToolStripButtonSearch,
            this.toolStripSeparator1,
            this.ToolStripButtonReturn,
            this.ToolStripButtonNext,
            this.ToolStripButtonCancel});
            this.toolStrip1.Location = new System.Drawing.Point(0, 0);
            this.toolStrip1.Name = "toolStrip1";
            this.toolStrip1.Size = new System.Drawing.Size(534, 25);
            this.toolStrip1.Stretch = true;
            this.toolStrip1.TabIndex = 17;
            this.toolStrip1.Text = "toolStrip1";
            // 
            // ToolStripButtonNew
            // 
            this.ToolStripButtonNew.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.ToolStripButtonNew.Image = global::ProjetoEmpresaDasAguas.Properties.Resources.if_document_new_118910;
            this.ToolStripButtonNew.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.ToolStripButtonNew.Name = "ToolStripButtonNew";
            this.ToolStripButtonNew.Size = new System.Drawing.Size(23, 22);
            this.ToolStripButtonNew.Text = "Novo";
            this.ToolStripButtonNew.Click += new System.EventHandler(this.ToolStripButtonNew_Click);
            // 
            // ToolStripButtonSave
            // 
            this.ToolStripButtonSave.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.ToolStripButtonSave.Enabled = false;
            this.ToolStripButtonSave.Image = global::ProjetoEmpresaDasAguas.Properties.Resources.if_save_173091;
            this.ToolStripButtonSave.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.ToolStripButtonSave.Name = "ToolStripButtonSave";
            this.ToolStripButtonSave.Size = new System.Drawing.Size(23, 22);
            this.ToolStripButtonSave.Text = "Gravar";
            this.ToolStripButtonSave.Click += new System.EventHandler(this.ToolStripButtonSave_Click);
            // 
            // ToolStripButtonUpdate
            // 
            this.ToolStripButtonUpdate.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.ToolStripButtonUpdate.Enabled = false;
            this.ToolStripButtonUpdate.Image = global::ProjetoEmpresaDasAguas.Properties.Resources.if_gtk_refresh_48111;
            this.ToolStripButtonUpdate.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.ToolStripButtonUpdate.Name = "ToolStripButtonUpdate";
            this.ToolStripButtonUpdate.Size = new System.Drawing.Size(23, 22);
            this.ToolStripButtonUpdate.Text = "Atualizar";
            this.ToolStripButtonUpdate.Click += new System.EventHandler(this.ToolStripButtonUpdate_Click);
            // 
            // ToolStripButtonDelete
            // 
            this.ToolStripButtonDelete.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.ToolStripButtonDelete.DoubleClickEnabled = true;
            this.ToolStripButtonDelete.Enabled = false;
            this.ToolStripButtonDelete.Image = global::ProjetoEmpresaDasAguas.Properties.Resources.if_trash_115789;
            this.ToolStripButtonDelete.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.ToolStripButtonDelete.Name = "ToolStripButtonDelete";
            this.ToolStripButtonDelete.Size = new System.Drawing.Size(23, 22);
            this.ToolStripButtonDelete.Text = "Eliminar";
            this.ToolStripButtonDelete.Click += new System.EventHandler(this.ToolStripButtonDelete_Click);
            // 
            // ToolStripButtonSearch
            // 
            this.ToolStripButtonSearch.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.ToolStripButtonSearch.Image = global::ProjetoEmpresaDasAguas.Properties.Resources.if_system_search_118797__1_;
            this.ToolStripButtonSearch.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.ToolStripButtonSearch.Name = "ToolStripButtonSearch";
            this.ToolStripButtonSearch.Size = new System.Drawing.Size(23, 22);
            this.ToolStripButtonSearch.Text = "Pesquisar";
            this.ToolStripButtonSearch.Click += new System.EventHandler(this.ToolStripButtonSearch_Click);
            // 
            // toolStripSeparator1
            // 
            this.toolStripSeparator1.Name = "toolStripSeparator1";
            this.toolStripSeparator1.Size = new System.Drawing.Size(6, 25);
            // 
            // ToolStripButtonReturn
            // 
            this.ToolStripButtonReturn.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.ToolStripButtonReturn.Image = global::ProjetoEmpresaDasAguas.Properties.Resources.if_gtk_go_back_ltr_48100;
            this.ToolStripButtonReturn.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.ToolStripButtonReturn.Name = "ToolStripButtonReturn";
            this.ToolStripButtonReturn.Size = new System.Drawing.Size(23, 22);
            this.ToolStripButtonReturn.Text = "Anterior";
            this.ToolStripButtonReturn.Click += new System.EventHandler(this.ToolStripButtonReturn_Click);
            // 
            // ToolStripButtonNext
            // 
            this.ToolStripButtonNext.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.ToolStripButtonNext.Image = global::ProjetoEmpresaDasAguas.Properties.Resources.if_gtk_go_back_rtl_48101;
            this.ToolStripButtonNext.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.ToolStripButtonNext.Name = "ToolStripButtonNext";
            this.ToolStripButtonNext.Size = new System.Drawing.Size(23, 22);
            this.ToolStripButtonNext.Text = "Próximo";
            this.ToolStripButtonNext.Click += new System.EventHandler(this.ToolStripButtonNext_Click);
            // 
            // ToolStripButtonCancel
            // 
            this.ToolStripButtonCancel.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.ToolStripButtonCancel.Enabled = false;
            this.ToolStripButtonCancel.Image = global::ProjetoEmpresaDasAguas.Properties.Resources.if_f_cross_256_282471;
            this.ToolStripButtonCancel.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.ToolStripButtonCancel.Name = "ToolStripButtonCancel";
            this.ToolStripButtonCancel.Size = new System.Drawing.Size(23, 22);
            this.ToolStripButtonCancel.Text = "Cancelar";
            this.ToolStripButtonCancel.Click += new System.EventHandler(this.ToolStripButtonCancel_Click);
            // 
            // panel1
            // 
            this.panel1.Controls.Add(this.TextBoxMinimo);
            this.panel1.Controls.Add(this.label4);
            this.panel1.Controls.Add(this.TextBoxValor);
            this.panel1.Controls.Add(this.TextBoxId);
            this.panel1.Controls.Add(this.ButtonPesquisaIdEscalao);
            this.panel1.Controls.Add(this.label2);
            this.panel1.Controls.Add(this.label1);
            this.panel1.Enabled = false;
            this.panel1.Location = new System.Drawing.Point(6, 41);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(230, 258);
            this.panel1.TabIndex = 18;
            // 
            // TextBoxMinimo
            // 
            this.TextBoxMinimo.Location = new System.Drawing.Point(6, 91);
            this.TextBoxMinimo.Name = "TextBoxMinimo";
            this.TextBoxMinimo.Size = new System.Drawing.Size(155, 20);
            this.TextBoxMinimo.TabIndex = 58;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.ForeColor = System.Drawing.SystemColors.HotTrack;
            this.label4.Location = new System.Drawing.Point(6, 134);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(84, 13);
            this.label4.TabIndex = 57;
            this.label4.Text = "Valor Unitário";
            // 
            // TextBoxValor
            // 
            this.TextBoxValor.Location = new System.Drawing.Point(6, 150);
            this.TextBoxValor.Name = "TextBoxValor";
            this.TextBoxValor.Size = new System.Drawing.Size(155, 20);
            this.TextBoxValor.TabIndex = 56;
            // 
            // TextBoxId
            // 
            this.TextBoxId.Location = new System.Drawing.Point(6, 34);
            this.TextBoxId.Name = "TextBoxId";
            this.TextBoxId.Size = new System.Drawing.Size(155, 20);
            this.TextBoxId.TabIndex = 55;
            // 
            // ButtonPesquisaIdEscalao
            // 
            this.ButtonPesquisaIdEscalao.Image = global::ProjetoEmpresaDasAguas.Properties.Resources.if_system_search_118797__1_;
            this.ButtonPesquisaIdEscalao.Location = new System.Drawing.Point(178, 31);
            this.ButtonPesquisaIdEscalao.Name = "ButtonPesquisaIdEscalao";
            this.ButtonPesquisaIdEscalao.Size = new System.Drawing.Size(25, 25);
            this.ButtonPesquisaIdEscalao.TabIndex = 52;
            this.ButtonPesquisaIdEscalao.UseVisualStyleBackColor = true;
            this.ButtonPesquisaIdEscalao.Click += new System.EventHandler(this.ButtonPesquisaIdEscalao_Click);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.ForeColor = System.Drawing.SystemColors.HotTrack;
            this.label2.Location = new System.Drawing.Point(6, 75);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(46, 13);
            this.label2.TabIndex = 49;
            this.label2.Text = "Minimo";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.ForeColor = System.Drawing.SystemColors.HotTrack;
            this.label1.Location = new System.Drawing.Point(6, 19);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(67, 13);
            this.label1.TabIndex = 48;
            this.label1.Text = "Id Escalão";
            // 
            // ListViewFiltro
            // 
            this.ListViewFiltro.BackColor = System.Drawing.SystemColors.MenuBar;
            this.ListViewFiltro.GridLines = true;
            this.ListViewFiltro.Location = new System.Drawing.Point(242, 41);
            this.ListViewFiltro.Name = "ListViewFiltro";
            this.ListViewFiltro.Size = new System.Drawing.Size(286, 258);
            this.ListViewFiltro.TabIndex = 62;
            this.ListViewFiltro.UseCompatibleStateImageBehavior = false;
            this.ListViewFiltro.View = System.Windows.Forms.View.Details;
            this.ListViewFiltro.MouseClick += new System.Windows.Forms.MouseEventHandler(this.ListViewFiltro_MouseClick);
            // 
            // FormEscalao
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(534, 311);
            this.Controls.Add(this.ListViewFiltro);
            this.Controls.Add(this.panel1);
            this.Controls.Add(this.toolStrip1);
            this.Name = "FormEscalao";
            this.Text = "Escalao";
            this.WindowState = System.Windows.Forms.FormWindowState.Maximized;
            this.toolStrip1.ResumeLayout(false);
            this.toolStrip1.PerformLayout();
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.ToolStrip toolStrip1;
        private System.Windows.Forms.ToolStripButton ToolStripButtonNew;
        private System.Windows.Forms.ToolStripButton ToolStripButtonSave;
        private System.Windows.Forms.ToolStripButton ToolStripButtonUpdate;
        private System.Windows.Forms.ToolStripButton ToolStripButtonDelete;
        private System.Windows.Forms.ToolStripButton ToolStripButtonSearch;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator1;
        private System.Windows.Forms.ToolStripButton ToolStripButtonReturn;
        private System.Windows.Forms.ToolStripButton ToolStripButtonNext;
        private System.Windows.Forms.ToolStripButton ToolStripButtonCancel;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.TextBox TextBoxValor;
        private System.Windows.Forms.TextBox TextBoxId;
        private System.Windows.Forms.Button ButtonPesquisaIdEscalao;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox TextBoxMinimo;
        private System.Windows.Forms.ListView ListViewFiltro;
    }
}
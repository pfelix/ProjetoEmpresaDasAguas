﻿using LivrariaDeClasses;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace ProjetoEmpresaDasAguas
{
    public partial class FormInicio : Form
    {
        CRUDCliente _CRUDCliente;
        CRUDConsumo _CRUDConsumo;
        CRUDFatura _CRUDFatura;
        CRUDEscalao _CRUDEscalao;
        public FormInicio()
        {
            InitializeComponent();
            ControlBox = false;
            _CRUDCliente = new CRUDCliente();
            _CRUDConsumo = new CRUDConsumo();
            _CRUDFatura = new CRUDFatura();
            _CRUDEscalao = new CRUDEscalao();

            
        }

        private void FormInicio_Load(object sender, EventArgs e)
        {
            //conta os servicos
            int cliente = _CRUDCliente.ReadCliente().Count;
            int fatura = _CRUDFatura.ReadFatura().Count;
            int consumo = _CRUDConsumo.ReadConsumo().Count;
            int escalao = _CRUDEscalao.ReadEscalao().Count;
            
            ChartClientes.Series["Companhia das Aguas"].Points.AddXY("Clientes", cliente);
           
            ChartClientes.Series["Companhia das Aguas"].Points.AddXY("Faturas", fatura);

            ChartClientes.Series["Companhia das Aguas"].Points.AddXY("Consumos", consumo);

            ChartClientes.Series["Companhia das Aguas"].Points.AddXY("Escaloes", escalao);

            var Totalconsumos = from consumos in _CRUDConsumo.ReadConsumo()
            group consumos by consumos.idCliente into consumosConta select new
            {
                idCliente = consumosConta.Key,
                Total = consumosConta.Sum(x => x.contagem),
            };

            foreach (var item in Totalconsumos)
            {
                ChartConsumos.Series["Consumos"].Points.AddXY("id" + item.idCliente, item.Total);
            }
            
        }
    }
}

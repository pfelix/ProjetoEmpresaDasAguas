﻿namespace ProjetoEmpresaDasAguas
{
    partial class FormInicio
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.Windows.Forms.DataVisualization.Charting.ChartArea chartArea1 = new System.Windows.Forms.DataVisualization.Charting.ChartArea();
            System.Windows.Forms.DataVisualization.Charting.Legend legend1 = new System.Windows.Forms.DataVisualization.Charting.Legend();
            System.Windows.Forms.DataVisualization.Charting.Series series1 = new System.Windows.Forms.DataVisualization.Charting.Series();
            System.Windows.Forms.DataVisualization.Charting.ChartArea chartArea2 = new System.Windows.Forms.DataVisualization.Charting.ChartArea();
            System.Windows.Forms.DataVisualization.Charting.Legend legend2 = new System.Windows.Forms.DataVisualization.Charting.Legend();
            System.Windows.Forms.DataVisualization.Charting.Series series2 = new System.Windows.Forms.DataVisualization.Charting.Series();
            this.ChartClientes = new System.Windows.Forms.DataVisualization.Charting.Chart();
            this.ChartConsumos = new System.Windows.Forms.DataVisualization.Charting.Chart();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.ChartClientes)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ChartConsumos)).BeginInit();
            this.SuspendLayout();
            // 
            // ChartClientes
            // 
            this.ChartClientes.BackColor = System.Drawing.SystemColors.Control;
            this.ChartClientes.BorderlineColor = System.Drawing.SystemColors.Control;
            chartArea1.Name = "ChartArea1";
            this.ChartClientes.ChartAreas.Add(chartArea1);
            legend1.Name = "Legend1";
            this.ChartClientes.Legends.Add(legend1);
            this.ChartClientes.Location = new System.Drawing.Point(0, 29);
            this.ChartClientes.Name = "ChartClientes";
            this.ChartClientes.Palette = System.Windows.Forms.DataVisualization.Charting.ChartColorPalette.SeaGreen;
            series1.ChartArea = "ChartArea1";
            series1.IsVisibleInLegend = false;
            series1.Legend = "Legend1";
            series1.Name = "Companhia das Aguas";
            this.ChartClientes.Series.Add(series1);
            this.ChartClientes.Size = new System.Drawing.Size(290, 283);
            this.ChartClientes.TabIndex = 0;
            this.ChartClientes.Text = "chart1";
            // 
            // ChartConsumos
            // 
            this.ChartConsumos.BackColor = System.Drawing.SystemColors.Control;
            chartArea2.Name = "ChartArea1";
            this.ChartConsumos.ChartAreas.Add(chartArea2);
            legend2.Name = "Legend1";
            this.ChartConsumos.Legends.Add(legend2);
            this.ChartConsumos.Location = new System.Drawing.Point(288, 29);
            this.ChartConsumos.Name = "ChartConsumos";
            series2.ChartArea = "ChartArea1";
            series2.IsVisibleInLegend = false;
            series2.Legend = "Legend1";
            series2.Name = "Consumos";
            this.ChartConsumos.Series.Add(series2);
            this.ChartConsumos.Size = new System.Drawing.Size(244, 283);
            this.ChartConsumos.TabIndex = 1;
            this.ChartConsumos.Text = "chart1";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.ForeColor = System.Drawing.SystemColors.HotTrack;
            this.label1.Location = new System.Drawing.Point(360, 9);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(128, 13);
            this.label1.TabIndex = 2;
            this.label1.Text = "Consumos por cliente";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.ForeColor = System.Drawing.SystemColors.HotTrack;
            this.label2.Location = new System.Drawing.Point(59, 9);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(111, 13);
            this.label2.TabIndex = 3;
            this.label2.Text = "Total dos serviços";
            // 
            // FormInicio
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(534, 311);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.ChartConsumos);
            this.Controls.Add(this.ChartClientes);
            this.Name = "FormInicio";
            this.Text = "Inicio";
            this.WindowState = System.Windows.Forms.FormWindowState.Maximized;
            this.Load += new System.EventHandler(this.FormInicio_Load);
            ((System.ComponentModel.ISupportInitialize)(this.ChartClientes)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ChartConsumos)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.DataVisualization.Charting.Chart ChartClientes;
        private System.Windows.Forms.DataVisualization.Charting.Chart ChartConsumos;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
    }
}
﻿namespace ProjetoEmpresaDasAguas
{
    using LivrariaDeClasses;
    using System;
    using System.Collections.Generic;
    using System.Data;
    using System.Linq;
    using System.Windows.Forms;

    public partial class FormConsumo : Form
    {
        CRUDConsumo _CRUDConsumo;
        CRUDCliente _CRUDCliente;
        List<Cliente> listaclientes;
        CRUDFatura _CRUDFatura;

        List<Consumo> listaconsumos;

        bool novo = false;
        
        public FormConsumo()
        {
            InitializeComponent();

            ControlBox = false;
            _CRUDConsumo = new CRUDConsumo();
            _CRUDCliente = new CRUDCliente();
            _CRUDFatura = new CRUDFatura();
            
            listaclientes = _CRUDCliente.ReadCliente();
        
            listaconsumos = _CRUDConsumo.ReadConsumo();
            
            ComboBoxCliente.DataSource = listaclientes;
            ComboBoxCliente.DisplayMember = "idCliente";

            CarregarTree();//mete dados na treeview

            //selecionar a linha completa
            ListViewFiltro.View = View.Details;
            ListViewFiltro.FullRowSelect = true;

           
        }


        #region metodos

        /// <summary>
        /// Carregar na treeview os dados dos clientes
        /// </summary>
        private void CarregarTree()
        {

            TreeViewFiltro.Nodes.Clear();
            TreeViewFiltro.ImageList = imageList1;
            
            var ListaCliente = _CRUDCliente.ReadCliente().ToList();
            var ListaConsumo = _CRUDConsumo.ReadConsumo().ToList();

            var listagem = ListaConsumo.Select(c => c.idCliente).Distinct();
            

            foreach (var item in listagem)
            {
                TreeViewFiltro.Nodes.Add("", item.ToString(), 1);
            }
        }


        /// <summary>
        /// carrega os consumos do cliente na listbox
        /// </summary>
        /// <param name="lista"></param>
        /// <param name="idCliente"></param>
        private void CarregarListView(int idCliente)
        {
            ListViewFiltro.Clear();

            //Criar as colunas
            ListViewFiltro.Columns.Add("Id Consumo");
            ListViewFiltro.Columns.Add("Id Cliente");
            ListViewFiltro.Columns.Add("Data");
            ListViewFiltro.Columns.Add("Contagem");

            var listas = from Consumos in _CRUDConsumo.ReadConsumo() where Consumos.idCliente == idCliente select Consumos;

            foreach (var consumo in listas)
            {
                ListViewItem item;

                item = ListViewFiltro.Items.Add(consumo.idConsumo.ToString());
                item.SubItems.Add(consumo.idCliente.ToString());
                item.SubItems.Add(consumo.data.ToShortDateString());
                item.SubItems.Add(consumo.contagem.ToString());
            }
            
            for (int idx = 0; idx <= 3; idx++)
            {
                ListViewFiltro.Columns[idx].AutoResize(ColumnHeaderAutoResizeStyle.HeaderSize);
            }
            
            listaconsumos = null;
            listaconsumos = _CRUDConsumo.ReadConsumo();
        }

        /// <summary>
        /// pesquisar por consumo e carregar a listview
        /// </summary>
        /// <param name="idConsumo"></param>
        private void CarregarListViewConsumo(int idConsumo)
        {
            ListViewFiltro.Clear();

            //Criar as colunas
            ListViewFiltro.Columns.Add("Id Consumo");
            ListViewFiltro.Columns.Add("Id Cliente");
            ListViewFiltro.Columns.Add("Data");
            ListViewFiltro.Columns.Add("Contagem");

            var listas = from Consumos in _CRUDConsumo.ReadConsumo() where Consumos.idConsumo == idConsumo select Consumos;

            foreach (var consumo in listas)
            {
                ListViewItem item;

                item = ListViewFiltro.Items.Add(consumo.idConsumo.ToString());
                item.SubItems.Add(consumo.idCliente.ToString());
                item.SubItems.Add(consumo.data.ToShortDateString());
                item.SubItems.Add(consumo.contagem.ToString());
            }

            for (int idx = 0; idx <= 3; idx++)
            {
                ListViewFiltro.Columns[idx].AutoResize(ColumnHeaderAutoResizeStyle.HeaderSize);
            }


        }

        /// <summary>
        /// pesquisar por data e carrega a listview
        /// </summary>
        /// <param name="data"></param>
        private void CarregarListViewData(DateTime data)
        {
            ListViewFiltro.Clear();

            //Criar as colunas
            ListViewFiltro.Columns.Add("Id Consumo");
            ListViewFiltro.Columns.Add("Id Cliente");
            ListViewFiltro.Columns.Add("Data");
            ListViewFiltro.Columns.Add("Contagem");

            var listas = from Consumos in _CRUDConsumo.ReadConsumo() where Consumos.data == data select Consumos;

            foreach (var consumo in listas)
            {
                ListViewItem item;

                item = ListViewFiltro.Items.Add(consumo.idConsumo.ToString());
                item.SubItems.Add(consumo.idCliente.ToString());
                item.SubItems.Add(consumo.data.ToShortDateString());
                item.SubItems.Add(consumo.contagem.ToString());
            }

            for (int idx = 0; idx <= 3; idx++)
            {
                ListViewFiltro.Columns[idx].AutoResize(ColumnHeaderAutoResizeStyle.HeaderSize);
            }
        }

        /// <summary>
        /// desativa os botoes de pesquisa
        /// </summary>
        private void DesativarBotoesPesquisa()
        {
            ButtonPesquisaIdConsumo.Enabled = false;
            ButtonPesquisaIdCliente.Enabled = false;
            ButtonPesquisaData.Enabled = false;

        }

        /// <summary>
        /// ativa os botoes de pesquisa
        /// </summary>
        private void AtivarBotoesPesquisa()
        {
            ButtonPesquisaIdConsumo.Enabled = true;
            ButtonPesquisaIdCliente.Enabled = true;
            ButtonPesquisaData.Enabled = true;

        }


        /// <summary>
        /// Limpa as TextBox
        /// </summary>
        private void ClearTextBox()
        {
            TextBoxId.Clear();

            ComboBoxCliente.SelectedIndex = -1;
            DateTimePickerData.Value = DateTime.Today;
            TextBoxContagem.Clear();
        }

        #endregion
        
        #region eventos
        
        /// <summary>
        /// seleciona um cliente ao selecionar
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void TreeViewFiltro_AfterSelect_1(object sender, TreeViewEventArgs e)
        {
            ToolStripButtonUpdate.Enabled = false;
            ToolStripButtonDelete.Enabled = false;

            int curItems = Convert.ToInt32(TreeViewFiltro.SelectedNode.Text);

            ListViewFiltro.Items.Clear();

            var consumo = (from Consumos in _CRUDConsumo.ReadConsumo() where Consumos.idConsumo == curItems select Consumos).SingleOrDefault();

            CarregarListView(curItems);
        }

        /// <summary>
        /// procura por id de consumo
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void ButtonPesquisaIdConsumo_Click(object sender, EventArgs e)
        {
            int id = 0;

            if (Validacoes.ValidacaoInt(TextBoxId.Text))
            {
                MessageBox.Show("Não existe nenhum dado para procurar.");
                return;
            }

            id = Convert.ToInt32(TextBoxId.Text);

            Consumo consumo = _CRUDConsumo.ReadConsumoPorIdConsumo(id);

            if (consumo == null)
            {
                MessageBox.Show($"Não existe nenhum consumo com o id {id}");
                ClearTextBox();
                return;
            }


            CarregarListViewConsumo(consumo.idConsumo);

            TextBoxId.Text = consumo.idConsumo.ToString();
            
            ComboBoxCliente.Text = consumo.idCliente.ToString();
            TextBoxContagem.Text = consumo.contagem.ToString();
            DateTimePickerData.Text = consumo.data.ToShortDateString();
            ToolStripButtonCancel.Enabled = false;

            panel1.Enabled = false;
            AtivaCampos();
            ToolStripButtonDelete.Enabled = true;
            ToolStripButtonUpdate.Enabled = true;
        }

        /// <summary>
        /// butão de pesquisa por id de cliente
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void ButtonPesquisaIdCliente_Click(object sender, EventArgs e)
        {
            int id = 0;

            if (Validacoes.ValidacaoInt(ComboBoxCliente.Text))
            {
                MessageBox.Show("Não existe nenhum dado para procurar.");
                return;
            }

            id = Convert.ToInt32(ComboBoxCliente.Text);

            Consumo consumo = _CRUDConsumo.ReadConsumoPorIdCliente(id);

            if (consumo == null)
            {
                MessageBox.Show($"Este cliente nao tem consumos");
                ClearTextBox();
                return;
            }

            CarregarListView(consumo.idCliente);

            //procura na treeview pelo idcliente e seleciona esse
            int conta = 0;
           
            foreach (TreeNode item in TreeViewFiltro.Nodes)
            {
                if (item.Text == consumo.idCliente.ToString())

                {
                    TreeViewFiltro.SelectedNode = TreeViewFiltro.Nodes[conta];
                }
                conta++;
            }

            TextBoxId.Text = consumo.idConsumo.ToString();
            ComboBoxCliente.Text = consumo.idCliente.ToString();
            TextBoxContagem.Text = consumo.contagem.ToString();
            DateTimePickerData.Text = consumo.data.ToShortDateString();
            ToolStripButtonCancel.Enabled = false;
            
            panel1.Enabled = false;

           
        }

        /// <summary>
        /// pesquisar consumo por data
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void ButtonPesquisaData_Click(object sender, EventArgs e)
        {
            DateTime data;

            if (!Validacoes.ValidacaoInt(DateTimePickerData.Text))
            {
                MessageBox.Show("Não existe nenhum dado para procurar.");
                return;
            }

            data = Convert.ToDateTime(DateTimePickerData.Text);

            Consumo consumo = _CRUDConsumo.ReadConsumoPorData(data);

            if (consumo == null)
            {
                MessageBox.Show($"Este existem consumos nesta data.");
                ClearTextBox();
                return;
            }
            
            CarregarListViewData(consumo.data);

            TextBoxId.Text = consumo.idConsumo.ToString();

            ComboBoxCliente.Text = consumo.idCliente.ToString();
            TextBoxContagem.Text = consumo.contagem.ToString();
            DateTimePickerData.Text = consumo.data.ToShortDateString();
            ToolStripButtonCancel.Enabled = false;
            
            panel1.Enabled = false;
        }

        /// <summary>
        /// filtrar dados pela listview, selecionando a linha
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void ListViewFiltro_MouseClick(object sender, MouseEventArgs e)
        {
            int idConsumo = Convert.ToInt32(ListViewFiltro.SelectedItems[0].Text);
           
            ToolStripButtonUpdate.Enabled = true;
            ToolStripButtonDelete.Enabled = true;
            
            Consumo temp = _CRUDConsumo.ReadConsumoPorIdConsumo(idConsumo);
            
            TextBoxId.Text = temp.idConsumo.ToString();
            
            ComboBoxCliente.Text = temp.idCliente.ToString();

            DateTimePickerData.Text = temp.data.ToShortDateString();
            TextBoxContagem.Text = temp.contagem.ToString();
        }


        /// <summary>
        /// ativar os campos de insercao
        /// </summary>
        private void AtivaCampos()
        {
            ComboBoxCliente.Enabled = true;
            DateTimePickerData.Enabled = true;
            TextBoxContagem.Enabled = true;
        }
        
        #endregion


        #region toolstrip


        /// <summary>
        /// para inserir um novo cliente
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void ToolStripButtonNew_Click(object sender, EventArgs e)
        {
            panel1.Enabled = true;
            TreeViewFiltro.Enabled = false;
            ToolStripButtonSave.Enabled = true;
            TextBoxId.Enabled = false;
            ToolStripButtonUpdate.Enabled = false;
            ToolStripButtonDelete.Enabled = false;
            ToolStripButtonCancel.Enabled = true;
            ToolStripButtonNew.Enabled = false;
            ToolStripButtonNext.Enabled = false;
            ToolStripButtonReturn.Enabled = false;

            ClearTextBox(); //limpa as TextBox
            DesativarBotoesPesquisa();
            AtivaCampos();
            novo = true;

        }


        /// <summary>
        /// grava os dados
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void ToolStripButtonSave_Click(object sender, EventArgs e)
        {
            bool erro = false;
            string mensagem = null;
            

            //se ja estiver faturado, nao pode alterar
            if (!novo)
            {
                List<Fatura> fatura = (from faturas in _CRUDFatura.ReadFatura() where faturas.idConsumo == Convert.ToInt32(TextBoxId.Text) select faturas).ToList();

                if (fatura.Count > 0)
                {
                    MessageBox.Show("Não pode alterar este consumo, pois já foi faturado!");
                    panel1.Enabled = false;
                    ToolStripButtonSave.Enabled = false;
                    ToolStripButtonCancel.Enabled = false;
                    ToolStripButtonNew.Enabled = true;
                    ToolStripButtonUpdate.Enabled = false;
                    ToolStripButtonSearch.Enabled = true;
                    return;
                }
            }
            
            if (Validacoes.ValidacaoText(ComboBoxCliente.Text) || Validacoes.ValidacaoInt(ComboBoxCliente.Text) || ComboBoxCliente.SelectedIndex == -1)
            {
                mensagem = "Insira um id de um cliente existente!\n";
                erro = true;
            }

            if (string.IsNullOrEmpty(DateTimePickerData.Text))
            {
                mensagem += "Insira uma data!\n";
                erro = true;
            }

            if (string.IsNullOrEmpty(TextBoxContagem.Text) || Validacoes.ValidacaoInt(TextBoxContagem.Text))
            {
                mensagem += "insira uma contagem!\n";
                erro = true;
            }

            if (novo)
            {
                //verifica se ja foi inserido um consumo neste mes, por este cliente
                List<Consumo> verificaData = listaconsumos.Where(x =>
                x.data.Year == DateTimePickerData.Value.Year
                && x.data.Month == DateTimePickerData.Value.Month
                && x.idCliente.ToString() == ComboBoxCliente.Text).ToList();

                if (verificaData.Count > 0)
                {
                    mensagem += "Já inseriu uma contagem para este mês!\n";
                    erro = true;
                }
                //verifica a data
                List<Consumo> cons = (from consumos in _CRUDConsumo.ReadConsumo() where consumos.idCliente == Convert.ToInt32(ComboBoxCliente.Text) select consumos).ToList();

                foreach (var item in cons)
                {
                    if (item.data > DateTimePickerData.Value)
                    {
                        MessageBox.Show("Não pode inserir ou actualizar uma contagem\ncom uma data anterior as contagens inseridas");
                        return;
                    }

                }
            }

            if (erro)
            {
                MessageBox.Show(mensagem);
                return;
            }
            
            //cria objecto temp para inserir
            Consumo consumo = new Consumo
            {
                idCliente = Convert.ToInt32(ComboBoxCliente.Text),
                data = Convert.ToDateTime(DateTimePickerData.Text),
                contagem = Convert.ToInt32(TextBoxContagem.Text)
            };

            //grava um novo consumo
            if (novo)
            {
                if (!_CRUDConsumo.CreateConsumo(consumo))
                {
                    MessageBox.Show(_CRUDConsumo.Mensagem);
                    return;
                }

                MessageBox.Show($"Consumo inserido com sucesso!");
            }
            //grava uma atualização do consumo
            else
            {
                //cria objecto temp para actualizar
                Consumo Updateconsumo = new Consumo
                {
                    idConsumo = Convert.ToInt32(TextBoxId.Text),
                    idCliente = Convert.ToInt32(ComboBoxCliente.Text),
                    data = Convert.ToDateTime(DateTimePickerData.Text),
                    contagem = Convert.ToInt32(TextBoxContagem.Text)
                };

                if (!_CRUDConsumo.UpdateConsumo(Updateconsumo))
                {
                    MessageBox.Show(_CRUDConsumo.Mensagem);
                    return;
                }

                MessageBox.Show($"Consumo actualizado com sucesso!");
            }

            ClearTextBox();

            CarregarTree();
            TreeViewFiltro.Enabled = true;
            panel1.Enabled = false;
            ToolStripButtonSave.Enabled = false;
            ToolStripButtonCancel.Enabled = false;
            ToolStripButtonNew.Enabled = true;

            AtivaCampos();

        }

        /// <summary>
        /// atualiza os campos de pesquisa
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void ToolStripButtonUpdate_Click(object sender, EventArgs e)
        {

            panel1.Enabled = true;
            ToolStripButtonCancel.Enabled = true;
            ToolStripButtonDelete.Enabled = false;
            ToolStripButtonSave.Enabled = true;
            ToolStripButtonNew.Enabled = false;
            ToolStripButtonSearch.Enabled = false;
            ToolStripButtonNext.Enabled = false;
            ToolStripButtonReturn.Enabled = false;

            TextBoxId.Enabled = false;
            ComboBoxCliente.Enabled = false;
            DateTimePickerData.Enabled = false;
            novo = false;
        }

        /// <summary>
        /// liberta os campos de pesquisa
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void ToolStripButtonSearch_Click(object sender, EventArgs e)
        {
            panel1.Enabled = true;
            ToolStripButtonCancel.Enabled = true;
            ClearTextBox(); //limpa as TextBox
            AtivarBotoesPesquisa();

            AtivaCampos();
            TextBoxContagem.Enabled = false;
            TextBoxId.Enabled = true;
            //ToolStripButtonSearch.Enabled = false;
        }

        /// <summary>
        /// apaga um consumo
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void ToolStripButtonDelete_Click(object sender, EventArgs e)
        {
            if (MessageBox.Show("Eliminar?", "Eliminar", MessageBoxButtons.OKCancel) == DialogResult.OK)
            {
                if (string.IsNullOrEmpty(TextBoxId.Text))
                {
                    MessageBox.Show("Escolha um consumo para apagar");
                    return;
                }

                if (!_CRUDConsumo.DeleteConsumo(Convert.ToInt32(TextBoxId.Text)))
                {
                    MessageBox.Show(_CRUDConsumo.Mensagem);
                    return;
                }
                MessageBox.Show($"Consumo eliminado com sucesso!");

                CarregarTree();
                ClearTextBox();
                TreeViewFiltro.Enabled = true;
                panel1.Enabled = false;

                ListViewFiltro.Clear();
                ToolStripButtonCancel.Enabled = false;
            }
        }

        int i = 0; //variavel para os cosnumos seguintes e anteriores

        /// <summary>
        /// butão para ver o consumo anterior
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void ToolStripButtonReturn_Click(object sender, EventArgs e)
        {
            listaconsumos = (from lista in listaconsumos select lista).ToList();

            if (i == -1)
            {
                i = listaconsumos.Count -1;
            }

            TextBoxId.Text = listaconsumos[i].idConsumo.ToString();
            DateTimePickerData.Text = listaconsumos[i].data.ToShortDateString();
            TextBoxContagem.Text = listaconsumos[i].contagem.ToString();

            i--;
        }


       /// <summary>
       /// butão para ver o cosnumo seguinte
       /// </summary>
       /// <param name="sender"></param>
       /// <param name="e"></param>
        private void ToolStripButtonNext_Click(object sender, EventArgs e)
        {
        
            listaconsumos = (from lista in listaconsumos select lista).ToList();

            if (i == listaconsumos.Count)
            {
                i = 0;
            }
            
            TextBoxId.Text = listaconsumos[i].idConsumo.ToString();
            DateTimePickerData.Text = listaconsumos[i].data.ToShortDateString();
            TextBoxContagem.Text = listaconsumos[i].contagem.ToString();

            i++;
        }

        /// <summary>
        /// cancela o que estivermos a fazer
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void ToolStripButtonCancel_Click(object sender, EventArgs e)
        {
            if (novo)
            {
                if (MessageBox.Show("Cancelar?", "Cancelar", MessageBoxButtons.OKCancel) == DialogResult.OK)
                {
                    CarregarTree();

                    TreeViewFiltro.Enabled = true;
                    ToolStripButtonCancel.Enabled = false;
                    ToolStripButtonDelete.Enabled = false;
                    ToolStripButtonNew.Enabled = true;
                    ToolStripButtonSave.Enabled = false;
                    ToolStripButtonSearch.Enabled = true;
                    panel1.Enabled = false;

                    ToolStripButtonCancel.Enabled = false;

                }
                else
                {
                    return;
                }
            }
            else
            {
                    if (MessageBox.Show("Cancelar?", "Cancelar", MessageBoxButtons.OKCancel) == DialogResult.OK)
                    {
                        CarregarTree();

                        TreeViewFiltro.Enabled = true;
                        ToolStripButtonCancel.Enabled = false;
                        ToolStripButtonDelete.Enabled = false;
                        ToolStripButtonNew.Enabled = true;
                        ToolStripButtonSave.Enabled = false;
                        ToolStripButtonSearch.Enabled = true;
                        panel1.Enabled = false;
                        ClearTextBox(); //limpa as TextBox

                        ToolStripButtonCancel.Enabled = false;

                    }
                    else
                    {
                        return;
                    }
                   
            }
            ToolStripButtonNext.Enabled = true;
            ToolStripButtonReturn.Enabled = true;
        }

        #endregion

    }
}

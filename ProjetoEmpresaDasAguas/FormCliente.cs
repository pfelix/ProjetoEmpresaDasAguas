﻿namespace ProjetoEmpresaDasAguas
{
    using LivrariaDeClasses;
    using System;
    using System.Collections.Generic;
    using System.ComponentModel;
    using System.Data;
    using System.Drawing;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;
    using System.Windows.Forms;

    public partial class FormCliente : Form
    {
        CRUDCliente _CRUDCliente;
        bool novoRegisto = false;
        int primeiroId = 0; // Para guarda o id do primeiro registo da lista quando for carregado a ListBox
        int idxListBox = -1;

        public FormCliente()
        {
            InitializeComponent();

            ControlBox = false;

            _CRUDCliente = new CRUDCliente();

            CarregarListBox(_CRUDCliente.ReadCliente());
        }

        /// <summary>
        /// Metodo para carregar lista na ListBoxFiltro
        /// </summary>
        /// <param name="lista"></param>
        private void CarregarListBox(List<Cliente> lista)
        {
            ListBoxFiltro.Items.Clear();
            
            // Guarda o id do primeiro registo da lista
            primeiroId = lista.FirstOrDefault().idCliente;

            foreach (var cliente in lista)
            {
                ListBoxFiltro.Items.Add(cliente.idCliente + " - " + cliente.nome);
            }
        }

        /// <summary>
        /// Metodo para mostrar os dados
        /// </summary>
        /// <param name="cliente"></param>
        private void MostrarDados(Cliente cliente)
        {
            LimparDados();

            TextBoxId.Text = cliente.idCliente.ToString();
            TextBoxNome.Text = cliente.nome;
            TextBoxMorada.Text = cliente.morada;
            TextBoxCodPostal.Text = cliente.codPostal;
            TextBoxNif.Text = cliente.nif.ToString();
            TextBoxUser.Text = cliente.utilizador;
            TextBoxPassword.Text = cliente.pass;
        }

        /// <summary>
        /// Desativar todos os butões de procura
        /// </summary>
        private void DesativarBotoesProcuraTextBox()
        {
            ButtonProcuraId.Enabled = false;
            ButtonProcuraNome.Enabled = false;
            ButtonProcuraMorada.Enabled = false;
            ButtonProcuraCodPostal.Enabled = false;
            ButtonProcuraNif.Enabled = false;
        }

        /// <summary>
        /// Metodo que limpa todas as TextBox
        /// </summary>
        private void LimparDados()
        {
            TextBoxId.Text = string.Empty;
            TextBoxNome.Text = string.Empty;
            TextBoxMorada.Text = string.Empty;
            TextBoxCodPostal.Text = string.Empty;
            TextBoxNif.Text = string.Empty;
            TextBoxUser.Text = string.Empty;
            TextBoxPassword.Text = string.Empty;
        }

        /// <summary>
        /// Botão Criar novo cliente
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void ToolStripButtonNovo_Click(object sender, EventArgs e)
        {
            // Limpa os dados de todas as textbox
            LimparDados();

            // Ativa e desativação botões, panel e listbox
            PanelCliente.Enabled = true;
            ListBoxFiltro.Enabled = false;
            ToolStripButtonNovo.Enabled = false;
            ToolStripButtonGuardar.Enabled = true;
            ToolStripButtonAtualizar.Enabled = false;
            ToolStripButtonApagar.Enabled = false;
            ToolStripButtonProcurar.Enabled = false;
            ToolStripButtonFrente.Enabled = false;
            ToolStripButtonTras.Enabled = false;
            ToolStripButtonCancelar.Enabled = true;

            // Desativa TextBoxId para não deixar introduzir id
            TextBoxId.Enabled = false;

            // Ativar as textBox User e Password (Para garantir que estão ativas)
            TextBoxUser.Enabled = true;
            TextBoxPassword.Enabled = true;

            // Marca variável novoRegisto para true, para fazer a destinção no botão save
            novoRegisto = true;

            DesativarBotoesProcuraTextBox();
        }

        /// <summary>
        /// Botão salvar
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void ToolStripButtonGuardar_Click(object sender, EventArgs e)
        {
            bool erro = false;
            string mensagem = null;

            // Verifica se as TextBox tem dados validos
            if (Validacoes.ValidacaoText(TextBoxNome.Text))
            {
                mensagem = "Falta preencher o Nome \n";
                erro = true;
            }

            if (Validacoes.ValidacaoText(TextBoxMorada.Text))
            {
                mensagem += "Falta preencher a Morada \n";
                erro = true;
            }

            if (!Validacoes.ValidacaoCodPostal(TextBoxCodPostal.Text))
            {
                mensagem += "Falta preencher um Código Postal válido (xxxx-xxx) \n";
                erro = true;
            }

            if (Validacoes.ValidacaoInt(TextBoxNif.Text))
            {
                mensagem += "Falta preencher um NIF válido \n";
                erro = true;
            }

            if (Validacoes.ValidacaoText(TextBoxUser.Text))
            {
                mensagem += "Falta preencher o User \n";
                erro = true;
            }

            if (Validacoes.ValidacaoText(TextBoxPassword.Text))
            {
                mensagem += "Falta preencher a Password \n";
                erro = true;
            }

            if (erro)
            {
                MessageBox.Show(mensagem);
                return;
            }

            // Gravar na base de dados

            // Verifica se é novo registo ou atualização
            if (novoRegisto)
            {
                // Cria o novo cliente
                Cliente dadosCliente = new Cliente
                {
                    nome = TextBoxNome.Text,
                    morada = TextBoxMorada.Text,
                    codPostal = TextBoxCodPostal.Text,
                    nif = Convert.ToInt32(TextBoxNif.Text),
                    utilizador = TextBoxUser.Text,
                    pass = TextBoxPassword.Text
                };

                // Manda dados apara a BD
                if (!_CRUDCliente.CreateCliente(dadosCliente))
                {
                    MessageBox.Show(_CRUDCliente.Retorno, "Erro ao criar o cliente");
                    return;
                }

                MessageBox.Show("Cliente criado com sucesso.");

                MostrarDados(_CRUDCliente.ReadIdCliente(dadosCliente.idCliente));
            }
            else
            {
                // Atualizar o cliente
                Cliente dadosClienteAtualizar = new Cliente
                {
                    idCliente = Convert.ToInt32(TextBoxId.Text),
                    nome = TextBoxNome.Text,
                    morada = TextBoxMorada.Text,
                    codPostal = TextBoxCodPostal.Text,
                    nif = Convert.ToInt32(TextBoxNif.Text),
                    utilizador = TextBoxUser.Text,
                    pass = TextBoxPassword.Text
                };

                // Manda dados apara a BD
                if (!_CRUDCliente.UpdateCliente(dadosClienteAtualizar))
                {
                    MessageBox.Show(_CRUDCliente.Retorno, "Erro ao atualizar o cliente");
                    return;
                }

                MessageBox.Show("Cliente atualizado com sucesso.");

                MostrarDados(_CRUDCliente.ReadIdCliente(dadosClienteAtualizar.idCliente));
            }

            CarregarListBox(_CRUDCliente.ReadCliente());

            // Ativa e desativação botões, panel e listbox
            PanelCliente.Enabled = false;
            ListBoxFiltro.Enabled = true;
            ToolStripButtonNovo.Enabled = true;
            ToolStripButtonGuardar.Enabled = false;
            ToolStripButtonAtualizar.Enabled = true;
            ToolStripButtonApagar.Enabled = true;
            ToolStripButtonProcurar.Enabled = true;
            ToolStripButtonFrente.Enabled = true;
            ToolStripButtonTras.Enabled = true;
            ToolStripButtonCancelar.Enabled = false;

            
        }

        /// <summary>
        /// Metodo para cancelar
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void ToolStripButtonCancelar_Click(object sender, EventArgs e)
        {
            // Verifica se alguma das textbox tem algum texto
            if (
                !string.IsNullOrEmpty(TextBoxNome.Text) ||
                !string.IsNullOrEmpty(TextBoxMorada.Text) ||
                !string.IsNullOrEmpty(TextBoxCodPostal.Text) ||
                !string.IsNullOrEmpty(TextBoxNif.Text) ||
                !string.IsNullOrEmpty(TextBoxUser.Text) ||
                !string.IsNullOrEmpty(TextBoxPassword.Text)
                )
            {
                // Mostra MessageBox para ter a certeza que quer cancelar
                if (MessageBox.Show("Tem a certeza que quer cancelar?", "Cancelar", MessageBoxButtons.OKCancel) == DialogResult.OK)
                {
                    LimparDados();
                }
                else
                {
                    return;
                }
            }

            CarregarListBox(_CRUDCliente.ReadCliente());

            // Ativa e desativação botões, panel e listbox
            PanelCliente.Enabled = false;
            ListBoxFiltro.Enabled = true;
            ToolStripButtonNovo.Enabled = true;
            ToolStripButtonGuardar.Enabled = false;
            ToolStripButtonAtualizar.Enabled = false;
            ToolStripButtonApagar.Enabled = false;
            ToolStripButtonProcurar.Enabled = true;
            ToolStripButtonFrente.Enabled = true;
            ToolStripButtonTras.Enabled = true;
            ToolStripButtonCancelar.Enabled = false;
        }

        /// <summary>
        /// Mostrar os dados do cliente selecionado na ListBoxFiltro
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void ListBoxFiltro_SelectedIndexChanged(object sender, EventArgs e)
        {
            idxListBox = ListBoxFiltro.SelectedIndex;

            if (idxListBox == -1)
            {
                return;
            }

            int idCliente = obterIdDaListBox();

            // Mostrar os dados do cliente selecionado
            MostrarDados(_CRUDCliente.ReadIdCliente(idCliente));

            // Ativar os botões apagar e atualizar
            ToolStripButtonApagar.Enabled = true;
            ToolStripButtonAtualizar.Enabled = true;
        }

        /// <summary>
        /// Metodo para obter o id do cliente da listBox
        /// </summary>
        /// <returns></returns>
        private int obterIdDaListBox()
        {
            // Obter a string do ListBox
            string curItem = ListBoxFiltro.SelectedItem.ToString();

            // Carater de separação
            char separa = '-';

            // passar para um arreio a string, fazendo a separação por -
            // Na listbox o formado é idCliente - Nome
            string[] etiqueta = curItem.Split(separa);

            // obter o idCliente
            return Convert.ToInt32(etiqueta[0]);
        }

        /// <summary>
        /// Botão que ativa textBox e botões de procura
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void ToolStripButtonProcurar_Click(object sender, EventArgs e)
        {
            PanelCliente.Enabled = true;
            ToolStripButtonCancelar.Enabled = true;
            TextBoxUser.Enabled = false;
            TextBoxPassword.Enabled = false;
            TextBoxId.Enabled = true;
            ButtonProcuraId.Enabled = true;
            ButtonProcuraNome.Enabled = true;
            ButtonProcuraMorada.Enabled = true;
            ButtonProcuraCodPostal.Enabled = true;
            ButtonProcuraNif.Enabled = true;
            ToolStripButtonAtualizar.Enabled = false;
            ToolStripButtonApagar.Enabled = false;
            ToolStripButtonFrente.Enabled = false;
            ToolStripButtonTras.Enabled = false;

            LimparDados();
        }

        /// <summary>
        /// Botão procurar por cliente
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void ButtonProcuraId_Click(object sender, EventArgs e)
        {
            int idProcura=0;

            if (Validacoes.ValidacaoInt(TextBoxId.Text))
            {
                MessageBox.Show("Não existe nenhum dado para procurar.");
                return;
            }

            // Guarda o id que está na textBoxId
            idProcura = Convert.ToInt32(TextBoxId.Text);

            // Cria uma lista onde consta o idProcura
            List<Cliente> lista = new List<Cliente> { _CRUDCliente.ReadIdCliente(idProcura) };

            // Mensagem de erro caso não existe nenhum cliente que tenha o id escolhido
            if (lista[0] == null)
            {
                MessageBox.Show($"Não existe nenhum cliente com o id {idProcura}");
                LimparDados();
                return;
            }

            MostrarDados(_CRUDCliente.ReadIdCliente(idProcura));

            CarregarListBox(lista);

            TextBoxUser.Enabled = true;
            TextBoxPassword.Enabled = true;
            PanelCliente.Enabled = false;
        }

        /// <summary>
        /// Botão procurar por nome
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void ButtonProcuraNome_Click(object sender, EventArgs e)
        {
            // Valida de textBoxNome tem algum dado
            if (Validacoes.ValidacaoText(TextBoxNome.Text))
            {
                MessageBox.Show($"Não existe nenhum dado para procurar.");
                return;
            }

            // Mensagem de erro caso não existem nenhum nome que contenha o texto que está a ser procurado
            if (_CRUDCliente.ProcurarNomeCliente(TextBoxNome.Text).Count == 0)
            {
                MessageBox.Show($"Não existe nenhum nome que contenha {TextBoxNome.Text}.");
                LimparDados();
                return;
            }

            CarregarListBox(_CRUDCliente.ProcurarNomeCliente(TextBoxNome.Text));

            MostrarDados(_CRUDCliente.ReadIdCliente(primeiroId));

            TextBoxUser.Enabled = true;
            TextBoxPassword.Enabled = true;
            PanelCliente.Enabled = false;
        }

        /// <summary>
        /// Botão procurar por morada
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void ButtonProcuraMorada_Click(object sender, EventArgs e)
        {

            // Valida se textBoxMorada tem algum dado para procurar
            if (Validacoes.ValidacaoText(TextBoxMorada.Text))
            {
                MessageBox.Show($"Não existe nenhum dado para procurar.");
                return;
            }

            // Mensagem de erro caso não exista nenhuma morada que contenha o texto que está a ser procurado
            if (_CRUDCliente.ProcurarMoradaCliente(TextBoxMorada.Text).Count == 0)
            {
                MessageBox.Show($"Não existe nenhuma morada que contenha {TextBoxMorada.Text}.");
                LimparDados();
                return;
            }

            CarregarListBox(_CRUDCliente.ProcurarMoradaCliente(TextBoxMorada.Text));

            MostrarDados(_CRUDCliente.ReadIdCliente(primeiroId));

            TextBoxUser.Enabled = true;
            TextBoxPassword.Enabled = true;
            PanelCliente.Enabled = false;
        }

        /// <summary>
        /// Botão procurar por Cód. Postal
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void ButtonProcuraCodPostal_Click(object sender, EventArgs e)
        {
            // Valida se textBoxCodPostal tem alguem dado para procurar
            if (Validacoes.ValidacaoText(TextBoxCodPostal.Text))
            {
                MessageBox.Show($"Não existe nenhum dado para procurar.");
                return;
            }

            // Mensagem de erro caso não exista nenhum código postal que contenha os texto que está a ser procurado
            if (_CRUDCliente.ProcurarCodPostalCliente(TextBoxCodPostal.Text).Count == 0)
            {
                MessageBox.Show($"Não existe nenhum Cód. Postal que contenha {TextBoxCodPostal.Text}.");
                LimparDados();
                return;
            }

            CarregarListBox(_CRUDCliente.ProcurarCodPostalCliente(TextBoxCodPostal.Text));

            MostrarDados(_CRUDCliente.ReadIdCliente(primeiroId));

            TextBoxUser.Enabled = true;
            TextBoxPassword.Enabled = true;
            PanelCliente.Enabled = false;
        }

        /// <summary>
        /// Botão procurar por NIF
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void ButtonProcuraNif_Click(object sender, EventArgs e)
        {
            // Valida se textBoxNif tem alguem dado para procurar
            if (Validacoes.ValidacaoText(TextBoxNif.Text))
            {
                MessageBox.Show($"Não existe nenhum dado válido para procurar.");
                return;
            }

            // Mensagem de erro caso não exista nenhum NIF que contenha os texto que está a ser procurado
            if (_CRUDCliente.ProcurarNifCliente(TextBoxNif.Text).Count == 0)
            {
                MessageBox.Show($"Não existe nenhum NIF que contenha {TextBoxNif.Text}.");
                LimparDados();
                return;
            }

            CarregarListBox(_CRUDCliente.ProcurarNifCliente(TextBoxNif.Text));

            MostrarDados(_CRUDCliente.ReadIdCliente(primeiroId));

            TextBoxUser.Enabled = true;
            TextBoxPassword.Enabled = true;
            PanelCliente.Enabled = false;
        }

        /// <summary>
        /// Botão apagar
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void ToolStripButtonApagar_Click(object sender, EventArgs e)
        {
            // Validação se é valor inteiro
            if(Validacoes.ValidacaoInt(TextBoxId.Text))
            {
                MessageBox.Show("Não existe nenhum Id válido para apagar.");
                return;
            }

            // Aguarda o id a apagar
            int id = Convert.ToInt32(TextBoxId.Text);

            // Apagar o id na BD
            if (!_CRUDCliente.DeleteCliente(id))
            {
                // Mensagem de erro caso não seja possivel apagar
                MessageBox.Show(_CRUDCliente.Retorno);
                return;
            }

            MessageBox.Show($"Cliente {id} apagado.");

            LimparDados();

            CarregarListBox(_CRUDCliente.ReadCliente());

            ToolStripButtonApagar.Enabled = false;
        }

        /// <summary>
        /// Botão atualizar
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void ToolStripButtonAtualizar_Click(object sender, EventArgs e)
        {
            // Ativa e desativação botões, panel e listbox
            PanelCliente.Enabled = true;
            ListBoxFiltro.Enabled = false;
            ToolStripButtonNovo.Enabled = false;
            ToolStripButtonGuardar.Enabled = true;
            ToolStripButtonAtualizar.Enabled = false;
            ToolStripButtonApagar.Enabled = false;
            ToolStripButtonProcurar.Enabled = false;
            ToolStripButtonFrente.Enabled = false;
            ToolStripButtonTras.Enabled = false;
            ToolStripButtonCancelar.Enabled = true;
            ButtonProcuraCodPostal.Enabled = false;
            ButtonProcuraId.Enabled = false;
            ButtonProcuraMorada.Enabled = false;
            ButtonProcuraNif.Enabled = false;
            ButtonProcuraNome.Enabled = false;

            // Marca variável novoRegisto para false, para fazer a destinção no botão save
            novoRegisto = false;

        }

        /// <summary>
        /// Botão avançar na lista de clientes
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void ToolStripButtonFrente_Click(object sender, EventArgs e)
        {
            idxListBox += 1;

            if (ListBoxFiltro.Items.Count <= idxListBox)
                idxListBox = 0;

            ListBoxFiltro.SelectedIndex = idxListBox;
        }

        /// <summary>
        /// Botão recuar na lista de clientes
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void ToolStripButtonTras_Click(object sender, EventArgs e)
        {
            idxListBox -= 1;

            if (idxListBox <= -1)
                idxListBox = ListBoxFiltro.Items.Count - 1;

            ListBoxFiltro.SelectedIndex = idxListBox;
        }
    }
}

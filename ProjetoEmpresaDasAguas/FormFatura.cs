﻿namespace ProjetoEmpresaDasAguas
{
    using LivrariaDeClasses;
    using System;
    using System.Collections.Generic;
    using System.ComponentModel;
    using System.Data;
    using System.Drawing;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;
    using System.Windows.Forms;

    public partial class FormFatura : Form
    {
        CRUDFatura _CRUDFatura;
        CRUDCliente _CRUDCliente;
        CRUDConsumo _CRUDConsumo;
        CRUDEscalao _CRUDEscalao;
        bool novoRegisto = false;
        List<Fatura> listaFaturas;

        public FormFatura()
        {
            InitializeComponent();

            _CRUDFatura = new CRUDFatura();
            _CRUDCliente = new CRUDCliente();
            _CRUDConsumo = new CRUDConsumo();
            _CRUDEscalao = new CRUDEscalao();
            ControlBox = false;

            ComboBoxIdCliente.DataSource = _CRUDCliente.ReadCliente();
            ComboBoxIdCliente.DisplayMember = "idCliente";

            ComboBoxIdCliente.SelectedIndex = -1;

            CriarColunasListViewFatura();

            //selecionar a linha completa
            ListViewFaturas.FullRowSelect = true; 
        }

        /// <summary>
        /// Crias as colunas da listViewFatura
        /// </summary>
        private void CriarColunasListViewFatura()
        {
            //Criar as colunas
            ListViewFaturas.Columns.Add("N. Fatura");
            ListViewFaturas.Columns.Add("N. Cliente");
            ListViewFaturas.Columns.Add("Data");
            ListViewFaturas.Columns.Add("Contagem");
            ListViewFaturas.Columns.Add("Total");
        }

        /// <summary>
        /// Botão Procurar da barra de Menu
        /// Ativa PanelFatura
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void ToolStripButtonProcurar_Click(object sender, EventArgs e)
        {
            PanelFatura.Enabled = true;
            NumericUpDownIva.Enabled = false;
            ComboBoxIdConsumo.Enabled = false;
            ToolStripButtonCancelar.Enabled = true;
            ToolStripButtonNovo.Enabled = false;
            TextBoxIdFatura.ReadOnly = false;
            ButtonProcurarIdCliente.Enabled = true;
            ButtonProcurarIdFatura.Enabled = true;
            ButtonProcurarData.Enabled = true;

            LimparDadosDoForm();
        }

        /// <summary>
        /// Botão procurar por Id de Fatura
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void ButtonProcurarIdFatura_Click(object sender, EventArgs e)
        {
            // Valida se foi introduzido um int
            if (Validacoes.ValidacaoInt(TextBoxIdFatura.Text))
            {
                MessageBox.Show("Tem que introduzir um numero inteiro para procurar.");
                return;
            }

            // Ler os dados da tabela faturas da BD
            Fatura fatura = _CRUDFatura.ReadFatura(Convert.ToInt32(TextBoxIdFatura.Text));

            // Se não existir nenhuma fatura com o ID selecionado
            if (fatura == null)
            {
                MessageBox.Show("Não existe nenhuma fatura com esse ID.");
                return;
            }

            // Ler os dados da tabela clientes da BD
            Cliente cliente = _CRUDCliente.ReadIdCliente(fatura.idCliente);

            // Mostrar os dados
            MostrarDados(fatura, cliente);
            CarregarListViewComMesmoIdCliente(fatura.idCliente);

            // Ativar e desativar os botões panel
            PanelFatura.Enabled = false;
            NumericUpDownIva.Enabled = true;
            ComboBoxIdConsumo.Enabled = true;
            ToolStripButtonAtualizar.Enabled = true;
            ToolStripButtonApagar.Enabled = true;
        }

        /// <summary>
        /// Metodo para mostrar os dados e carregar a listViewFatura com o mesmo IDcliente
        /// </summary>
        /// <param name="fatura"></param>
        /// <param name="cliente"></param>
        private void MostrarDados(Fatura fatura, Cliente cliente)
        {
            TextBoxIdFatura.Text = fatura.idFatura.ToString();
            DateTimePickerFatura.Value = Convert.ToDateTime(fatura.data);
            ComboBoxIdCliente.Text = fatura.idCliente.ToString();
            TextBoxNome.Text = cliente.nome;
            TextBoxMorada.Text = cliente.morada;
            TextBoxCodPostal.Text = cliente.codPostal;
            ComboBoxIdConsumo.Text = fatura.idConsumo.ToString();
            TextBoxContagem.Text = fatura.contagem.ToString();
            NumericUpDownIva.Value = (decimal)fatura.iva;
            TextBoxTotal.Text = fatura.total.ToString();
        }

        /// <summary>
        /// Metodo para carregar as faturas do cliente na listViewFaturas
        /// </summary>
        /// <param name="idCliente"></param>
        private void CarregarListViewComMesmoIdCliente(int idCliente)
        {
            ListViewFaturas.Items.Clear();

            //Lista de Faturas com o mesmo IDCliente
            listaFaturas = (from Faturas in _CRUDFatura.ReadFatura()
                               where Faturas.idCliente == idCliente
                               select Faturas).ToList();

            //Carregar dados para a ListViewFaturas
            foreach (Fatura func in listaFaturas)
            {
                ListViewItem item;

                item = ListViewFaturas.Items.Add(func.idFatura.ToString());
                item.SubItems.Add(func.idCliente.ToString());

                // Forçar a converter para data, porque está a dar erro para ir buscar
                // ToShortDateString()
                DateTime date = Convert.ToDateTime(func.data);
                item.SubItems.Add(date.ToShortDateString());

                item.SubItems.Add(func.contagem.ToString());
                item.SubItems.Add(func.total.ToString());
            }

            //formatar as colunas
            for (int idx = 0; idx <= 4; idx++)
            {
                ListViewFaturas.Columns[idx].AutoResize(ColumnHeaderAutoResizeStyle.HeaderSize);
            }
        }

        /// <summary>
        /// Botão cancelar
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void ToolStripButtonCancelar_Click(object sender, EventArgs e)
        {
            ToolStripButtonAtualizar.Enabled = false;
            ToolStripButtonApagar.Enabled = false;
            ToolStripButtonProcurar.Enabled = true;
            ToolStripButtonCancelar.Enabled = false;
            ToolStripButtonGuardar.Enabled = false;
            ToolStripButtonNovo.Enabled = true;
            PanelFatura.Enabled = false;
            NumericUpDownIva.Enabled = true;
            ComboBoxIdConsumo.Enabled = true;

            ListViewFaturas.Items.Clear();

            LimparDadosDoForm();
        }

        /// <summary>
        /// Metodo para limpar todos os dados do Form
        /// </summary>
        private void LimparDadosDoForm()
        {
            TextBoxIdFatura.Text = string.Empty;
            DateTimePickerFatura.Value = DateTime.Now;
            ComboBoxIdCliente.SelectedIndex = -1;
            // Para limpar quando é escrito para a procura
            ComboBoxIdCliente.Text = "";
            TextBoxNome.Text = string.Empty;
            TextBoxMorada.Text = string.Empty;
            TextBoxCodPostal.Text = string.Empty;
            ComboBoxIdConsumo.SelectedIndex = -1;
            TextBoxContagem.Text = string.Empty;
            NumericUpDownIva.Value = 0;
            TextBoxTotal.Text = string.Empty;
            ListViewFaturas.Items.Clear();
        }

        /// <summary>
        /// Botão procurar por data
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void ButtonProcurarData_Click(object sender, EventArgs e)
        {
            // Lista de todas as faturas com a data selecionada
            listaFaturas = (from Faturas
                                        in _CRUDFatura.ReadFatura()
                                         where Convert.ToDateTime(Faturas.data).ToShortDateString() == Convert.ToDateTime(DateTimePickerFatura.Value).ToShortDateString()
                                         select Faturas).ToList();

            // Apanhar os dados da primeira fatura da lista
            Fatura primeiraFatura = listaFaturas.FirstOrDefault();

            if (primeiraFatura == null)
            {
                MessageBox.Show("Não existe nenhuma fatura.");
                return;
            }
            // Apagar os dados do cliente em relação à primeira fatura
            Cliente primeiroCliente = _CRUDCliente.ReadIdCliente(primeiraFatura.idCliente);

            // Mostrar os dados
            MostrarDados(primeiraFatura, primeiroCliente);

            // Carregar a ListView om todas as faturas
            CarregarListViewTodasFaturas(listaFaturas);

            // Ativar e desativar os botões panel
            PanelFatura.Enabled = false;
            NumericUpDownIva.Enabled = true;
            ComboBoxIdConsumo.Enabled = true;
            ToolStripButtonAtualizar.Enabled = true;
            ToolStripButtonApagar.Enabled = true;
        }

        /// <summary>
        /// Metodo para carregar todas as faturas na listViewFatura
        /// </summary>
        /// <param name="listaFaturas"></param>
        private void CarregarListViewTodasFaturas(List<Fatura> listaFaturas)
        {
            // Limpar dados das liste View
            ListViewFaturas.Items.Clear();

            // Carregar dados na liste View
            foreach (Fatura fatura in listaFaturas)
            {
                ListViewItem item;

                item = ListViewFaturas.Items.Add(fatura.idFatura.ToString());
                item.SubItems.Add(fatura.idCliente.ToString());

                // Forçar a converter para data, porque está a dar erro para ir buscar
                // ToShortDateString()
                DateTime date = Convert.ToDateTime(fatura.data);
                item.SubItems.Add(date.ToShortDateString());

                item.SubItems.Add(fatura.contagem.ToString());
                item.SubItems.Add(fatura.total.ToString());
            }

            //formatar as colunas
            for (int idx = 0; idx <= 4; idx++)
            {
                ListViewFaturas.Columns[idx].AutoResize(ColumnHeaderAutoResizeStyle.HeaderSize);
            }
        }

        /// <summary>
        /// Botão procurar por Id Cliente
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void ButtonProcurarIdCliente_Click(object sender, EventArgs e)
        {
            int idCliente;

            // Verificar se é um numero inteiro
            if (!int.TryParse(ComboBoxIdCliente.Text, out idCliente))
            {
                MessageBox.Show("Tem que inserir um número de cliente válido.");
                return;
            }

            // Lista de todas as faturas com o ID de cliente escolhido
            listaFaturas = (from Faturas in _CRUDFatura.ReadFatura()
                                         where Faturas.idCliente == Convert.ToInt32(ComboBoxIdCliente.Text)
                                         select Faturas).ToList();

            // Verifica se existem fatura
            if (listaFaturas.Count == 0)
            {
                MessageBox.Show("Não existe nenhuma fatura para esse cliente.");
                return;
            }

            // Apanhar os dados da primeira fatura da lista
            Fatura primeiraFatura = listaFaturas.FirstOrDefault();

            // Apagar os dados do cliente em relação à primeira fatura
            Cliente primeiroCliente = _CRUDCliente.ReadIdCliente(primeiraFatura.idCliente);

            // Mostrar os dados
            MostrarDados(primeiraFatura, primeiroCliente);

            // Carregar a ListView om todas as faturas
            CarregarListViewTodasFaturas(listaFaturas);

            // Ativar e desativar os botões panel
            PanelFatura.Enabled = false;
            NumericUpDownIva.Enabled = true;
            ComboBoxIdConsumo.Enabled = true;
            ToolStripButtonAtualizar.Enabled = true;
            ToolStripButtonApagar.Enabled = true;
        }

        /// <summary>
        /// Adicionar consumo na ComboBoxIdConsumo da fatura selecionado
        /// </summary>
        /// <param name="idConsumo"></param>
        private void AdicionarConsumoNaComboCox(int idConsumo)
        {
            ComboBoxIdConsumo.Items.Add(idConsumo);
        }

        /// <summary>
        /// Botão apagar
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void ToolStripButtonApagar_Click(object sender, EventArgs e)
        {
            // Validação se é valor inteiro
            if (Validacoes.ValidacaoInt(TextBoxIdFatura.Text))
            {
                MessageBox.Show("Não existe nenhuma fatura com esse numero.");
                return;
            }

            // Aguarda o id a apagar
            int id = Convert.ToInt32(TextBoxIdFatura.Text);

            Fatura fatura = (from Faturas in _CRUDFatura.ReadFatura()
                             where Faturas.idFatura == id
                             select Faturas).FirstOrDefault();

            if (fatura == null)
            {
                MessageBox.Show("Não existe nenhuma fatura com esse numero para apagar.");
                return;
            }

            // Apagar o id na BD
            if (!_CRUDFatura.DeleteFatura(id))
            {
                // Mensagem de erro caso não seja possivel apagar
                MessageBox.Show("Não foi possivel apagar a fatura.\nTente mais tarde.");
                return;
            }

            MessageBox.Show($"Fatura {id} apagada com sucesso.");

            LimparDadosDoForm();

            ToolStripButtonCancelar.Enabled = false;
            ToolStripButtonApagar.Enabled = false;
            ToolStripButtonAtualizar.Enabled = false;
            ToolStripButtonGuardar.Enabled = false;
        }

        /// <summary>
        /// Botão Atualizar
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void ToolStripButtonAtualizar_Click(object sender, EventArgs e)
        {
            PanelFatura.Enabled = true;
            TextBoxIdFatura.ReadOnly = true;
            ToolStripButtonAtualizar.Enabled = false;
            ToolStripButtonGuardar.Enabled = true;
            ToolStripButtonApagar.Enabled = false;
            ToolStripButtonCancelar.Enabled = true;
            ToolStripButtonProcurar.Enabled = false;
            ButtonProcurarData.Enabled = false;
            ButtonProcurarIdCliente.Enabled = false;
            ButtonProcurarIdFatura.Enabled = false;

            // Mudar variável para validação no botão salvar
            novoRegisto = false;

            // Adicionar idConsumo na ComboBoxIdConsumo
            AdicionarConsumoNaComboCox(Convert.ToInt32(ComboBoxIdConsumo.Text));
        }

        /// <summary>
        /// Evento para quando é selecionado o cliente e mostra os dados do mesmo
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void ComboBoxIdCliente_SelectedIndexChanged(object sender, EventArgs e)
        {
            // Verifica se existe algum cliente selecionado
            if (ComboBoxIdCliente.SelectedIndex == -1)
            {
                return;
            }

            // Verifica se o texto é válido, para corrigir break ao iniciar o programa
            if (Validacoes.ValidacaoInt(ComboBoxIdCliente.Text))
            {
                return;
            }

            // Guardar os dados do cliente
            Cliente cliente = _CRUDCliente.ReadIdCliente(Convert.ToInt32(ComboBoxIdCliente.Text));

            // Mostrar os dados
            TextBoxNome.Text = cliente.nome;
            TextBoxMorada.Text = cliente.morada;
            TextBoxCodPostal.Text = cliente.codPostal;

            // Carregar ComboBoxIdConsumo só com Consumos disponiveis
            CarregarComboBoxIdConsumoDisponivel();
        }

        /// <summary>
        /// Carrega ComboBoxIdConsumo com os consumos disponiveis para faturar
        /// </summary>
        private void CarregarComboBoxIdConsumoDisponivel()
        {
            ComboBoxIdConsumo.Items.Clear();

            // Lista de todos os consumos do cliente
            List<Consumo> listaConsumosCliente = (from Consumos in _CRUDConsumo.ReadConsumo()
                                                  where Consumos.idCliente == Convert.ToInt32(ComboBoxIdCliente.Text)
                                                  select Consumos).ToList();

            // Lista de faturas emitidas ao cliente
            List<Fatura> listaFaturasCliente = (from Faturas in _CRUDFatura.ReadFatura()
                                                where Faturas.idCliente == Convert.ToInt32(ComboBoxIdCliente.Text)
                                                select Faturas).ToList();

            // Lista com consumos disponiveis para faturar
            List<Consumo> listaConsumosFaturar = new List<Consumo>(listaConsumosCliente);

            // Correr a lista de todos os consumo do cliente
            foreach (var consumos in listaConsumosCliente)
            {
                // Correr a lista das faturas do cliente
                foreach (var faturas in listaFaturasCliente)
                {
                    // Verifica se na lista das faturas existe o id de consumo e remove da listaConsumosFaturar os consumos
                    if (consumos.idConsumo == faturas.idConsumo)
                        listaConsumosFaturar.Remove(consumos);
                }
            }

            // Carregar dados na ComboBoxIdConsumo
            foreach (var item in listaConsumosFaturar)
            {
                ComboBoxIdConsumo.Items.Add(item.idConsumo);
            }

            ComboBoxIdConsumo.SelectedIndex = -1;
            TextBoxContagem.Text = string.Empty;
        }

        /// <summary>
        /// Botão salvar
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void ToolStripButtonGuardar_Click(object sender, EventArgs e)
        {

            // Verifica se existe cliente selecionado
            if (ComboBoxIdCliente.SelectedIndex == -1)
            {
                MessageBox.Show("Tem que selecionar um N. de cliente válido.");
                return;
            }

            // Verifica se exite Consumo selecionado
            if (ComboBoxIdConsumo.SelectedIndex == -1)
            {
                MessageBox.Show("Tem que escolher um N. de consumo");
                return;
            }

            // Guardar contagem
            int contagem = Convert.ToInt32(TextBoxContagem.Text);

            // Guardar iva
            double iva = Convert.ToDouble(NumericUpDownIva.Value);

            // Guardar Valor Total da fatura
            double valorTotal = CalcularValorTotal(contagem, iva);

            if (novoRegisto)
            {
                // Procura se existe alguma fatura mais nova que a data que está a ser inserida
                Fatura ultimaFatura = _CRUDFatura.ReadFatura().
                    Where(f => f.data > Convert.ToDateTime(DateTimePickerFatura.Value) && f.idCliente == Convert.ToInt32(ComboBoxIdCliente.Text)).
                    OrderByDescending(f => f.data).FirstOrDefault();

                if (ultimaFatura != null)
                {
                    MessageBox.Show($"Não pode inserir uma fatura com data inferior á ultima fatura inserida.\n{Convert.ToDateTime(ultimaFatura.data).ToShortDateString()}");
                    return;
                }

                Fatura novaFatura = new Fatura
                {
                    idCliente = Convert.ToInt32(ComboBoxIdCliente.Text),
                    idConsumo = Convert.ToInt32(ComboBoxIdConsumo.Text),
                    data = Convert.ToDateTime(DateTimePickerFatura.Value),
                    contagem = Convert.ToInt32(TextBoxContagem.Text),
                    iva = Convert.ToDouble(NumericUpDownIva.Value),
                    total = valorTotal,
                };

                if (!_CRUDFatura.CreateFatura(novaFatura))
                {
                    MessageBox.Show("Não foi possivel inserir a fatura.");
                    return;
                }

                TextBoxIdFatura.Text = novaFatura.idFatura.ToString();
                TextBoxTotal.Text = novaFatura.total.ToString();

                MessageBox.Show("Fatura inserida com sucesso.");

            }
            else
            {
                Fatura atualizarFatura = new Fatura
                {
                    idFatura = Convert.ToInt32(TextBoxIdFatura.Text),
                    idCliente = Convert.ToInt32(ComboBoxIdCliente.Text),
                    idConsumo = Convert.ToInt32(ComboBoxIdConsumo.Text),
                    data = Convert.ToDateTime(DateTimePickerFatura.Value),
                    contagem = Convert.ToInt32(TextBoxContagem.Text),
                    iva = Convert.ToDouble(NumericUpDownIva.Value),
                    total = valorTotal,
                };

                if (!_CRUDFatura.UpdateFatura(atualizarFatura))
                {
                    MessageBox.Show("Não foi possivel atualizar a fatura.");
                    return;
                }

                MessageBox.Show("Fatura atualizada com sucesso.");
            }

            CarregarListViewComMesmoIdCliente(Convert.ToInt32(ComboBoxIdCliente.Text));

            PanelFatura.Enabled = false;
            TextBoxIdFatura.ReadOnly = false;
            ToolStripButtonAtualizar.Enabled = true;
            ToolStripButtonGuardar.Enabled = false;
            ToolStripButtonApagar.Enabled = true;
            ToolStripButtonCancelar.Enabled = true;
            ToolStripButtonProcurar.Enabled = true;

        }

        /// <summary>
        /// Metodo para calcular o valor total da fatura com IVA
        /// </summary>
        private double CalcularValorTotal(int contagem, double iva)
        {
            return (CalcularValorSubTotal(contagem) * iva / 100) + CalcularValorSubTotal(contagem);
        }

        /// <summary>
        /// Metodo para calcular o subtotal da fatura
        /// </summary>
        /// <param name="contagem"></param>
        /// <returns></returns>
        private double CalcularValorSubTotal(int contagem)
        {
            // Obter a lista de todos os escaloes onde entra a contagem
            var listaEscaloes = _CRUDEscalao.ReadEscalaoPorContagem(contagem);

            double subTotal = 0;
            int jaEmitido = 0;

            // ciclo para calcular o subtotal
            for (int i = 0; i < listaEscaloes.Count; i++)
            {
                // se for o ultimo idx da lista
                if (i == listaEscaloes.Count - 1)
                {
                    subTotal += listaEscaloes[i].valorUnitario * contagem;
                }
                else
                {
                    // Calculo do subtotal:
                    // 1 - Vai buscar o valorUnitario do idx
                    // 2 - Vai buscar o valor minimo do escalao a seguir e retira 1
                    // 3 - Ao ponto 2 retiramos a contagem que já foi contabilizada
                    // 4 - Multiplica-se o ponto 1 com o ponto 3
                    subTotal += listaEscaloes[i].valorUnitario * (listaEscaloes[i + 1].minimo - 1 - jaEmitido);
                    contagem -= listaEscaloes[i + 1].minimo - 1 - jaEmitido;
                    jaEmitido = listaEscaloes[i + 1].minimo - 1;
                }
            }

            return subTotal;
        }

        /// <summary>
        /// Evento para quando é selecionao o consumo e mostra a contagem
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void ComboBoxIdConsumo_SelectedIndexChanged(object sender, EventArgs e)
        {
            // Verifica se existe algum cliente selecionado
            if (ComboBoxIdConsumo.SelectedIndex == -1)
            {
                return;
            }

            // Verifica se o texto é válido, para corrigir break ao iniciar o programa
            if (Validacoes.ValidacaoInt(ComboBoxIdConsumo.Text))
            {
                return;
            }

            var consumo = _CRUDConsumo.ReadConsumoPorIdConsumo(Convert.ToInt32(ComboBoxIdConsumo.Text));

            TextBoxContagem.Text = consumo.contagem.ToString();
        }

        /// <summary>
        /// Botão novo registo
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void ToolStripButtonNovo_Click(object sender, EventArgs e)
        {
            novoRegisto = true;
            PanelFatura.Enabled = true;
            ToolStripButtonCancelar.Enabled = true;
            ToolStripButtonProcurar.Enabled = false;
            ToolStripButtonNovo.Enabled = false;
            ToolStripButtonGuardar.Enabled = true;
            ButtonProcurarData.Enabled = false;
            ButtonProcurarIdCliente.Enabled = false;
            ButtonProcurarIdFatura.Enabled = false;
            TextBoxIdFatura.ReadOnly = true;

            DateTimePickerFatura.Value = DateTime.Now;
        }

        /// <summary>
        /// Mostar fatura selecionada na ListView
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void ListViewFaturas_MouseClick(object sender, MouseEventArgs e)
        {
            int idFaturaSelecionada = Convert.ToInt32(ListViewFaturas.SelectedItems[0].Text);
            //int idClienteSelecionado = Convert.ToInt32(ListViewFaturas.SelectedItems[1].Text);

            Fatura faturaSelecionada = (from Faturas in _CRUDFatura.ReadFatura()
                            where Faturas.idFatura == idFaturaSelecionada
                                        select Faturas).FirstOrDefault();
            Cliente clienteSelecionado = _CRUDCliente.ReadIdCliente(faturaSelecionada.idCliente);

            MostrarDados(faturaSelecionada, clienteSelecionado);
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using WindowsFormsCliente.Modelos;
using WindowsFormsCliente.Servicos;

namespace WindowsFormsCliente
{
    public partial class Dados : Form
    {
        private Cliente cliente;

        private Cliente MostraCliente;

        private NetworkServices networkServices; //chamo objecto do tipo networkservices(que tem la dentro o try do response)

        private ApiServicesCliente apiServices;

        private DialogServices dialogServices;//crio objecto mensagem


        public Dados(Cliente cliente)
        {
            InitializeComponent();

            this.cliente = cliente;

            ControlBox = false;

            networkServices = new NetworkServices();

            apiServices = new ApiServicesCliente();

            dialogServices = new DialogServices();//instanciar objecto mensagem

            LoadCliente();
        }
        
        /// <summary>
        /// carrega os dados do cliente no form
        /// </summary>
        private async void LoadCliente() //este metodo tem de ser async, por causa do await
        {
            var connection = networkServices.CheckConnection();//variavel ganhas as propriedades do network(torna-se uma variavel do tipo response)

            if (!connection.IsSuccess)//caso nao tenha conexão
            {
                MessageBox.Show(connection.Message);
                return;
            }
            else
            {
                await LoadApiCliente();//carrega da api
            }

            if (MostraCliente == null)//na primeira ligacao, caso nao tenha internet, por isso nao tenho ainda carregada a lista local
            {
                //environment.new line mete o texto numa linha nova na label
                
                return;
            }

            TextBoxId.Text = MostraCliente.idCliente.ToString();
            TextBoxNome.Text = MostraCliente.nome;
            TextBoxMorada.Text = MostraCliente.morada;
            TextBoxCodPostal.Text = MostraCliente.codPostal;
            TextBoxNif.Text = MostraCliente.nif.ToString();
            
        }


        /// <summary>
        /// carrega os dados da api por idcliente
        /// </summary>
        /// <returns></returns>
        private async Task LoadApiCliente()//async tem de ter um await e vice-versa
        {
            //LabelResultado.Text = "A carregar clientes.";
            var response = await apiServices.GetClientes("http://empagua.somee.com", "/api/Clientes/"+cliente.idCliente);

            MostraCliente = (Cliente)response.Result;//vem como objecto e converto num cliente
        }

    }
}

﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using WindowsFormsCliente.Modelos;
using WindowsFormsCliente.Servicos;

namespace WindowsFormsCliente
{
    public partial class Consumos : Form
    {
        private Cliente cliente;
        
        private List<Consumo> MostraConsumo;

        private NetworkServices networkServices; //chamo objecto do tipo networkservices(que tem la dentro o try do response)

        private ApiServicesConsumo apiServices;

        private DialogServices dialogServices;//crio objecto mensagem


        public Consumos(Cliente cliente)
        {
            InitializeComponent();

            this.cliente = cliente;

            ControlBox = false;

            networkServices = new NetworkServices();

            apiServices = new ApiServicesConsumo();

            dialogServices = new DialogServices();//instanciar objecto mensagem

            LoadConsumo();

        }


        /// <summary>
        /// carregar os consumos no form
        /// </summary>
        private async void LoadConsumo() //este metodo tem de ser async, por causa do await
        {
            var connection = networkServices.CheckConnection();//variavel ganhas as propriedades do network(torna-se uma variavel do tipo response)

            if (!connection.IsSuccess)//caso nao tenha conexão
            {
                MessageBox.Show(connection.Message);
                return;
            }
            else
            {
                await LoadApiConsumo();//carrega da api
            }

            if (MostraConsumo == null)
            {
                //environment.new line mete o texto numa linha nova na label
                LabelResultado.Visible = true;
                LabelResultado.Text = "Este cliente não tem consumos";
                return;
            }

            foreach (var consumo in MostraConsumo)
            {
                ListBoxConsumos.Items.Add(consumo.idConsumo.ToString());
            }


            ListBoxConsumos.SelectedIndex = 0;
        }


        /// <summary>
        /// carregar os dados da api por idcliente
        /// </summary>
        /// <returns></returns>
        private async Task LoadApiConsumo()//async tem de ter um await e vice-versa
        {
            //LabelResultado.Text = "A carregar clientes.";
            var response = await apiServices.GetConsumos("http://empagua.somee.com", "/api/Consumos/GetConsumos/" + cliente.idCliente);

            MostraConsumo = (List<Consumo>)response.Result;//vem como objecto e converto num cliente
        }

        /// <summary>
        /// alterar os dados da textbox ao selecionar o consumo
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void ListBoxConsumos_SelectedIndexChanged(object sender, EventArgs e)
        {
            Consumo lista = (from Consumos in MostraConsumo where Consumos.idConsumo == Convert.ToInt32(ListBoxConsumos.SelectedItem.ToString()) select Consumos).Single();
            TextBoxId.Text = lista.idConsumo.ToString();
            TextBoxData.Text = lista.data.ToShortDateString();
            textBoxContagem.Text = lista.contagem.ToString();
        }
    }
}

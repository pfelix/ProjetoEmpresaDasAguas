﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using WindowsFormsCliente.Modelos;

namespace WindowsFormsCliente
{
    public partial class FormDadosCliente : Form
    {
        Cliente cliente;

        public FormDadosCliente(Cliente cliente)
        {
            InitializeComponent();
            this.cliente = cliente;

            dadosClienteToolStripMenuItem.Enabled = false;
            consumosToolStripMenuItem.Enabled = true;
            faturasToolStripMenuItem.Enabled = true;
            
            Dados dados = new Dados(cliente);
            dados.MdiParent = this;
            dados.Show();
           
        }
        
        /// <summary>
        /// carrega a cor do form
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void FormDadosCliente_Load(object sender, EventArgs e)
        {
            AlterarCorForm();
        }

        /// <summary>
        /// altera a cor do mdiparent
        /// </summary>
        private void AlterarCorForm()
        {
            // Classe que contem os multiplos formulários
            MdiClient mdiClient;

            // Procura os controlos do tipo MdiClient
            foreach (Control var in this.Controls)
            {
                try
                {
                    // cast para ser MdiClient
                    mdiClient = (MdiClient)var;

                    // atribui cor ao MdiClient
                    mdiClient.BackColor = this.BackColor;
                }
                catch (Exception)
                {
                    // Se cast falhar ignora o erro
                }
            }
        }

        /// <summary>
        /// chama o form dos dados do cliente
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void dadosClienteToolStripMenuItem_Click(object sender, EventArgs e)
        {
            dadosClienteToolStripMenuItem.Enabled = false;
            consumosToolStripMenuItem.Enabled = true;
            faturasToolStripMenuItem.Enabled = true;

            Dados dados = new Dados(cliente);
            dados.MdiParent = this;
            dados.Show();
        }

        /// <summary>
        /// chama o form dos consumos
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void consumosToolStripMenuItem_Click(object sender, EventArgs e)
        {
            dadosClienteToolStripMenuItem.Enabled = true;
            consumosToolStripMenuItem.Enabled = false;
            faturasToolStripMenuItem.Enabled = true;

            Consumos consumos = new Consumos(cliente);
            consumos.MdiParent = this;
            consumos.Show();
        }

        /// <summary>
        /// chama o form das faturas
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void faturasToolStripMenuItem_Click(object sender, EventArgs e)
        {
            dadosClienteToolStripMenuItem.Enabled = true;
            consumosToolStripMenuItem.Enabled = true;
            faturasToolStripMenuItem.Enabled = false;

            Faturas faturas = new Faturas(cliente);
            faturas.MdiParent = this;
            faturas.Show();
        }

        /// <summary>
        /// faz o logout e volta ao login
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void logoutToolStripMenuItem_Click(object sender, EventArgs e)
        {

            this.Hide();
            Login sistema = new Login();
            sistema.ShowDialog();
            this.Close();

        }
    }
}

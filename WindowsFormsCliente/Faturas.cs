﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using WindowsFormsCliente.Modelos;
using WindowsFormsCliente.Servicos;

namespace WindowsFormsCliente
{
    public partial class Faturas : Form
    {
        private Cliente cliente;

        private List<Fatura> MostraFatura;

        private NetworkServices networkServices; //chamo objecto do tipo networkservices(que tem la dentro o try do response)

        private ApiServicesFatura apiServices;

        private DialogServices dialogServices;//crio objecto mensagem


        public Faturas(Cliente cliente)
        {
            InitializeComponent();

            this.cliente = cliente;

            ControlBox = false;

            networkServices = new NetworkServices();

            apiServices = new ApiServicesFatura();

            dialogServices = new DialogServices();//instanciar objecto mensagem

            LoadFatura();
        }

        /// <summary>
        /// carrega os dados das faturas no form
        /// </summary>
        private async void LoadFatura() //este metodo tem de ser async, por causa do await
        {
            var connection = networkServices.CheckConnection();//variavel ganhas as propriedades do network(torna-se uma variavel do tipo response)

            if (!connection.IsSuccess)//caso nao tenha conexão
            {
                MessageBox.Show(connection.Message);
                return;
            }
            else
            {
                await LoadApiFatura();//carrega da api
            }



            if (MostraFatura == null)
            {
                //environment.new line mete o texto numa linha nova na label
                LabelResultado.Visible = true;
                LabelResultado.Text = "Este cliente não tem Faturas";
                return;
            }

            foreach (var fatura in MostraFatura)
            {
                ListBoxFaturas.Items.Add(fatura.idFatura.ToString());
            }

            ListBoxFaturas.SelectedIndex = 0;
        }

        /// <summary>
        /// carrega da api as faturas selecionando por idcliente
        /// </summary>
        /// <returns></returns>
        private async Task LoadApiFatura()//async tem de ter um await e vice-versa
        {
            //LabelResultado.Text = "A carregar clientes.";
            var response = await apiServices.GetFaturas("http://empagua.somee.com", "/api/Faturas/GetFaturas/" + cliente.idCliente);

            MostraFatura = (List<Fatura>)response.Result;//vem como objecto e converto num cliente
        }

        /// <summary>
        /// altera a info nas textbox, quando seleciona as faturas
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void ListBoxFaturas_SelectedIndexChanged(object sender, EventArgs e)
        {
            Fatura lista = (from faturas in MostraFatura where faturas.idFatura == Convert.ToInt32(ListBoxFaturas.SelectedItem.ToString()) select faturas).Single();
            TextBoxIdFatura.Text = lista.idFatura.ToString();
            TextBoxIdConsumo.Text = lista.idConsumo.ToString();
            TextBoxData.Text = lista.data.ToShortDateString();
            TextBoxContagem.Text = lista.contagem.ToString();
            TextBoxIva.Text = lista.iva.ToString()+" %";
            TextBoxTotal.Text = lista.total.ToString()+" Euros";
        }
    }
}

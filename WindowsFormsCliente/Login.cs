﻿namespace WindowsFormsCliente
{
    using System;
    using System.Threading.Tasks;
    using System.Windows.Forms;
    using Modelos;
    using Servicos;


    public partial class Login : Form
    {
        private Cliente Cliente;
        
        private NetworkServices networkServices; //chamo objecto do tipo networkservices(que tem la dentro o try do response)

        private ApiServicesCliente apiServices;

        private DialogServices dialogServices;//crio objecto mensagem

        FormDadosCliente formcliente;

        public Login()
        {
            InitializeComponent();
            
            networkServices = new NetworkServices();

            apiServices = new ApiServicesCliente();

            dialogServices = new DialogServices();//instanciar objecto mensagem
        }

        /// <summary>
        /// carrega da os dados do cliente
        /// </summary>
        private async void LoadCliente() //este metodo tem de ser async, por causa do await
        {
            LabelResultado.Text = "A verificar utilizador...";

            var connection = networkServices.CheckConnection();//variavel ganhas as propriedades do network(torna-se uma variavel do tipo response)

            if (!connection.IsSuccess)//caso nao tenha conexão
            {
                MessageBox.Show(connection.Message);
                ButtonLogin.Enabled = true;
                return;
            }
            else
            {
                await LoadApiCliente();//carrega da api
            }

            if (Cliente == null)//na primeira ligacao, caso nao tenha internet, por isso nao tenho ainda carregada a lista local
            {
                //environment.new line mete o texto numa linha nova na label
                LabelUser.Visible = false;
                LabelPass.Visible = false;
                LabelResultado.Visible = true;
                LabelResultado.Text = "Utilizador ou Password incorrectos." + Environment.NewLine + "Tente novamente!";
                TextBoxUser.Clear();
                TextBoxPass.Clear();
                ButtonLogin.Enabled = true;
                return;
            }

            this.Hide();

            formcliente = new FormDadosCliente((Cliente)Cliente);
            
            formcliente.ShowDialog();
            
            ButtonLogin.Enabled = false;
           
            this.Close();

        }

        /// <summary>
        /// verifica se existe e carrega da api
        /// </summary>
        /// <returns></returns>
        private async Task LoadApiCliente()//async tem de ter um await e vice-versa
        {
            //LabelResultado.Text = "A carregar clientes.";
            var response = await apiServices.GetClientes("http://empagua.somee.com", "/api/Clientes/GetUser/"+TextBoxUser.Text+" "+TextBoxPass.Text);
            
            Cliente = (Cliente)response.Result;//vem como objecto e converto num cliente
        }

        /// <summary>
        /// caso o user e a pass sejam as correctas faz o login
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void ButtonLogin_Click(object sender, EventArgs e)
        {
            ButtonLogin.Enabled = false;

            if (LivrariaDeClasses.Validacoes.ValidacaoText(TextBoxUser.Text))
            {
                LabelUser.Visible = true;
                LabelPass.Visible = false;
                LabelUser.Text ="Insira o seu Utilizador.";
                LabelResultado.Visible = false;
                ButtonLogin.Enabled = true;
                return;
            }

            if (LivrariaDeClasses.Validacoes.ValidacaoText(TextBoxPass.Text))
            {
                LabelPass.Visible = true;
                LabelUser.Visible = false;
                LabelPass.Text = "Insira a Password.";
                LabelResultado.Visible = false;
                ButtonLogin.Enabled = true;
                return;
            }

            LoadCliente();

            

        }
    }
}

﻿namespace WindowsFormsCliente
{
    partial class FormDadosCliente
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.menuStrip1 = new System.Windows.Forms.MenuStrip();
            this.dadosClienteToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.consumosToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.faturasToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.logoutToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.menuStrip1.SuspendLayout();
            this.SuspendLayout();
            // 
            // menuStrip1
            // 
            this.menuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.dadosClienteToolStripMenuItem,
            this.consumosToolStripMenuItem,
            this.faturasToolStripMenuItem,
            this.logoutToolStripMenuItem});
            this.menuStrip1.Location = new System.Drawing.Point(0, 0);
            this.menuStrip1.Name = "menuStrip1";
            this.menuStrip1.Size = new System.Drawing.Size(484, 24);
            this.menuStrip1.TabIndex = 0;
            this.menuStrip1.Text = "menuStrip1";
            // 
            // dadosClienteToolStripMenuItem
            // 
            this.dadosClienteToolStripMenuItem.Name = "dadosClienteToolStripMenuItem";
            this.dadosClienteToolStripMenuItem.Size = new System.Drawing.Size(92, 20);
            this.dadosClienteToolStripMenuItem.Text = "Dados Cliente";
            this.dadosClienteToolStripMenuItem.Click += new System.EventHandler(this.dadosClienteToolStripMenuItem_Click);
            // 
            // consumosToolStripMenuItem
            // 
            this.consumosToolStripMenuItem.Name = "consumosToolStripMenuItem";
            this.consumosToolStripMenuItem.Size = new System.Drawing.Size(76, 20);
            this.consumosToolStripMenuItem.Text = "Consumos";
            this.consumosToolStripMenuItem.Click += new System.EventHandler(this.consumosToolStripMenuItem_Click);
            // 
            // faturasToolStripMenuItem
            // 
            this.faturasToolStripMenuItem.Name = "faturasToolStripMenuItem";
            this.faturasToolStripMenuItem.Size = new System.Drawing.Size(57, 20);
            this.faturasToolStripMenuItem.Text = "Faturas";
            this.faturasToolStripMenuItem.Click += new System.EventHandler(this.faturasToolStripMenuItem_Click);
            // 
            // logoutToolStripMenuItem
            // 
            this.logoutToolStripMenuItem.Name = "logoutToolStripMenuItem";
            this.logoutToolStripMenuItem.Size = new System.Drawing.Size(57, 20);
            this.logoutToolStripMenuItem.Text = "Logout";
            this.logoutToolStripMenuItem.Click += new System.EventHandler(this.logoutToolStripMenuItem_Click);
            // 
            // FormDadosCliente
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(484, 311);
            this.Controls.Add(this.menuStrip1);
            this.IsMdiContainer = true;
            this.MainMenuStrip = this.menuStrip1;
            this.Name = "FormDadosCliente";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Dados do Cliente";
            this.Load += new System.EventHandler(this.FormDadosCliente_Load);
            this.menuStrip1.ResumeLayout(false);
            this.menuStrip1.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.MenuStrip menuStrip1;
        private System.Windows.Forms.ToolStripMenuItem dadosClienteToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem consumosToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem faturasToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem logoutToolStripMenuItem;
    }
}
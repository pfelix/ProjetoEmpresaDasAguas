﻿namespace WindowsFormsCliente
{
    partial class Consumos
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.ListBoxConsumos = new System.Windows.Forms.ListBox();
            this.TextBoxId = new System.Windows.Forms.TextBox();
            this.TextBoxData = new System.Windows.Forms.TextBox();
            this.textBoxContagem = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.LabelResultado = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // ListBoxConsumos
            // 
            this.ListBoxConsumos.BackColor = System.Drawing.SystemColors.MenuBar;
            this.ListBoxConsumos.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.ListBoxConsumos.FormattingEnabled = true;
            this.ListBoxConsumos.Location = new System.Drawing.Point(303, 54);
            this.ListBoxConsumos.Name = "ListBoxConsumos";
            this.ListBoxConsumos.ScrollAlwaysVisible = true;
            this.ListBoxConsumos.Size = new System.Drawing.Size(132, 197);
            this.ListBoxConsumos.TabIndex = 0;
            this.ListBoxConsumos.SelectedIndexChanged += new System.EventHandler(this.ListBoxConsumos_SelectedIndexChanged);
            // 
            // TextBoxId
            // 
            this.TextBoxId.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TextBoxId.Location = new System.Drawing.Point(78, 54);
            this.TextBoxId.Name = "TextBoxId";
            this.TextBoxId.ReadOnly = true;
            this.TextBoxId.Size = new System.Drawing.Size(170, 26);
            this.TextBoxId.TabIndex = 4;
            // 
            // TextBoxData
            // 
            this.TextBoxData.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TextBoxData.Location = new System.Drawing.Point(78, 136);
            this.TextBoxData.Name = "TextBoxData";
            this.TextBoxData.ReadOnly = true;
            this.TextBoxData.Size = new System.Drawing.Size(170, 26);
            this.TextBoxData.TabIndex = 6;
            // 
            // textBoxContagem
            // 
            this.textBoxContagem.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textBoxContagem.Location = new System.Drawing.Point(78, 224);
            this.textBoxContagem.Name = "textBoxContagem";
            this.textBoxContagem.ReadOnly = true;
            this.textBoxContagem.Size = new System.Drawing.Size(170, 26);
            this.textBoxContagem.TabIndex = 7;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(78, 26);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(95, 20);
            this.label1.TabIndex = 8;
            this.label1.Text = "Id Consumo";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.Location = new System.Drawing.Point(78, 108);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(44, 20);
            this.label2.TabIndex = 9;
            this.label2.Text = "Data";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.Location = new System.Drawing.Point(78, 196);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(83, 20);
            this.label3.TabIndex = 10;
            this.label3.Text = "Contagem";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.Location = new System.Drawing.Point(299, 26);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(77, 20);
            this.label4.TabIndex = 11;
            this.label4.Text = "Consumo";
            // 
            // LabelResultado
            // 
            this.LabelResultado.AutoSize = true;
            this.LabelResultado.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LabelResultado.ForeColor = System.Drawing.Color.Red;
            this.LabelResultado.Location = new System.Drawing.Point(75, 264);
            this.LabelResultado.Name = "LabelResultado";
            this.LabelResultado.Size = new System.Drawing.Size(45, 16);
            this.LabelResultado.TabIndex = 12;
            this.LabelResultado.Text = "label5";
            this.LabelResultado.Visible = false;
            // 
            // Consumos
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(484, 291);
            this.Controls.Add(this.LabelResultado);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.textBoxContagem);
            this.Controls.Add(this.TextBoxData);
            this.Controls.Add(this.TextBoxId);
            this.Controls.Add(this.ListBoxConsumos);
            this.Name = "Consumos";
            this.Text = "Consumos";
            this.WindowState = System.Windows.Forms.FormWindowState.Maximized;
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.ListBox ListBoxConsumos;
        private System.Windows.Forms.TextBox TextBoxId;
        private System.Windows.Forms.TextBox TextBoxData;
        private System.Windows.Forms.TextBox textBoxContagem;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label LabelResultado;
    }
}
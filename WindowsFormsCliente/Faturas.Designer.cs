﻿namespace WindowsFormsCliente
{
    partial class Faturas
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.LabelResultado = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.TextBoxContagem = new System.Windows.Forms.TextBox();
            this.TextBoxData = new System.Windows.Forms.TextBox();
            this.TextBoxIdFatura = new System.Windows.Forms.TextBox();
            this.ListBoxFaturas = new System.Windows.Forms.ListBox();
            this.label5 = new System.Windows.Forms.Label();
            this.TextBoxIdConsumo = new System.Windows.Forms.TextBox();
            this.label6 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.TextBoxTotal = new System.Windows.Forms.TextBox();
            this.TextBoxIva = new System.Windows.Forms.TextBox();
            this.SuspendLayout();
            // 
            // LabelResultado
            // 
            this.LabelResultado.AutoSize = true;
            this.LabelResultado.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LabelResultado.ForeColor = System.Drawing.Color.Red;
            this.LabelResultado.Location = new System.Drawing.Point(20, 263);
            this.LabelResultado.Name = "LabelResultado";
            this.LabelResultado.Size = new System.Drawing.Size(45, 16);
            this.LabelResultado.TabIndex = 21;
            this.LabelResultado.Text = "label5";
            this.LabelResultado.Visible = false;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.Location = new System.Drawing.Point(319, 17);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(56, 20);
            this.label4.TabIndex = 20;
            this.label4.Text = "Fatura";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.Location = new System.Drawing.Point(166, 113);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(83, 20);
            this.label3.TabIndex = 19;
            this.label3.Text = "Contagem";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.Location = new System.Drawing.Point(19, 114);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(44, 20);
            this.label2.TabIndex = 18;
            this.label2.Text = "Data";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(15, 21);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(74, 20);
            this.label1.TabIndex = 17;
            this.label1.Text = "Id Fatura";
            // 
            // TextBoxContagem
            // 
            this.TextBoxContagem.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TextBoxContagem.Location = new System.Drawing.Point(166, 137);
            this.TextBoxContagem.Name = "TextBoxContagem";
            this.TextBoxContagem.ReadOnly = true;
            this.TextBoxContagem.Size = new System.Drawing.Size(128, 26);
            this.TextBoxContagem.TabIndex = 16;
            // 
            // TextBoxData
            // 
            this.TextBoxData.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TextBoxData.Location = new System.Drawing.Point(19, 137);
            this.TextBoxData.Name = "TextBoxData";
            this.TextBoxData.ReadOnly = true;
            this.TextBoxData.Size = new System.Drawing.Size(123, 26);
            this.TextBoxData.TabIndex = 15;
            // 
            // TextBoxIdFatura
            // 
            this.TextBoxIdFatura.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TextBoxIdFatura.Location = new System.Drawing.Point(15, 45);
            this.TextBoxIdFatura.Name = "TextBoxIdFatura";
            this.TextBoxIdFatura.ReadOnly = true;
            this.TextBoxIdFatura.Size = new System.Drawing.Size(123, 26);
            this.TextBoxIdFatura.TabIndex = 14;
            // 
            // ListBoxFaturas
            // 
            this.ListBoxFaturas.BackColor = System.Drawing.SystemColors.MenuBar;
            this.ListBoxFaturas.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.ListBoxFaturas.FormattingEnabled = true;
            this.ListBoxFaturas.Location = new System.Drawing.Point(323, 45);
            this.ListBoxFaturas.Name = "ListBoxFaturas";
            this.ListBoxFaturas.ScrollAlwaysVisible = true;
            this.ListBoxFaturas.Size = new System.Drawing.Size(132, 210);
            this.ListBoxFaturas.TabIndex = 13;
            this.ListBoxFaturas.SelectedIndexChanged += new System.EventHandler(this.ListBoxFaturas_SelectedIndexChanged);
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label5.Location = new System.Drawing.Point(166, 19);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(95, 20);
            this.label5.TabIndex = 23;
            this.label5.Text = "Id Consumo";
            // 
            // TextBoxIdConsumo
            // 
            this.TextBoxIdConsumo.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TextBoxIdConsumo.Location = new System.Drawing.Point(166, 45);
            this.TextBoxIdConsumo.Name = "TextBoxIdConsumo";
            this.TextBoxIdConsumo.ReadOnly = true;
            this.TextBoxIdConsumo.Size = new System.Drawing.Size(124, 26);
            this.TextBoxIdConsumo.TabIndex = 22;
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label6.Location = new System.Drawing.Point(166, 201);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(44, 20);
            this.label6.TabIndex = 27;
            this.label6.Text = "Total";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label7.Location = new System.Drawing.Point(19, 201);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(30, 20);
            this.label7.TabIndex = 26;
            this.label7.Text = "Iva";
            // 
            // TextBoxTotal
            // 
            this.TextBoxTotal.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TextBoxTotal.Location = new System.Drawing.Point(166, 229);
            this.TextBoxTotal.Name = "TextBoxTotal";
            this.TextBoxTotal.ReadOnly = true;
            this.TextBoxTotal.Size = new System.Drawing.Size(128, 26);
            this.TextBoxTotal.TabIndex = 25;
            // 
            // TextBoxIva
            // 
            this.TextBoxIva.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TextBoxIva.Location = new System.Drawing.Point(19, 229);
            this.TextBoxIva.Name = "TextBoxIva";
            this.TextBoxIva.ReadOnly = true;
            this.TextBoxIva.Size = new System.Drawing.Size(123, 26);
            this.TextBoxIva.TabIndex = 24;
            // 
            // Faturas
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(484, 291);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.label7);
            this.Controls.Add(this.TextBoxTotal);
            this.Controls.Add(this.TextBoxIva);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.TextBoxIdConsumo);
            this.Controls.Add(this.LabelResultado);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.TextBoxContagem);
            this.Controls.Add(this.TextBoxData);
            this.Controls.Add(this.TextBoxIdFatura);
            this.Controls.Add(this.ListBoxFaturas);
            this.Name = "Faturas";
            this.Text = "Faturas";
            this.WindowState = System.Windows.Forms.FormWindowState.Maximized;
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label LabelResultado;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox TextBoxContagem;
        private System.Windows.Forms.TextBox TextBoxData;
        private System.Windows.Forms.TextBox TextBoxIdFatura;
        private System.Windows.Forms.ListBox ListBoxFaturas;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.TextBox TextBoxIdConsumo;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.TextBox TextBoxTotal;
        private System.Windows.Forms.TextBox TextBoxIva;
    }
}
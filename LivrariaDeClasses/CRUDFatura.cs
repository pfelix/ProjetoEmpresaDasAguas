﻿namespace LivrariaDeClasses
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;

    public class CRUDFatura
    {
        DataClasses1DataContext dc = new DataClasses1DataContext();

        /// <summary>
        /// Inserir fatura
        /// </summary>
        /// <param name="inserirFatura"></param>
        /// <returns></returns>
        public bool CreateFatura(Fatura inserirFatura)
        {
            //Procurar se existe nif
            var verificarFatura = dc.Faturas.FirstOrDefault(f => f.idFatura == inserirFatura.idFatura);

            //Se NIF existe
            if (verificarFatura != null)
            {
                return false;
            }

            //Inserir dados
            dc.Faturas.InsertOnSubmit(inserirFatura);

            //Enviar para base de dados
            try
            {
                dc.SubmitChanges();
            }
            catch
            {
                return false;
            }
            return true;
        }

        /// <summary>
        /// Atualizar fatura
        /// </summary>
        /// <param name="atualizarFatura"></param>
        /// <returns></returns>
        public bool UpdateFatura(Fatura atualizarFatura)
        {
            //Procurar o cliente
            var fatura = dc.Faturas.FirstOrDefault(f => f.idFatura == atualizarFatura.idFatura);

            //Se cliente não existe
            if (fatura == null)
            {
                return false;
            }

            //Atualizar dados
            fatura.idCliente = atualizarFatura.idCliente;
            fatura.idConsumo = atualizarFatura.idConsumo;
            fatura.data = atualizarFatura.data;
            fatura.contagem = atualizarFatura.contagem;
            fatura.iva = atualizarFatura.iva;
            fatura.total = atualizarFatura.total;

            //Enviar para base de dados
            try
            {
                dc.SubmitChanges();
            }
            catch
            {
                return false;
            }
            return true;
        }

        /// <summary>
        /// Lista de todas as faturas
        /// </summary>
        /// <returns></returns>
        public List<Fatura> ReadFatura()
        {
            var fatura = from Fatura in dc.Faturas select Fatura;

            return fatura.ToList();
        }

        /// <summary>
        /// Mostra a fatura escolhido
        /// </summary>
        /// <param name="idFatura"></param>
        /// <returns></returns>
        public Fatura ReadFatura(int idFatura)
        {
            var fatura = dc.Faturas.
                FirstOrDefault(f => f.idFatura == idFatura);

            return fatura;
        }

        /// <summary>
        /// Mostra as faturas do cliente
        /// </summary>
        /// <param name="idCliente"></param>
        /// <returns></returns>
        public Fatura ReadFaturaCliente(int idCliente)
        {
            var fatura = dc.Faturas.FirstOrDefault(f => f.idCliente == idCliente);

            return fatura;
        }

        /// <summary>
        /// Apagar fatura
        /// </summary>
        /// <param name="idFatura"></param>
        /// <returns></returns>
        public bool DeleteFatura(int idFatura)
        {
            //Procurar o cliente
            var fatura = dc.Faturas.FirstOrDefault(f => f.idFatura == idFatura);

            //Se cliente não existe
            if (fatura == null)
            {
                return false;
            }

            dc.Faturas.DeleteOnSubmit(fatura);

            //Enviar para base de dados
            try
            {
                dc.SubmitChanges();
            }
            catch
            {
                return false;
            }

            return true;
        }
    }
}

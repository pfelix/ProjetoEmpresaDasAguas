﻿namespace LivrariaDeClasses
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;

    public class CRUDCliente
    {
        DataClasses1DataContext dc = new DataClasses1DataContext();

        public string Retorno { get; set; }

        /// <summary>
        /// Inserir dados do cliente da base de dados
        /// </summary>
        /// <param name="inserirCliente"></param>
        /// <returns></returns>
        public bool CreateCliente (Cliente inserirCliente)
        {
            //Procurar se existe nif
            var verificarNif = dc.Clientes.FirstOrDefault(
                c => c.nif == inserirCliente.nif);

            //Se NIF existe
            if (verificarNif != null)
            {
                Retorno = "NIF em duplicado";
                return false;
            }

            //Inserir dados
            dc.Clientes.InsertOnSubmit(inserirCliente);

            //Enviar para base de dados
            try
            {
                dc.SubmitChanges();
                return true;
            }
            catch (Exception e)
            {
                Retorno = e.ToString();
                return false;
            }
        }

        /// <summary>
        /// Atualizar dados do cliente da base de dados
        /// </summary>
        /// <param name="atualizarCliente"></param>
        /// <returns></returns>
        public bool UpdateCliente (Cliente atualizarCliente)
        {
            //Procurar o cliente
            var cliente = dc.Clientes.FirstOrDefault(c => c.idCliente == atualizarCliente.idCliente);

            //Se cliente não existe
            if (cliente == null)
            {
                Retorno = "Cliente já existe";
                return false;
            }

            //Atualizar dados
            cliente.nome = atualizarCliente.nome;
            cliente.morada = atualizarCliente.morada;
            cliente.codPostal = atualizarCliente.codPostal;
            cliente.nif = atualizarCliente.nif;
            cliente.utilizador = atualizarCliente.utilizador;
            cliente.pass = atualizarCliente.utilizador;

            //Enviar para base de dados
            try
            {
                dc.SubmitChanges();
            }
            catch (Exception e)
            {
                Retorno = e.ToString();
                return false;
            }
            return true;
        }

        /// <summary>
        /// Lista de todos os clientes
        /// </summary>
        /// <returns></returns>
        public List<Cliente> ReadCliente ()
        {
            var cliente = from Cliente in dc.Clientes select Cliente;

            return cliente.ToList();
        }

        /// <summary>
        /// Mostra o cliente escolhido
        /// </summary>
        /// <param name="idCliente"></param>
        /// <returns></returns>
        public Cliente ReadIdCliente(int idCliente)
        {
            var cliente = dc.Clientes.FirstOrDefault(c => c.idCliente == idCliente);

            return cliente;
        }

        /// <summary>
        /// Metodo para procurar pelo nome de cliente e retorna uma lista
        /// </summary>
        /// <param name="nome"></param>
        /// <returns></returns>
        public List<Cliente> ProcurarNomeCliente(string nome)
        {
            List<Cliente> listaCliente = (from clientes in dc.Clientes where clientes.nome.Contains(nome) select clientes).ToList();

            return listaCliente;
        }

        /// <summary>
        /// Metodo para procurar pela morada de cliente e retorna uma lista
        /// </summary>
        /// <param name="morada"></param>
        /// <returns></returns>
        public List<Cliente> ProcurarMoradaCliente(string morada)
        {
            List<Cliente> listaCliente = (from clientes in dc.Clientes where clientes.morada.Contains(morada) select clientes).ToList();

            return listaCliente;
        }

        /// <summary>
        /// Metodo para procurar pela Cód. Postal de cliente e retorna uma lista
        /// </summary>
        /// <param name="codPostal"></param>
        /// <returns></returns>
        public List<Cliente> ProcurarCodPostalCliente(string codPostal)
        {
            List<Cliente> listaCliente = (from clientes in dc.Clientes where clientes.codPostal.Contains(codPostal) select clientes).ToList();

            return listaCliente;
        }

        /// <summary>
        /// Metodo para procurar pelo NIF de cliente e retorna uma lista
        /// </summary>
        /// <param name="nif"></param>
        /// <returns></returns>
        public List<Cliente> ProcurarNifCliente(string nif)
        {
            List<Cliente> listaCliente = (from clientes in dc.Clientes where clientes.nif.ToString().Contains(nif) select clientes).ToList();

            return listaCliente;
        }

        /// <summary>
        /// Apagar dados do cliente da base de dados
        /// </summary>
        /// <param name="idCliente"></param>
        /// <returns></returns>
        public bool DeleteCliente (int idCliente)
        {
            //Procurar o cliente
            Cliente cliente = dc.Clientes.
                FirstOrDefault(c => c.idCliente == idCliente);

            //Se cliente não existe
            if(cliente == null)
            {
                Retorno = "Cliente não existe";
                return false;
            }

            Fatura fatura = dc.Faturas.
                FirstOrDefault(f => f.idCliente == idCliente);

            if (fatura != null)
            {
                Retorno = "Não pode apagar o cliente porque existem faturas já emitidas.";
                return false;
            }

            Consumo consumo = dc.Consumos.
                FirstOrDefault(co => co.idCliente == idCliente);

            if (consumo != null)
            {
                Retorno = "Não pode apagar o cliente porque existem consumos já emitidas.";
                return false;
            }

            dc.Clientes.DeleteOnSubmit(cliente);

            //Enviar para base de dados
            try
            {
                dc.SubmitChanges();
                return true;
            }
            catch (Exception e)
            {
                Retorno = e.ToString();
                return false;
            }
        }
    }
}

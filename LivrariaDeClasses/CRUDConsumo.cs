﻿namespace LivrariaDeClasses
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text.RegularExpressions;


    public class CRUDConsumo
    {
        
        DataClasses1DataContext dc = new DataClasses1DataContext();
        public string Mensagem { get; private set; }

        /// <summary>
        /// criar um consumo e inserir na tabela
        /// </summary>
        /// <param name="consumo"></param>
        /// <returns></returns>
        public bool CreateConsumo(Consumo consumo)
        {
            Cliente cliente = dc.Clientes.FirstOrDefault(c => c.idCliente ==consumo.idCliente);

            if (cliente == null)
            {
                Mensagem = "Cliente não existe";
                return false;
            }

            dc.Consumos.InsertOnSubmit(consumo);

            try
            {
                dc.SubmitChanges();
                return true;
            }
            catch (Exception e)
            {
                Mensagem = e.ToString();
                return false;
            }
        }
        
        /// <summary>
        /// ver os consumos
        /// </summary>
        /// <returns></returns>
        public List<Consumo> ReadConsumo()
        {
            //retorna uma lista dos consumos
            List<Consumo> consumo = (from consumos in dc.Consumos select consumos).ToList();

            return consumo;
        }


        /// <summary>
        /// ver o consumo de um cliente pesquisando por idCliente
        /// </summary>
        /// <param name="idCliente"></param>
        /// <returns></returns>
        public Consumo ReadConsumoPorIdCliente(int idCliente)
        {
            //ver o consumo de um cliente, por id
            Consumo consumo = dc.Consumos.FirstOrDefault(c => c.idCliente == idCliente);

            return consumo;
        }

       

        /// <summary>
        /// ver o consumo de um cliente pesquisando por idConsumo
        /// </summary>
        /// <param name="idConsumo"></param>
        /// <returns></returns>
        public Consumo ReadConsumoPorIdConsumo(int idConsumo)
        {
            //ver o consumo de um cliente, por id
            Consumo consumo = dc.Consumos.FirstOrDefault(c => c.idConsumo == idConsumo);

            return consumo;
        }

        /// <summary>
        /// pesquisa por data
        /// </summary>
        /// <param name="data"></param>
        /// <returns></returns>
        public Consumo ReadConsumoPorData(DateTime data)
        {
            //ver o consumo de um cliente, por id
            Consumo consumo = dc.Consumos.FirstOrDefault(c => c.data == data);

            return consumo;
        }


        /// <summary>
        /// fazer o update dos consumos
        /// </summary>
        /// <param name="consumo"></param>
        /// <returns></returns>
        public bool UpdateConsumo(Consumo consumo)
        {
            
            //onde o id for igual ao inserido faz o update dos dados
            Consumo atualiza = (from consumos in dc.Consumos where consumos.idConsumo == consumo.idConsumo select consumos).Single();
           
            atualiza.idCliente = consumo.idCliente;
            atualiza.data = consumo.data;
            atualiza.contagem = consumo.contagem;

            try
            {
                dc.SubmitChanges();

                return true;
            }
            catch (Exception e)
            {
                Mensagem = e.ToString();
                return false;
            }
           
        }

        /// <summary>
        /// apagar um consumo
        /// </summary>
        /// <param name="idConsumo"></param>
        /// <returns></returns>
        public bool DeleteConsumo(int idConsumo)
        {
            //apaga o consumo com este id
            Consumo consumo = (from consumos in dc.Consumos where consumos.idConsumo == idConsumo select consumos).SingleOrDefault();

            if (consumo == null)
            {
                Mensagem = "Consumo não existe";
                return false;
            }
            Fatura fatura = (from faturas in dc.Faturas where faturas.idConsumo == idConsumo select faturas).SingleOrDefault();

            if (fatura != null)
            {
                Mensagem = "Não pode apagar este consumo. Consumo ja faturado!";

                return false;
            }
            dc.Consumos.DeleteOnSubmit(consumo);
           
            try
            {
                dc.SubmitChanges();

                return true;
            }
            catch (Exception e)
            {
                Mensagem = e.ToString();
                return false;
            }
        }
    }
}

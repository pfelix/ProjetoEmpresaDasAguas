﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace LivrariaDeClasses
{
    public class Validacoes
    {
        /// <summary>
        /// Verifica se string é empty ou null
        /// </summary>
        /// <param name="texto"></param>
        /// <returns></returns>
        public static bool ValidacaoText(string texto)
        {
            if(!string.IsNullOrEmpty(texto))
            {
                return false;
            }
            return true;
        }

        /// <summary>
        /// Verifica se string é um número inteiro
        /// </summary>
        /// <param name="numero"></param>
        /// <returns></returns>
        public static bool ValidacaoInt(string numero)
        {
            int numero1;

            if (int.TryParse(numero, out numero1))
            {
                return false;
            }

            return true;
        }

        /// <summary>
        /// Verifica se string é um numero doblue
        /// </summary>
        /// <param name="numero"></param>
        /// <returns></returns>
        public static bool ValidacaoDouble(string numero)
        {
            double numero1;

            if (double.TryParse(numero, out numero1))
            {
                return false;
            }

            return true;
        }

        /// <summary>
        /// Verifica se CodPostal é valido
        /// </summary>
        /// <param name="value"></param>
        /// <returns></returns>
        public static bool ValidacaoCodPostal(string value)
        {
            /*Regex regex = new Regex("^[0-9]{2}" - tipo de dados, neste caso de 0 a  9 com 2 casas
        "-"  sinal
        "[a-zA-Z]{2}" a -z, 2 casas
        "-" 
        "[0-9]{2}$"); */
            Regex regex = new Regex("^[0-9]{4}-[0-9]{3}$");
            Match match = regex.Match(value);
            if (match.Success)
                return true;
            else
                return false;
        }
        
    }
}

﻿namespace LivrariaDeClasses
{
    using System;
    using System.Collections.Generic;
    using System.Linq;

    public class CRUDEscalao
    {
        DataClasses1DataContext dc = new DataClasses1DataContext();

        public string Mensagem { get; private set; }

        /// <summary>
        /// criar um novo escalao e inserir na tabela
        /// </summary>
        /// <param name="escalao"></param>
        /// <returns></returns>
        public bool CreateEscalao(Escalao escalao)
        {
            //insere um objecto do tipo escalao na tabela escaloes
            dc.Escalaos.InsertOnSubmit(escalao);

            try
            {
                dc.SubmitChanges();
                return true;
            }
            catch (Exception e)
            {
                Mensagem = e.ToString();
                return false;
            }
        }


        /// <summary>
        /// ve os escalões existentes
        /// </summary>
        /// <returns></returns>
        public List<Escalao> ReadEscalao()
        {
            //retorna uma lista dos escaloes
            List<Escalao> escalao = (from escaloes in dc.Escalaos select escaloes).ToList();

            return escalao;
        }

        /// <summary>
        /// Retorna o escalao
        /// </summary>
        /// <param name="idEscalao"></param>
        /// <returns></returns>
        public Escalao ReadEscalaoPorId(int idEscalao)
        {
            //retorna uma lista dos escaloes
            Escalao escalao = (from escaloes in dc.Escalaos where escaloes.idEscalao == idEscalao select escaloes).SingleOrDefault();

            return escalao;
        }

        /// <summary>
        /// Retorna todos os escaloes da contagem
        /// </summary>
        /// <param name="contagem"></param>
        /// <returns></returns>
        public List<Escalao> ReadEscalaoPorContagem(int contagem)
        {
            //retorna o escalao
            List<Escalao> escalao = (
                from escaloes 
                in dc.Escalaos
                where contagem > escaloes.minimo
                orderby escaloes.minimo
                select escaloes).ToList();

            return escalao;
        }

        /// <summary>
        /// faz a atualização dos escaloes
        /// </summary>
        /// <param name="escalao"></param>
        /// <returns></returns>
        public bool UpdateEscalao(Escalao escalao)
        {
            //onde o id for igual ao id inserido faz o update dos dados do escalao
            var atualiza = (from escaloes in dc.Escalaos where escaloes.idEscalao == escalao.idEscalao select escaloes).Single();
            
            //campos a actualizar
            atualiza.minimo = escalao.minimo;
            atualiza.valorUnitario = escalao.valorUnitario;
            
            try
            {
                dc.SubmitChanges();

                return true;
            }
            catch (Exception e)
            {
                Mensagem = e.ToString();
                return false;
            }
        }


        /// <summary>
        /// apaga um escalao
        /// </summary>
        /// <param name="idEscalao"></param>
        /// <returns></returns>
        public bool DeleteEscalao(int idEscalao)
        {
            //apaga o escalao com este id
            var escalao = (from escaloes in dc.Escalaos where escaloes.idEscalao == idEscalao select escaloes).SingleOrDefault();

            dc.Escalaos.DeleteOnSubmit(escalao);

            try
            {
                dc.SubmitChanges();

                return true;
            }
            catch (Exception e)
            {
                Mensagem = e.ToString();
                return false;
            }
        }
    }
}

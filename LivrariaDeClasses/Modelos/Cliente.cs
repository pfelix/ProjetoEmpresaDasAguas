﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WindowsFormsCliente.Modelos
{
    public class Cliente
    {
        public int idCliente { get; set; }

        public string nome { get; set; }

        public string morada { get; set; }

        public string codPostal { get; set; }

        public int nif { get; set; }

        public string utilizador { get; set; }

        public string pass { get; set; }
    }
}

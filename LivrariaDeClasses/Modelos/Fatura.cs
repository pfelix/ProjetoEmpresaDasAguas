﻿namespace WindowsFormsCliente.Modelos
{
    using System;

    public class Fatura
    {
        public int idFatura { get; set; }

        public int idCliente { get; set; }

        public int idConsumo { get; set; }

        public DateTime data { get; set; }

        public int contagem { get; set; }

        public double iva { get; set; }

        public double total { get; set; }

    }
}

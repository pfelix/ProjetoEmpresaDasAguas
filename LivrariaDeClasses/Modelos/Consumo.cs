﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WindowsFormsCliente.Modelos
{
    public class Consumo
    {
        public int idConsumo { get; set; }

        public int idCliente { get; set; }

        public DateTime data { get; set; }

        public int contagem { get; set; }
    }
}

﻿namespace WindowsFormsCliente.Servicos
{
    using Newtonsoft.Json;
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Net.Http;
    using System.Text;
    using System.Threading.Tasks;
    using Modelos;

    public class ApiServicesConsumo
    {
        /// <summary>
        /// vai buscar os resultador e converte de json
        /// </summary>
        /// <param name="urlBase"></param>
        /// <param name="controller"></param>
        /// <returns></returns>
        public async Task<Response> GetConsumos(string urlBase, string controller)//faz uma tarefa do tipo response //async faz isto sem ir abaixo a aplicacao
        {
            try
            {
                var client = new HttpClient();//crio uma ligacao http, para ir a uma ligacao externa

                client.BaseAddress = new Uri(urlBase);//dou a url de base, so o site, sem a pasta expecifica

                var response = await client.GetAsync(controller);// getasync dou o resto do caminho, portanto a pasta onde ele esta (controlador da api)

                var result = await response.Content.ReadAsStringAsync();//recebo o resultado da api em formato string, para dentro da 

                if (!response.IsSuccessStatusCode)//se alguma coisa correr mal
                {
                    return new Response
                    {
                        IsSuccess = false,
                        Message = result
                    };
                }

                var consumos = JsonConvert.DeserializeObject<List<Consumo>>(result);//pega nos resultados, convert de json e cria uma lista de objectos

                return new Response
                {
                    IsSuccess = true,
                    Result = consumos
                };

            }
            catch (Exception ex)
            {
                return new Response
                {
                    IsSuccess = false,
                    Message = ex.Message
                };
            }
        }

    }
}

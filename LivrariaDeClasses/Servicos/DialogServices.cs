﻿namespace WindowsFormsCliente.Servicos
{
    using System.Windows.Forms;
    

    public class DialogServices
    {
        /// <summary>
        /// ostra mensagem
        /// </summary>
        /// <param name="title"></param>
        /// <param name="message"></param>
        public void ShowMessage(string title, string message)
        {
            MessageBox.Show(message, title);
        }
    }
}

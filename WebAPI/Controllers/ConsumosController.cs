﻿namespace WebAPI.Controllers
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Net;
    using System.Net.Http;
    using System.Web.Http;
    using WebAPI.Models;

    /// <summary>
    /// Acesso tabela Consumos
    /// </summary>
    public class ConsumosController : ApiController
    {
        DataClasses1DataContext dc = new DataClasses1DataContext();

        /// <summary>
        /// Lista de todos os consumos
        /// </summary>
        /// <returns>Retorna uma lista de todos os consumos</returns>
        // GET: api/Consumos
        public List<Consumo> Get()
        {
            var listaConsumos = from Consumos in dc.Consumo select Consumos;

            return listaConsumos.ToList();
        }

        /// <summary>
        /// Dados de consumos
        /// </summary>
        /// <param name="id"></param>
        /// <returns>Retorna os dados de consumo de um idConsumo</returns>
        // GET: api/Consumos/5
        public IHttpActionResult Get(int id)
        {
            var consumo = dc.Consumo.FirstOrDefault(c => c.idConsumo == id);

            if (consumo != null)
            {
                return ResponseMessage(Request.CreateResponse(HttpStatusCode.OK, consumo));
            }

            return ResponseMessage(Request.CreateResponse(HttpStatusCode.NotFound));
        }

        /// <summary>
        /// Lista de consumos do cliente
        /// </summary>
        /// <param name="id"></param>
        /// <returns>Retorna uma lista de consumos do cliente. id = idCliente</returns>
        // GET: api/Consumos/GetConsumos/5
        [Route("api/Consumos/GetConsumos/{id}")]
        public IHttpActionResult GetConsumos(int id)
        {
            var listaConsumos = (from Consumos in dc.Consumo
                                where Consumos.idCliente == id
                                select Consumos).ToList();

            if (listaConsumos.Count != 0)
            {
                return ResponseMessage(Request.CreateResponse(HttpStatusCode.OK, listaConsumos));
            }

            return ResponseMessage(Request.CreateResponse(HttpStatusCode.NotFound));
        }

        // POST: api/Consumos
        //public void Post([FromBody]string value)
        //{
        //}

        //// PUT: api/Consumos/5
        //public void Put(int id, [FromBody]string value)
        //{
        //}

        //// DELETE: api/Consumos/5
        //public void Delete(int id)
        //{
        //}
    }
}

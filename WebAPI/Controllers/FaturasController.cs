﻿namespace WebAPI.Controllers
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Net;
    using System.Net.Http;
    using System.Web.Http;
    using WebAPI.Models;

    /// <summary>
    /// Acesso Tabela faturas
    /// </summary>
    public class FaturasController : ApiController
    {
        DataClasses1DataContext dc = new DataClasses1DataContext();

        /// <summary>
        /// Lista de todas as faturas
        /// </summary>
        /// <returns>Retorna uma lista de todas as faturas</returns>
        /// // GET: api/Faturas
        public List<Fatura> Get()
        {
            var listaFaturas = from Faturas in dc.Fatura select Faturas;

            return listaFaturas.ToList();
        }

        /// <summary>
        /// Lista de todas as faturas do cliente
        /// </summary>
        /// <param name="id"></param>
        /// <returns>Retorna uma lista das faturas do cliente</returns>
        // GET: api/Faturas/GetFaturas/1
        [Route("api/Faturas/GetFaturas/{id}")]
        public IHttpActionResult GetFaturas(int id)
        {
            List<Fatura> listaFaturas = (from Faturas in dc.Fatura
                                        where Faturas.idCliente == id
                                        select Faturas).ToList();

            if (listaFaturas.Count != 0)
            {
                return ResponseMessage(Request.CreateResponse(HttpStatusCode.OK, listaFaturas));
            }

            return ResponseMessage(Request.CreateResponse(HttpStatusCode.NotFound)) ;
        }

        /// <summary>
        /// Dados de fatura
        /// </summary>
        /// <param name="id"></param>
        /// <returns>Retorna uma fatura pelo id Fatura</returns>
        // GET: api/Faturas/5
        public IHttpActionResult Get(int id)
        {
            var fatura = dc.Fatura.FirstOrDefault(f => f.idFatura == id);

            if (fatura != null)
            {
                return ResponseMessage(Request.CreateResponse(HttpStatusCode.OK, fatura));
            }

            return ResponseMessage(Request.CreateResponse(HttpStatusCode.NotFound));
        }

        // POST: api/Faturas
        //public void Post([FromBody]string value)
        //{
        //}

        //// PUT: api/Faturas/5
        //public void Put(int id, [FromBody]string value)
        //{
        //}

        //// DELETE: api/Faturas/5
        //public void Delete(int id)
        //{
        //}
    }
}

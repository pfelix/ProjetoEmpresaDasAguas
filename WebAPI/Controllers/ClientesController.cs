﻿namespace WebAPI.Controllers
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Net;
    using System.Net.Http;
    using System.Web.Http;
    using WebAPI.Models;

    /// <summary>
    /// Acesso Tabela clientes
    /// </summary>
    public class ClientesController : ApiController
    {
        DataClasses1DataContext dc = new DataClasses1DataContext();

        /// <summary>
        /// Lista de clientes
        /// </summary>
        /// <returns>Retorna uma lista de clientes</returns>
        // GET: api/Clientes
        public List<Cliente> Get()
        {
            var lista = from Clientes in dc.Cliente select Clientes;

            return lista.ToList();
        }

        /// <summary>
        /// Validar acesso de cliente
        /// </summary>
        /// <param name="user"></param>
        /// <param name="pass"></param>
        /// <returns>Retorna o cliente</returns>
        // GET: api/Clientes/ana senha123
        [Route("api/Clientes/GetUser/{user} {pass}")]
        public IHttpActionResult GetUer(string user, string pass)
        {
            Cliente cliente = dc.Cliente.Where(u => u.utilizador == user && u.pass == pass).FirstOrDefault();

            if (cliente != null)
            {
                return ResponseMessage(Request.CreateResponse(HttpStatusCode.OK, cliente));
            }

            return ResponseMessage(Request.CreateResponse(HttpStatusCode.NotAcceptable));
        }

        /// <summary>
        /// Dados do cliente
        /// </summary>
        /// <param name="id"></param>
        /// <returns>Retorna os dados do cliente, procurado pelo Id Cliente</returns>
        // GET: api/Clientes/5
        public IHttpActionResult Get(int id)
        {
            Cliente cliente = dc.Cliente.FirstOrDefault(c => c.idCliente == id);
            
            if (cliente != null)
            {
                return ResponseMessage(Request.CreateResponse(HttpStatusCode.OK, cliente));
            }

            return ResponseMessage(Request.CreateResponse(HttpStatusCode.NotFound));
        }

        // POST: api/Clientes
        //public void Post([FromBody]string value)
        //{
        //}

        //// PUT: api/Clientes/5
        //public void Put(int id, [FromBody]string value)
        //{
        //}

        //// DELETE: api/Clientes/5
        //public void Delete(int id)
        //{
        //}
    }
}
